import { Homepage, HomepageStats } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class HomeModal {
  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }
  createHomePageStatsTable() {
    console.log('came in CreateMainHometable Modal');
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  home_page_stats (`Id` int(10) NOT NULL, `total_salons` int , `toal_fashion` int, `toal_beauty_trends` int, `toal_beauty_tips` int,  `total_product` int );"//
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('home_page_stats table created');
        
        return isTableCreated
      })
      .catch(e => {
        let msg:string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)  
        }
        console.log('error: InsertInToHomeTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table app_home_page_section already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }
  InsertInToHomeStatsTable(Id, total_salons, total_fashion, total_beauty_trends, total_beauty_tips, total_product) {
     console.log('Salons:'+total_salons+'fashion:'+total_fashion+'toal_beauty_trends:'+total_beauty_trends+"total_beauty_tips"+total_beauty_tips)
    let firstPart = "INSERT INTO `home_page_stats` (`Id`, `total_salons`, `toal_fashion`  , `toal_beauty_trends` , `toal_beauty_tips` , `total_product` ) VALUES ";
    let secondPart = ""
    let sectionIDs = ""
    secondPart += '("' + Id + '", "' +
    total_salons+ '", "' + 
    total_fashion+ '", "' + 
    total_beauty_trends+ '", "' + 

    total_beauty_tips+ '", "' + 
    total_product + '"),'; 
     sectionIDs += Id + ','
    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    sectionIDs = sectionIDs.slice(0, sectionIDs.length - 1)
    console.log("sectionIDs", sectionIDs)
    return this.deleteMultipleHomePageStats(sectionIDs).then(tblHomeDeleted => {
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
          console.log('Inserted'+res.rowsAffected)
          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {
          console.log('message'+e.message)
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)  
          isDataInserted = false
          return e.message
        });

    })
  }

  getHomePageStats() {
    let Inserquery = "select * from home_page_stats"
    let homePageStats = null
    return this.database.executeSql(Inserquery, [])
      .then(homeSectionsArray => {
        for (var i = 0; i < homeSectionsArray.rows.length; i++) {
          console.log('homeSectionItem:',JSON.stringify(homeSectionsArray.rows.item(i)))
           homePageStats = homeSectionsArray.rows.item(i)
        //   return homePageStats
        }
        return homePageStats
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)  
        console.log('error:  HomeSections ' + e.message);

        return homePageStats

      });
  }

  deleteMultipleHomePageStats(Ids) {
    let deleteSelectedQuery = 'DELETE FROM `home_page_stats`  where Id IN (' + Ids + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)  
        return false;
      });
  }

  createMainHomeTable() {
    console.log('came in CreateMainHometable Modal');
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  app_home_page_section ( `Id` int(10) NOT NULL,`sectionName` varchar(500) NOT NULL,`sectionDescription` text NOT NULL,`otherDescription` text NOT NULL,`sectionImage` varchar(1000) NOT NULL,`sectionBackground` varchar(1000) NOT NULL,`sort_order` int(10) NOT NULL,`is_header` int(11) NOT NULL DEFAULT '0',`view` varchar(1000) NOT NULL  , `sectionParameters` varchar(1000) );"
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('app_home_page_section table created');
        
        return res
      })
      .catch(e => {
        let msg:string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)  
        }
        

        console.log('error: InsertInToHomeTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table app_home_page_section already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }

  InsertInToHomeTable(homepageData) {
    if (!homepageData || homepageData.length === 0) {
      return 
    }
    let firstPart = "INSERT INTO `app_home_page_section` (`Id`, `sectionName`, `sectionDescription`, `otherDescription`, `sectionImage`, `sectionBackground`, `sort_order`, `is_header`, `view`, `sectionParameters`) VALUES ";
    let secondPart = ""
    console.log('homepageData', homepageData);
    let sectionIDs = ""
    homepageData.forEach(element => {
      let section: Homepage = element
      if (section.status == "1") {
        secondPart += '("' + section.Id + '", "' + section.sectionName + '", "' + section.sectionDescription + '", "' + section.otherDescription + '" , "' + section.sectionImage + '", "' + section.sectionBackground + '", "' + section.sort_order + '", "' + section.is_header + '", "' + section.view + '", "' + section.sectionParameters + '"),';
      }
      sectionIDs += section.Id + ','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    sectionIDs = sectionIDs.slice(0, sectionIDs.length - 1)

    console.log("sectionIDs", sectionIDs)

    return this.deleteMultipleSections(sectionIDs).then(tblHomeDeleted => {
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {

          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)  
          isDataInserted = false
          return e.message
        });

    })
  }

  getHomeSections() {
    let Inserquery = "select * from app_home_page_section"
    let HomeSections = []
    return this.database.executeSql(Inserquery, [])
      .then(homeSectionsArray => {
        for (var i = 0; i < homeSectionsArray.rows.length; i++) {
          console.log('homeSectionItem:',JSON.stringify(homeSectionsArray.rows.item(i)))
          let homeSection = homeSectionsArray.rows.item(i)
          HomeSections.push(homeSection)
        }
        return HomeSections
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)  
        console.log('error:  HomeSections ' + e.message);

        return HomeSections

      });
  }

  TruncCateHomeTable() {
    let deleteQuery = "DELETE FROM `app_home_page_section` ;"

    let isDeleted = false
    
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        

        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)  
        console.log('error: app_home_page_section');

        isDeleted = false
        return e.message

      });
  }
  
  deleteMultipleSections(salIds) {

    let deleteSelectedQuery = 'DELETE FROM `app_home_page_section`  where Id IN (' + salIds + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)  
        return false;
      });
  }
}

