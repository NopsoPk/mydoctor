
import { Platform, Alert, Item } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Services, SALON_SERVICES_SUB_CATEGORIES, SalonServices, ServiceSubCATEGORIES, Salon, SALON_SERVICES_SEARCH, SAL_SER_SUB_CATEGORIES, SalonHour, SalonTechnicain, Sal_Techs } from './../../providers/SalonAppUser-Interface';
import { GlobalProvider } from './../../providers/global/global';
import { BehaviorSubject } from "rxjs/Rx";
import { Storage } from "@ionic/storage";
import { Notification } from '../../Interfaces/Notification'
@Injectable()
export class SqliteDbProvider {
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;
  //for salon
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  public SALONS: Salon[]
  public SAL_SER_SUB_CAT: SAL_SER_SUB_CATEGORIES[]

  public notifications: Notification[]

  public SalonServices: SalonServices[]
  public ServiceSubCATEGORIE: ServiceSubCATEGORIES[]

  //salon table columns

  SQL_SAL_ID_AUTO = 'sal_id_pk'
  SQL_SAL_ID = 'sal_id'
  SQL_SAL_NAME = 'sal_name'
  SQL_SAL_PHONE = 'sal_phone'
  SQL_SAL_PIC = 'sal_pic'
  SQL_SAL_PROFILE_PIC = 'sal_profile_pic'
  SQL_SAL_RATING = 'sal_rating'
  SQL_SAL_STATUS = 'sal_status'
  SQL_SAL_SPECIALTY = 'sal_specialty'
  SQL_SAL_SERACH_WORD = 'sal_search_words'
  SQL_SAL_HOURS = 'sal_hours'
  SQL_SAL_HOURS_1 = 'sal_hours1'
  SQL_SAL_TEMP_ENABLED = 'sal_temp_enabled'
  SQL_SAL_ADDRESS = 'sal_address'
  SQL_SAL_ZIP = 'sal_zip'
  SQL_SAL_ACTIVE_PROMOTION = 'sal_active_promotions'
  SQL_SAL_MATCHED = 'matched'
  SQL_SAL_FAVSAL = 'favsal'
  SQL_SAL_DISTANCE = 'distance'


  //for services

  // for sal_ser_sub_categories 
  SQL_SAL_SER_SSSC_ID = 'sssc_id'
  SQL_SAL_SER_SAL_ID = 'sal_id'
  SQL_SAL_SER_SSC_ID = 'ssc_id'



  //zahid work
  sser_id = 'sser_id'
  ser_id = 'ser_id'
  sser_name = 'sser_name'
  sal_id = 'sal_id'
  sser_rate = 'sser_rate'
  sser_time = 'sser_time'
  sser_enabled = 'sser_enabled'
  sser_featured = 'sser_featured'
  ssc_id = 'ssc_id'
  sser_order = 'sser_order'
  ssubc_id = 'ssubc_id'

  //service sub catogories
  SQL_SSC_GENDER = 'ssc_gender'
  SQL_SSC_ID = 'ssc_id'
  SQL_SSC_IMAGE = 'ssc_image'
  SQL_SSC_NAME = 'ssc_name'
  SQL_SSC_STATUS = 'ssc_status'

  //for search table
  SQL_SR_KEYWORD = 'keyword'
  SQL_SR_TYPE = 'type'
  SQL_SR_REFER_ID = 'refer_id'
  SQL_SR_IMAGE = 'ser_image'



  //for table
  // SQL_TABLE_SER_SUB_CAT = 'service_sub_categories'
  SQL_TABLE_SALON = 'salons'
  //  SQL_TABLE_SAL_SERVICES = 'salon_services'

  SQL_TABLE_SEARCh = 'search'

  SQL_TABLE_SAL_SER_SUB_CATEGORIES = 'sal_ser_sub_categories'



  constructor(public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider,
  ) {

    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;

          this.databaseReady.next(true)

        }, err => {


        })
    }).catch(e => {

    })
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }
  create_table_salon() {

    let createTableQuery = "CREATE TABLE IF NOT EXISTS salons (`sal_id` int(6) ,`sal_name` varchar(500) ," +
      "`sal_address` varchar(255) NOT NULL DEFAULT '', `sal_city` varchar(48) NOT NULL DEFAULT ''," +
      "`sal_zip` varchar(100) NOT NULL DEFAULT '', `sal_contact_person` varchar(32) NOT NULL DEFAULT ''," +
      "`sal_email` varchar(128) NOT NULL DEFAULT '', `sal_phone` varchar(150) NOT NULL DEFAULT ''," +
      "`sal_hours` text NOT NULL DEFAULT '', `sal_pic` varchar(64) NOT NULL DEFAULT '', `sal_profile_pic` varchar(255) NOT NULL DEFAULT ''," +
      "`sal_created_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP," +
      "`sal_hours1` text , " +
      "`sal_status` tinyint(1) NOT NULL DEFAULT '', `sal_temp_enabled` int(1) ," +
      " `sal_lat` varchar(32) NOT NULL DEFAULT ''," +
      "`sal_lng` varchar(32) , `is_active` tinyint(1) NOT NULL DEFAULT '1'," +
      "`sal_auto_accept_app` tinyint(1) NOT NULL DEFAULT '1'," +
      "`sal_future_app_days` int(2) , `sal_24clock` tinyint(1) ," +
      "`sal_queue_status` tinyint(1) NOT NULL DEFAULT '', `sal_specialty` varchar(128) DEFAULT NULL," +
      "`sal_facebook` varchar(255) DEFAULT NULL, `sal_instagram` varchar(255)," +
      "`sal_twitter` varchar(250) DEFAULT NULL, `sal_reviews` int(4) ," +
      "`sal_rating` varchar(5) NOT NULL DEFAULT '0'," +
      "`sal_modify_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP," +
      "`sal_timing` varchar(255) DEFAULT NULL, `sal_website` varchar(255) DEFAULT NULL," +
      "`sal_nearby_zip` varchar(255) DEFAULT NULL, `sal_active_promotions` int(3) ," +
      "`sal_is_default` tinyint(1)  NOT NULL DEFAULT '0'," +
      "`sal_biography` text DEFAULT NULL, `sal_weekly_offs` varchar(16) ," +
      "`sal_appointment_interval` int(2), `sty_id` varchar(16) DEFAULT NULL," +
      "`sal_services` varchar(2000) DEFAULT NULL, `sal_is_reviewed` tinyint(1) NOT NULL DEFAULT '0'," +
      "`sal_review_datetime` datetime DEFAULT NULL DEFAULT CURRENT_TIMESTAMP," +
      "`temp_sal_id` varchar(50) DEFAULT NULL, " +
      "`search_key_word` varchar(1000) DEFAULT NULL, `sal_unsubscribe` tinyint(1),`distance`,`favsal` tinyint(1)  NOT NULL DEFAULT 0, `sal_app_after_hours` tinyint(2)); ";

    this.database.executeSql(createTableQuery, [])
      .then(res => {


      }).catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

      });

  }

  create_table_notification() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  notifications  ( `not_id` int(10), `not_datetime` varchar(55), `not_message` varchar(255), `not_is_read` int(10), `app_id` int(10), `sal_id` int(10), `cust_id` int(10), `sal_name` varchar(55), `sal_pic` varchar(255) );"
    this.database.executeSql(createTableQuery, []).then(res => {
      console.log("notification", "table created")
    }).catch(e => {
      let msg: string = e.message
      if (!msg.includes('already exists')) {
        this.serviceManager.sendErrorToServer(e.message, createTableQuery)
      }

      console.log('error: while creating table notification ' + e.message);
    })
  }

  insert_notification(Notification) {
    if (!Notification || Notification.length === 0) {
      return
    }
    let firstPart = "INSERT INTO `notifications` (`not_id`, `not_datetime`,`not_message` ,`not_is_read`,`app_id`,`sal_id`,`cust_id`,`sal_name`,`sal_pic`)  VALUES ";
    let secondPart = ""
    let salIds = ""
    Notification.forEach(sal => {
      secondPart += '("'
        + sal.not_id + '", "'
        + sal.not_datetime + '", "'
        + sal.not_message + '", "'
        + sal.not_is_read + '", "'
        + sal.app_id + '" , "'
        + sal.sal_id + '", "'
        + sal.cust_id + '", "'
        + sal.sal_name + '", "'
        + sal.sal_pic + '"),';
      salIds += sal.not_id + ','
    });
    salIds = salIds.slice(0, salIds.length - 1)
    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    return this.deleteSalNotification(salIds).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, [])
          .then(res => {
            console.log('NotificationAdd: ' + res.rowsAffected + ' notifications were dumped');
          })
          .catch(e => {
            console.log(slicedQuery);
            console.log('eror: while inserting into TechAdd' + e.message)
          });
      }
    })
  }
  getAllNotifications() {
    this.notifications = []
    // 'SELECT * FROM `notifications` order BY not_datetime desc'
    return this.database.executeSql('SELECT  * FROM  notifications ORDER BY not_datetime DESC ', [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          this.notifications.push(res.rows.item(i))

          // let item = {
          //   not_id: res.rows.item(i).not_id, not_datetime: res.rows.item(i).not_datetime,
          //   not_message: res.rows.item(i).not_message, not_is_read: res.rows.item(i).not_is_read,
          //   app_id: res.rows.item(i).app_id, sal_id: res.rows.item(i).sal_id, cust_id: res.rows.item(i).cust_id,
          //   sal_name: res.rows.item(i).sal_name, sal_pic: res.rows.item(i).sal_pic
          // }
          // this.notifications.push(item)
        }

        return this.notifications
      })
      .catch(e => {
        return this.notifications = []
      });
  }



  deleteSalNotifiById(not_id) {
    not_id = Number(not_id)
    let Query = 'DELETE FROM notifications where not_id= ' + not_id + '';
    //let deleteCategorires = 'DELETE FROM notifications ';//where not_id = "' + not_id + '"'
    return this.database.executeSql(Query, [])
      .then(Query => {

        return true;
      })
      .catch(e => {

        return false;
      });
  }
  deleteSalNotification(not_id) {
    // let deleteCategorires = 'DELETE FROM `service_categories` ';
    let deleteCategorires = 'DELETE FROM notifications where not_id IN (' + not_id + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {
        console.log("notificationDeleted", "Deleted")
        return true;
      })
      .catch(e => {

        return false;
      });
  }


  deleteNotificationAll() {
    return this.database.executeSql('DELETE FROM notifications', [])
      .then(res => {
        return true
      })
      .catch(e => {
        console.log('error: ' + e.message);
        return false
      });

  }

  //'DELETE FROM `search`  where type= "' + type + '"

  create_table_sal_ser_sub_categories() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS  sal_ser_sub_categories  ( `sssc_id` int(10), `sal_id` int(10), `ssc_id` int(10) );"
    this.database.executeSql(createTableQuery, []).then(res => {


    }).catch(e => {
      let msg: string = e.message
      if (!msg.includes('already exists')) {
        this.serviceManager.sendErrorToServer(e.message, createTableQuery)
      }

      console.log('error: while creating table sal_ser_sub_categories ' + e.message);

    })

  }


  insert_sal_ser_sub_categories(SAL_SER_SUB_CATEGORIE) {
    if (!SAL_SER_SUB_CATEGORIE || SAL_SER_SUB_CATEGORIE.length === 0) {
      // this.serviceManager.makeToastOnFailure('SAL_SER_SUB_CATEGORIE is UNDEDEFINED or NULL', 'top')
      return
    }
    let firstPart = "INSERT INTO `sal_ser_sub_categories` (`sssc_id`, `sal_id`,`ssc_id`)  VALUES ";
    let secondPart = ""
    SAL_SER_SUB_CATEGORIE.forEach(element => {
      secondPart += '("' + element[0] + '", "' + element[1] + '", "' + element[2] + '"),';
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    return this.database.executeSql(slicedQuery, []).then(res => {
      console.log('insertionTest: ' + res.rowsAffected + ' sal_ser_sub_categories were dumped');
    }).catch(e => {
    })
  }


  getSalSerSubCatFromSqliteDB(ssc_id) {
    let SalonServiceSubCategories: SALON_SERVICES_SUB_CATEGORIES[] = []
    let Query = 'SELECT DISTINCT sal_id FROM sal_ser_sub_categories where ssc_id=' + ssc_id;

    return this.database.executeSql(Query, [])
      .then(res => {

        for (var i = 0; i < res.rows.length; i++) {
          let salSerSubCat: SALON_SERVICES_SUB_CATEGORIES = res.rows.item(i)

          SalonServiceSubCategories.push(salSerSubCat)
        }
        console.log('salonInDb' + res.rows.length)
        return SalonServiceSubCategories
      })
      .catch(e => {
        console.log('ErrorHere' + e.eror)
        return SalonServiceSubCategories
      });
  }

  getSalSerSubCatFromSqliteDBForBeautyTips(ssc_id) {
    let SalonServiceSubCategories: SALON_SERVICES_SUB_CATEGORIES[] = []
    let Query = 'SELECT DISTINCT sal_id FROM sal_ser_sub_categories where ssc_id  IN (' + ssc_id + ')';

    return this.database.executeSql(Query, [])
      .then(res => {

        for (var i = 0; i < res.rows.length; i++) {
          let salSerSubCat: SALON_SERVICES_SUB_CATEGORIES = res.rows.item(i)

          SalonServiceSubCategories.push(salSerSubCat)
        }

        return SalonServiceSubCategories
      })
      .catch(e => {

        return SalonServiceSubCategories
      });
  }


  getAllSalSerSubCatFromSqliteDB() {
    this.SAL_SER_SUB_CAT = []
    return this.database.executeSql('SELECT  * FROM ' + this.SQL_TABLE_SAL_SER_SUB_CATEGORIES + '  ', [])
      .then(res => {

        for (var i = 0; i < res.rows.length; i++) {

          let item = { sssc_id: res.rows.item(i).sssc_id, sal_id: res.rows.item(i).sal_id, ssc_id: res.rows.item(i).ssc_id }
          this.SAL_SER_SUB_CAT.push(item)
        }

        return this.SAL_SER_SUB_CAT
      })
      .catch(e => {
        return this.SAL_SER_SUB_CAT = []
      });
  }



  create_table_search() {
    let createTableQuery = 'CREATE TABLE IF NOT EXISTS ' + this.SQL_TABLE_SEARCh + '(' +
      this.SQL_SR_KEYWORD + ' TEXT , ' + this.SQL_SR_TYPE + ' TEXT, ' + this.SQL_SR_IMAGE + ' TEXT, ' + this.SQL_SR_REFER_ID + ' TEXT)'


    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('success: search table created.');
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table category_sub_style', e.message)
      });
  }


  getSalonTechnicianBySalId(salId) {

    let SalonTechnician: Sal_Techs[] = []
    let Query = 'SELECT * FROM salon_technician where sal_id= "' + salId + '"';
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          let hours: Sal_Techs = res.rows.item(i)
          SalonTechnician.push(hours)
        }
        return SalonTechnician
      })
      .catch(e => {
        return SalonTechnician = []
      });
  }


  getSalonTechnicianLatUpdate(salId) {
    // let query = 'SELECT sal_id FROM  salons'
    let lastUpdate = ''
    let SalonTechnician: SalonTechnicain[] = []
    let Query = 'SELECT tech_last_update FROM salon_technician where sal_id= "' + salId + '" limit 1 ';
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          lastUpdate = res.rows.item(i).tech_last_update
        }
        return lastUpdate
      })
      .catch(e => {
        return lastUpdate = ''
      });
  }

  getSalonTotalTech(salId) {
    // let query = 'SELECT sal_id FROM  salons'
    let totalTechs = 0
    let SalonTechnician: SalonTechnicain[] = []
    let Query = 'SELECT COUNT(tech_id) as totalTechs   FROM salon_technician where sal_id= "' + salId + '" limit 1 ';
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          totalTechs = Number(res.rows.item(i).totalTechs)
        }
        return totalTechs
      })
      .catch(e => {
        return totalTechs = 0
      });
  }






  createSalonTechnician() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `salon_technician` (`sal_id` int(4),`tech_id` int(4) ,`tech_name` varchar(64) ,`tech_pic` varchar(64) ,`tech_status` varchar(64) ,`tech_specialty` varchar(164) ,`tech_weekly_offs` varchar(64),`tech_off` varchar(64) ,`tech_modify_datetime` varchar(64) )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('salon_technician created successfully!')
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while salon_technician table salon_hours', e.message)
      });
  }


  saveIntoSalTechTable(salTechs: SalonTechnicain[]) {
    console.log('insertionTest: ' + salTechs.length + ' salon_technician were sent for dumping');
    if (!salTechs || salTechs.length === 0) {
      return
    }

    let fieledsPart = "INSERT INTO `salon_technician` (`sal_id`, `tech_id`, `tech_name`, `tech_pic`, `tech_status`, `tech_specialty` , `tech_weekly_offs`,  `tech_off`, `tech_modify_datetime`) VALUES ";
    let ValuesPart = ""
    let salIds = ""
    salTechs.forEach(salTech => {
      ValuesPart += '("' + salTech.sal_id + '", "' + salTech.tech_id + '", "' + salTech.tech_name + '", "' + salTech.tech_pic + '", "' + salTech.tech_status + '", "' + salTech.tech_specialty + '", "' + salTech.tech_weekly_offs + '", "' + salTech.tech_off + '", "' + salTech.tech_modify_datetime + '"),';
      salIds += salTech.sal_id + ','
    });
    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    salIds = salIds.slice(0, salIds.length - 1)
    return this.deleteSalTechs(salIds).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, [])
          .then(res => {
            console.log('TechAdd: ' + res.rowsAffected + ' salonHours were dumped');
          })
          .catch(e => {
            console.log(slicedQuery);
            console.log('eror: while inserting into TechAdd' + e.message)
          });
      }
    })
  }


  deleteSalTechs(tech_id) {
    // let deleteCategorires = 'DELETE FROM `service_categories` ';
    let deleteCategorires = 'DELETE FROM salon_technician where tech_id IN (' + tech_id + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {
        console.log("techDeleted", "Deleted")
        return true;
      })
      .catch(e => {
        console.log("techDeleted", "FailedToDelete!")
        return false;
      });
  }

  createSalonHoursTable() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `salon_hours` (`sal_id` int(4),`sal_hours` varchar(64) ,`sal_day` varchar(64) )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table salon_hours', e.message)
      });
  }






  getSalonHoursBySalId(salId) {
    let SalonHours: SalonHour[] = []
    let Query = 'SELECT * FROM salon_hours where sal_id= "' + salId + '" ';
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          let hours: SalonHour = res.rows.item(i)
          SalonHours.push(hours)
        }
        return SalonHours
      })
      .catch(e => {
        return SalonHours = []
      });
  }


  saveIntoSalHoursTable(salHours: SalonHour[]) {

    console.log('insertionTest: ' + salHours.length + ' service_categories were sent for dumping');
    if (!salHours || salHours.length === 0) {
      return
    }

    let fieledsPart = "INSERT INTO `salon_hours` (`sal_id`, `sal_hours`, `sal_day`) VALUES ";
    let ValuesPart = ""
    let salIds = ""
    salHours.forEach(salHour => {
      ValuesPart += '("' + salHour.sal_id + '", "' + salHour.sal_hours + '", "' + salHour.sal_day + '"),';
      salIds += salHour.sal_id + ','
    });
    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    salIds = salIds.slice(0, salIds.length - 1)
    return this.deleteSalHours(salIds).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, [])
          .then(res => {
            console.log('insertionTestHours: ' + res.rowsAffected + ' salonHours were dumped');
          })
          .catch(e => {
            console.log(slicedQuery);
            console.log('eror: while inserting into salonHours' + e.message)
          });
      }
    })
  }
  deleteSalHours(sal_id) {
    // let deleteCategorires = 'DELETE FROM `service_categories` ';
    let deleteCategorires = 'DELETE FROM salon_hours where sal_id IN (' + sal_id + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {
        console.log("SalHours", "Deleted")
        return true;
      })
      .catch(e => {
        console.log("SalHours", "FailedToDelete!")
        return false;
      });
  }


  insert_salon_table(salon: Salon[]) {

    if (!salon || (salon && salon.length === 0)) {

      return
    }

    let firstPart = "INSERT OR IGNORE INTO `salons` (`sal_id`,  `sal_active_promotions`,  `sal_name`, `sal_address`, `sal_city`, `sal_zip`, `sal_contact_person`, `sal_email`, `sal_phone`, `sal_hours`, `sal_hours1`,   `sal_pic`, `sal_profile_pic`,  `sal_status`, `sal_temp_enabled`, `sal_lat`, `sal_lng`, `is_active`, `sal_auto_accept_app`, `sal_future_app_days`, `sal_24clock`,  `sal_specialty`, `sal_facebook`, `sal_instagram`, `sal_twitter`, `sal_reviews`, `sal_rating`, `sal_modify_datetime`, `sal_timing`, `sal_website`, `sal_nearby_zip`, `sal_biography`,  `sal_services`,`distance`,`favsal`,`sty_id`,`sal_app_after_hours` )  VALUES ";
    let secondPart = ""

    salon.forEach(sal => {
      // console.log("SalFutureAppDays",sal.sal_future_app_days)
      let section: Salon = sal
      secondPart += '("'
        + sal.sal_id + '", "'
        + sal.sal_active_promotions + '", "'
        + sal.sal_name + '", "'
        + sal.sal_address + '", "'
        + sal.sal_city + '" , "'
        + sal.sal_zip + '", "'
        + sal.sal_contact_person + '", "'
        + sal.sal_email + '", "'
        + sal.sal_phone + '", "'
        + sal.sal_hours + '", "'
        + sal.sal_hours1 + '", "'
        + sal.sal_pic + '", "'
        + sal.sal_profile_pic + '", "'
        + sal.sal_status + '", "'
        + sal.sal_temp_enabled + '", "'
        + sal.sal_lat + '", "'
        + sal.sal_lng + '", "'
        + sal.is_active + '", "'
        + sal.sal_auto_accept_app + '", "'
        + sal.sal_future_app_days + '", "'
        + sal.sal_24clock + '", "'
        + sal.sal_specialty + '", "'
        + sal.sal_facebook + '", "'
        + sal.sal_instagram + '", "'
        + sal.sal_twitter + '", "'
        + sal.sal_reviews + '", "'
        + sal.sal_rating + '", "'
        + sal.sal_modify_datetime + '", "'
        + sal.sal_timing + '", "'
        + sal.sal_website + '", "'
        + sal.sal_nearby_zip + '", "'
        + sal.sal_biography + '", "'
        + sal.services + '", "'
        + sal.distance + '", "'
        + sal.favsal + '", "'
        + sal.sty_id + '", "'
        + sal.sal_app_after_hours + '"),';
    });
    // });``
    // favsal
    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)


    this.database.executeSql(slicedQuery, []).then(success => {


    }).catch(e => {
      console.log(e.message);
    })
  }

  //key_word


  deleteSearchKeyWordsBySerCatId(serCatIds, type) {
    let deleteSelectedQuery = 'DELETE FROM `search`  where type= "' + type + '" and   ser_image IN (' + serCatIds + ');'


    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        console.log("MessageSqlKeyWord:Success", deleteSelectedQuery)
        return true;
      })
      .catch(e => {
        return false;
      });
  }

  InsertInTblSearchByIndex(SearchDataArray) {
    if (!SearchDataArray || SearchDataArray.length === 0) {
      // this.serviceManager.makeToastOnFailure('SearchDataArray is nulll or undefeind', 'top')
      return
    }
    console.log('insertionTest: ' + SearchDataArray.length + ' search were sent for dumping');
    let firstPart = "INSERT INTO `search` (`keyword`, `type`, `refer_id`,  `ser_image`) VALUES ";
    let secondPart = ""

    let searchKeyWords = ""
    SearchDataArray.forEach(element => {

      secondPart += '("' + element[1] + '", "key_word", "' + element[0] + '", ' + element[2] + '),';
      searchKeyWords += ' "' + element.keyword + '" ' + ','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)

    searchKeyWords = searchKeyWords.slice(0, searchKeyWords.length - 1)

    return this.deleteMultipleSearchKeyWords(searchKeyWords).then(areDuplicateKeyWordsDeleted => {
      if (areDuplicateKeyWordsDeleted) {
        return this.database.executeSql(slicedQuery, [])
          .then(res => {
            console.log('insertionTest: ' + res.rowsAffected + ' search were dumped');
            console.log("SearchInsert:", 'Success');
          })
          .catch(e => {
            // isDataInserted = false
            return e.message
          });

      } else {

      }
    }).catch(e => {

    })

  }



  InsertInTblSearch(SearchDataArray: SALON_SERVICES_SEARCH[]) {
    let isDataInserted = false

    if (!SearchDataArray || SearchDataArray.length === 0) {
      // this.serviceManager.makeToastOnFailure('SearchDataArray is nulll or undefeind: InsertInTblSearchByIndex', 'top')
      return
    }

    let firstPart = "INSERT INTO `search` (`keyword`, `type`, `refer_id`,  `ser_image`) VALUES ";
    let secondPart = ""

    let searchKeyWords = ""
    SearchDataArray.forEach(element => {

      secondPart += '("' + element.keyword + '", "' + element.type + '", "' + element.refer_id + '", "' + element.image_url + '"),';
      searchKeyWords += ' "' + element.keyword + '" ' + ','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)

    searchKeyWords = searchKeyWords.slice(0, searchKeyWords.length - 1)

    return this.deleteMultipleSearchKeyWords(searchKeyWords).then(areDuplicateKeyWordsDeleted => {
      if (areDuplicateKeyWordsDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' search were dumped');
        })
          .catch(e => {

            // isDataInserted = false
            return e.message
          });

      } else {

      }
    }).catch(e => {


    })

  }


  insert_saerch_table_new(SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]) {

    if (!SALON_SERVICE_SEARCH || SALON_SERVICE_SEARCH.length === 0) {
      return
    }

    // let firstPart = "INSERT OR IGNORE INTO `salons` (`sal_id`, `sal_name`, `sal_address`, `sal_city`, `sal_zip`, `sal_contact_person`, `sal_email`, `sal_phone`, `sal_hours`, `sal_pic`, `sal_profile_pic`,  `sal_status`, `sal_temp_enabled`, `sal_lat`, `sal_lng`, `is_active`, `sal_auto_accept_app`, `sal_future_app_days`, `sal_24clock`,  `sal_specialty`, `sal_facebook`, `sal_instagram`, `sal_twitter`, `sal_reviews`, `sal_rating`, `sal_modify_datetime`, `sal_timing`, `sal_website`, `sal_nearby_zip`, `sal_biography`,  `sal_services`,`distance`,`favsal`)  VALUES ";
    // let secondPart = ""


    let index = 0;
    let InsertQryData = "";
    SALON_SERVICE_SEARCH.forEach(element => {
      let isert_qury_iner = "('" + element.keyword + "','" + element.type + "','" + element.refer_id + "','" + element.image_url + "')"
      if (InsertQryData.length == 0) {
        InsertQryData = isert_qury_iner
      } else {
        InsertQryData = InsertQryData + "," + isert_qury_iner
      }
      if (index % 10000 == 0 && index != 0) {
        this.insertSearchTableByString(InsertQryData);
        InsertQryData = "";
      }
      index = index + 1;
    });
    if (InsertQryData.length != 0) {
      this.insertSearchTableByString(InsertQryData);
    }
  }
  insertSearchTableByString(InsertQryData) {
    let Query = "INSERT INTO " + this.SQL_TABLE_SEARCh + " (" + this.SQL_SR_KEYWORD +
      ", " + this.SQL_SR_TYPE + ", " + this.SQL_SR_REFER_ID + ", " + this.SQL_SR_IMAGE + ") VALUES " + InsertQryData + " "

    return this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }
  insertSalonTableByString(InsertQryData) {
    let Query = "INSERT INTO " + this.SQL_TABLE_SALON + " (" + this.SQL_SAL_ID +
      ", " + this.SQL_SAL_NAME + ", " + this.SQL_SAL_PHONE + ") VALUES " + InsertQryData + " "

    return this.database.executeSql(Query, [])
      .then(res => {
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }

  deleteSearchDataByType(type) {
    let Query = 'DELETE FROM search where type= ' + type
    return this.database.executeSql(Query, [])
      .then(res => {
        console.log('success: search services deleted ' + res.rowsAffected);
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: error while deleting services ' + e.message);

        return false
      })
  }
  deleteSearchData() {
    let Query = 'DELETE FROM search'
    this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });

  }

  deleteAllData() {
    let QueryDeleteSalons = 'DELETE FROM salons'
    let QueryDeleteSearch = 'DELETE FROM search'

    this.database.executeSql(QueryDeleteSalons, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, QueryDeleteSalons)
      });

    this.database.executeSql(QueryDeleteSearch, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, QueryDeleteSearch)
      });

  }

  deleteAllSalons() {
    let Query = 'DELETE FROM salons'
    this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });

  }

  deleteSalonTechnician() {
    let Query = 'DELETE FROM  salon_technician '
    this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });

  }

  deletesalon_hours() {
    return this.database.executeSql('DELETE FROM  salon_hours ', [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, 'DELETE FROM  salon_hours ')
      });

  }

  deletesal_ser_sub_categories() {
    return this.database.executeSql('DELETE FROM  sal_ser_sub_categories ', [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, 'DELETE FROM  sal_ser_sub_categories ')
      });
  }

  fetch_search_table_by_refer_id(referId): any {
    this.SALON_SERVICE_SEARCH = []
    let Query = 'SELECT * FROM ' + this.SQL_TABLE_SEARCh + ' where refer_id= "' + referId + '" '
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          let item = { keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).image_url }
          this.SALON_SERVICE_SEARCH.push(item)
        }
        return this.SALON_SERVICE_SEARCH;

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)

      });
  }

  fetch_search_table_all(): any {
    this.SALON_SERVICE_SEARCH = []
    let Query = 'SELECT * FROM ' + this.SQL_TABLE_SEARCh + ' '

    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {

          let item = { keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).image_url }
          this.SALON_SERVICE_SEARCH.push(item)
        }
        return this.SALON_SERVICE_SEARCH;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      }
      );

  }


  fetch_search_table(): any {
    this.SALON_SERVICE_SEARCH = []
    let Query = 'SELECT * FROM ' + this.SQL_TABLE_SEARCh + ' where type="service" '
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {

          let item = { keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).image_url }
          this.SALON_SERVICE_SEARCH.push(item)
        }

        return this.SALON_SERVICE_SEARCH;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }


  getAllSalonsFromDB() {
    let myAllSalons: Salon[] = []
    let getSalonQuery = "SELECT * FROM `salons`  order by distance asc"
    return this.database.executeSql(getSalonQuery, []).then(res => {
      for (var i = 0; i < res.rows.length; i++) {
        let Salon: Salon = res.rows.item(i)
        myAllSalons.push(Salon)
      }
      return myAllSalons
    }).catch(e => {
      this.serviceManager.sendErrorToServer(e.message, getSalonQuery)
      return myAllSalons
    })
  }
  getAllSalonsByTypeFromDB(typeId, sal_id, distance) {
    let myAllSalons: Salon[] = []
    let selectQuery = ''
    if (distance === 0) {
      selectQuery = 'SELECT sal.*  FROM salons as sal  where   sal.sty_id= "' + typeId + '" ORDER BY distance ASC, sal.sal_id DESC  limit 4'
    }
    else {
      selectQuery = 'SELECT sal.* FROM salons as sal  where  sal.sty_id= "' + typeId + '"  and ((sal.distance = "' + distance + '" and sal.sal_id < ' + sal_id + ') or sal.distance > "' + distance + '") ORDER BY sal.distance ASC, sal.sal_id DESC limit 8'
    }
    console.log('' + selectQuery)
    return this.database.executeSql(selectQuery, []).then(res => {
      console.log('total-salons' + res.rows.length)
      for (var i = 0; i < res.rows.length; i++) {
        let Salon: Salon = res.rows.item(i)
        if (Salon.fav_id === undefined || Salon.fav_id === null) {
          Salon.fav_id = 0
        }
        myAllSalons.push(Salon)
      }
      return myAllSalons
    }).catch(e => {
      console.log('error', e.message)
      this.serviceManager.sendErrorToServer(e.message, selectQuery)

      return myAllSalons
    })
  }


  getAllSalonsForTips(sal_id) {
    let myAllSalons: Salon[] = []
    let selectQuery = ''

    selectQuery = 'SELECT sal.*  FROM salons as sal   where   sal.sal_id= "' + sal_id + '" ORDER BY distance ASC, sal.sal_id DESC  limit 1'

    console.log('' + selectQuery)
    return this.database.executeSql(selectQuery, []).then(res => {
      console.log('total-salons' + res.rows.length)
      for (var i = 0; i < res.rows.length; i++) {
        let Salon: Salon = res.rows.item(i)
        if (Salon.fav_id === undefined || Salon.fav_id === null) {
          Salon.fav_id = 0
        }
        myAllSalons.push(Salon)
      }
      return myAllSalons
    }).catch(e => {
      console.log('error', e.message)
      this.serviceManager.sendErrorToServer(e.message, selectQuery)

      return myAllSalons
    })
  }

  searchSalonsByTypeFromDB(typeId, searchValue) {
    let myAllSalons: Salon[] = []
//typeId is not using for now,
    let Query = 'SELECT  sal_name as keyword, sal_id as refer_id, \'salon\' as type  FROM ' + this.SQL_TABLE_SALON + ' where sal_name like "%' + searchValue + '%"  '//AND  sty_id= "' + typeId + '"
    return this.database.executeSql(Query, []).then(res => {
      for (var i = 0; i < res.rows.length; i++) {
        let Salon: Salon = res.rows.item(i)
        myAllSalons.push(Salon)
      }
      return myAllSalons
    }).catch(e => {
      this.serviceManager.sendErrorToServer(e.message, Query)

      return myAllSalons
    })
  }

  localSearchLookingFor(searchKeyWord): any {
    this.SALON_SERVICE_SEARCH = []
    let Query = 'SELECT * FROM search where keyword like "%' + searchKeyWord + '%" limit 10'

    return this.database.executeSql(Query, [])
      .then(res => {

        for (var i = 0; i < res.rows.length; i++) {
          let item = res.rows.item(i) //{ keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).ser_image }
          this.SALON_SERVICE_SEARCH.push(item)
        }
        return this.SALON_SERVICE_SEARCH;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      }
      );
  }

  getSalonsListingFromSqliteDB(salIds) {


    this.SALONS = []
    let Query = 'SELECT sal.*  FROM salons as sal   where sal.sal_id IN (' + salIds + ')';
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          let salon: Salon = res.rows.item(i)
          if (salon.fav_id === undefined || salon.fav_id === null) {
            salon.fav_id = 0
          }

          this.SALONS.push(salon)
        }
        return this.SALONS
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return this.SALONS = []
      });
  }



  deleteMultipleSalon(salIds) {
    this.SALONS = []
    let Query = 'DELETE FROM `salons` where `sal_id` IN (' + salIds + ') '
    return this.database.executeSql(Query, [])
      .then(res => {
        return 1;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return -1;
      });
  }

  deleteMultipleSalServiceSubCat(salIds) {
    this.SALONS = []
    let Query = 'DELETE FROM sal_ser_sub_categories where sal_id IN (' + salIds + ') '
    return this.database.executeSql(Query, [])
      .then(res => {

        return 1;

      })
      .catch(e => {
        return -1;


      });
  }


  getSalonsFromSqliteDB() {

    this.SALONS = []
    let Query = 'SELECT * FROM   salons' + ' '
    return this.database.executeSql(Query, [])
      .then(res => {

        for (var i = 0; i < res.rows.length; i++) {
          let item = { sal_id: res.rows.item(i).sal_id, sal_name: res.rows.item(i).sal_name, sal_address: res.rows.item(i).sal_address }
          this.SALONS.push(item)
        }


        return this.SALONS
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });


  }

  getSalonServicesFromSqliteDB() {
    let SalonsServices: SALON_SERVICES_SEARCH[] = []
    let Query = 'SELECT * FROM search WHERE type="service" OR type="salon"'
    return this.database.executeSql(Query, [])
      .then(res => {

        for (var i = 0; i < res.rows.length; i++) {
          let item: SALON_SERVICES_SEARCH = {
            keyword: res.rows.item(i).keyword,
            type: res.rows.item(i).type,
            refer_id: res.rows.item(i).refer_id,
            image_url: res.rows.item(i).image_url
          }
          SalonsServices.push(item)
        }
        return SalonsServices
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return SalonsServices = []
      });
  }



  getSalonDetailsFrom(sal_id: string) {
    let SelectedSalons: Salon
    let Query = 'SELECT * FROM salons WHERE sal_id= "' + sal_id + '" '
    return this.database.executeSql(Query, [])
      .then(salons => {

        if (salons.rows.length > 0) {
          SelectedSalons = salons.rows.item(0)
        }
        return SelectedSalons
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return SelectedSalons

      });
  }
  getAllSalonIDs() {
    let salonIDS = ''
    let Query = 'SELECT sal_id FROM  salons'
    return this.database.executeSql(Query, []).then(res => {
      for (var i = 0; i < res.rows.length; i++) {
        salonIDS += res.rows.item(i).sal_id + ','

      }
      if (salonIDS.length > 0) {
        salonIDS = salonIDS.slice(0, salonIDS.length - 1)
      }
      return salonIDS
    }).catch(e => {
      this.serviceManager.sendErrorToServer(e.message, Query)
      return salonIDS = ''

    })
  }
  deleteMultipleSearchKeyWords(keyWords) {

    let Query = 'DELETE FROM `search`  where keyword IN (' + keyWords + ');'

    return this.database.executeSql(Query, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return false;
      });
  }


  deleteSearchKeyWordsBySalId(salIds, type) {
    let Query = 'DELETE FROM `search`  where type= "' + type + '" and   refer_id IN (' + salIds + ');'


    return this.database.executeSql(Query, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log("MessageSql:Failure", e.message);
        return false;
      });
  }



  deleteSearchBySalIdFromService(salIds, type) {


    let Query = 'Delete from  `search` where type= "' + type + '" and refer_id in (select ser_id from salon_service where sal_id in (' + salIds + '))';

    return this.database.executeSql(Query, [])
      .then(res => {

        console.log("MessageSql:Success", + ":" + res.rows.length)
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log("MessageSql:Failure", e.message);
        return false;
      });
  }
  dropTableWithName(tblName) {
    let Query = 'DROP TABLE "' + tblName + '" ;'

    return this.database.executeSql(Query, [])
      .then(res => {

        return true;
      })
      .catch(e => {

        this.serviceManager.sendErrorToServer(e.message, Query)
        return e.message;
      });
  }
  makeSalonFavorite(sal_id, favUnFav) {
    let Query = 'UPDATE salons SET favsal =' + favUnFav + ' where sal_id = ' + sal_id;

    return this.database.executeSql(Query, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)

        return false;
      });
  }
  getFavoriteSalon() {

    let myFavoriteSalons: Salon[] = []
    // 'select sc.*,  af.fav_id from `category_sub_style` as  sc, all_favorites af   where sc.ip_id = af.fav_id  ORDER BY  update_date DESC limit 3'

    let Query = 'SELECT sal.*   FROM salons as sal order by  sal.distance DESC';

    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {

          let salon: Salon = res.rows.item(i)

          if (salon.fav_id === undefined || salon.fav_id === null) {
            salon.fav_id = 0
          }
          myFavoriteSalons.push(salon)
        }
        return myFavoriteSalons;
      })
      .catch(e => {

        this.serviceManager.sendErrorToServer(e.message, Query)
        return myFavoriteSalons;
      });
  }


}
