export interface Salon {
    sal_id?: string,
    sal_name?: string,
    sal_address?: string,
    sal_city?: string,
    sal_zip?: string,
    sal_contact_person?: string,
    sal_email?: string,
    sal_phone?: string,
    sal_hours?: string,
    sal_hours1?: string,
    sal_hours2?: string,
    sal_pic?: string,
    sal_profile_pic?: string,
    sal_created_datetime?: string,
    sal_status?: Boolean,
    sal_password?: string,
    admin_device_id?: string,
    admin_device_type?: string,
    sal_lat?: string,
    sal_lng?: string,
    is_active?: Boolean,
    sal_auto_accept_app?: number,
    sal_future_app_days?: string,
    sal_24clock?: Boolean,
    sal_queue_status?: Boolean,
    sal_specialty?: "",
    sal_facebook?: string,
    sal_instagram?: string,
    sal_twitter?: string,
    sal_reviews?: string,
    sal_rating?: string,
    sal_modify_datetime?: string,
    sal_timing?: string,
    sal_website?: string,
    sal_nearby_zip?: string,
    sal_active_promotions?: "1",
    sal_is_default?: Boolean,
    sal_search_words?: string,
    sal_biography?: string,
    services?: SalonServices[],
    categories?: Category[],
    offers?: OFFERS[],
    queue?: [{}],
    pending_appointments_count?: number,
    sal_techs?: Sal_Techs[],
    techs?: Sal_Techs[],
    distance?: number,
    matched?: string,
    favsal?: number,
    sal_temp_enabled?: string,
    sty_id?: string,
    pinRelatedServices?: Services[],
    sal_app_after_hours?: number,
    fav_id?: number,

}

export interface SalonTechnicain {
    sal_id?: number,
    tech_id?: number,
    tech_name?: string,
    tech_pic?: string,
    tech_status?: string,
    tech_specialty?: string,
    tech_weekly_offs?: string,
    tech_off?: string,
    tech_modify_datetime?: string,
}



export interface Sal_Techs {
    sal_id: number,
    tech_id: number,
    tech_name: string,
    tech_pic: string,
    tech_slots: [{}],
    tech_phone: string,
    tech_specialty: string,
    tech_bio: string,
    tech_status: Boolean,
    tech_is_active: Boolean,
    tech_off: number,
    categories: Category[],
    tech_services: Services[],
    tech_appointments: Tech_appointments[],
    isSelected?: Boolean,
    tech_modify_datetime?: string,
}



export interface SalonHour {
    sal_hours_id?: number,
    sal_id?: number,
    sal_hours?: string,
    sal_day?: string,

}




export interface Services {
    sser_id: number,
    ser_id: number,
    sser_name: string,
    sal_id: string,
    sser_rate: number,
    sser_time: string,
    sser_enabled: Boolean,
    sser_featured: Boolean,
    ssc_id: number,
    sser_order: number,
    ssc_name: string,
    checked: Boolean,
    ssubc_id: number,
    expanded: boolean,

}

export interface Category {
    ssc_id: number,
    ssc_name: string,
    sal_id: number,
    ssc_is_active: Boolean,
    expanded: boolean,
    services: Services[],
    numberOfServicesSelected: number,

}


export interface Tech_appointments {
    app_id: string,
    app_created: string,
    app_services: string,
    app_price: string,
    app_est_duration: string,
    app_act_start_time: string,
    app_act_end_time: string,
    app_act_duration: string,
    app_start_time: string,
    app_end_time: string,
    app_type: string,
    app_slots: string,
    app_status: string,
    app_out_scheduled: number,
    sal_id: number,
    cust_id: string,
    tech_id: number,
    app_rating: null,
    app_review: string,
    app_review_datetime: string,
    app_notes: string,
    app_review_hide: number,
    so_id: number,
    app_price_without_offer: string,
    app_user_info: string,
    reminded_24hr: string,
    reminded_1hr: string,
    app_last_modified: string,
    cust_name: string,
    cust_pic: string,
    cust_phone: string,
    cust_zip: string,
    pp_id: string,
    p_imageUrl: string,
    ssc_id: number,
}
export interface Settings {
    status: number
}

export interface Notifications {
    app_id: string,
    app_created: string,
    app_services: string,
    app_price: string,
    app_est_duration: string,
    app_act_start_time: string,
    app_act_end_time: string,
    app_act_duration: string,
    app_start_time: string,
    app_end_time: string,
    app_type: string,
    app_slots: string,
    app_status: string,
    app_out_scheduled: string,
    sal_id: number,
    cust_id: string,
    tech_id: string,
    app_rating: number,
}
export interface Add_New_Customer {

    status?: string,
    cust_id?: string,
    cust_pic_name: string,
    sms_message: string,
    sms_status: string,

}

export interface Customer {

    auth_code_expiry?: string
    cust_credits?: string
    cust_datetime?: string
    cust_device_id?: string
    cust_device_type?: string
    cust_email?: string
    cust_gender?: string
    cust_id?: string
    cust_lat?: string
    cust_lng?: string
    cust_name?: string
    cust_phone?: string
    cust_pic?: string
    cust_zip?: string
    referrer_code?: string
    sal_id?: number
    cust_address?: string
    cust_city?: string
    cust_province?: string
    cust_store_credit?: number,

}

// export interface Homepage {
//     Homepage: Homepage[],
// }
export interface Homepage {
    Id: number
    sectionName: string,
    sectionDescription: string,
    otherDescription: string,
    sectionImage: string,
    sectionBackground: string,
    sort_order: string,
    is_header: string,
    view: string,
    lastFetchedDate?: string,
    sectionParameters: string,
    status: string,
    itemBgColour?: string,
    Homepage?: Homepage[],
}

export interface HomepageStats {
    Id: number
    sectionName: string,
    toal_items: string,
}

export interface SalonServices {
    sser_id: string,
    ser_id: string,
    sser_name: string,
    sal_id: string,
    sser_rate: string,
    sser_time: string,
    sser_enabled: string,
    sser_featured: string,
    ssc_id: string,
    sser_order: string,
    ssubc_id: string,
    ssc_name: string,

}


export interface Tech_Slots {
    app_id: string,
    duration_ahead: string,
    tech_id: string,
    ts_date: string,
    ts_duration: string,
    ts_end_time: string,
    ts_id: string,
    ts_is_booked: string,
    ts_number: string,
    ts_start_time: string,
    isSelected: boolean,
}


export interface SALON_SERVICES_SEARCH {
    keyword: string,
    type: string,
    refer_id: string,
    image_url: string,
}

export interface SALON_CATEGORIES_SEARCH {
    keyword: string,
    type: string,
    refer_id: string,
    image_url: string,
}

export interface LATEST_TRENDS_STYLES {
    id: string,
    ssc_image: string,
    ssc_name: string,
}

export interface SALON_SERVICES_SUB_CATEGORIES {
    sssc_id: number,
    sal_id: number,
    ssc_id: number,
}



export interface SAL_SER_SUB_CATEGORIES {
    sssc_id: number,
    sal_id: string,
    ssc_id: number,
}



export interface PINTEREST_OBJECT {
    ip_id: number,
    p_keywords: number,
    sal_id: number,
    ssc_id: number,
    imageUrl: string,
    favpost: number,
    p_setas_editor_img: number,
}

export interface Allfavorite {
    cft_id: number,
    cf_id: number,
    fav_id: number,
    brandName: string,
    image: string,
}

export interface SERVICE_CATEGORY {
    sc_featured: number,
    sc_type: number,
    sc_id: number,
    sc_name: string,
    sc_image: string,
    sc_status: number,
    expanded: boolean,
    sc_gender, number,
    ServiceSubCategories: ServiceSubCATEGORIES[]
    canCollapse?: boolean;
}


export interface FASHION_BRANDS {
    fb_featured: number,
    fb_id: number,
    fb_name: string,
    fb_image: string,
    fb_status: number,
    expanded: boolean,
    fb_gender: number,
    FashionBrandCollections: FASHION_BRANDS_COLLECTION[]
}

export interface FASHION_COLLECTION {
    fc_id: number,
    fc_title: string,
    fc_status: number,
    fc_image: string,
    fc_gender: number,
}

export interface CATEGORY_SUB_STYLE {

    ssc_id: string,
    imageUrl: string,
    p_keywords: number,
    favpost: string,
    p_setas_editor_img: string,
    update_date: string,
    ip_id: string,
    fav_id?: number,
}


export interface FBC_IMAGES {
    fbci_id: number,
    fbci_name: string,
    fbc_id: number,
    fbci_datetime: string,
    fbc_name: string,
    fb_name: string,
    fb_id: number,
    fc_id: number,
    fbc_gender: string,
    fav_id?: number,
}

export interface FB_IMAGES {
    fbci_id: number,
    fbci_name: string,
    fbc_id: number,
    fbci_datetime: string,
    fbc_name: string,
    fb_name: string,
    fb_id: number,
    fc_id: number,
    fbc_gender: string,
    fav_id?: number,
    fbc_modify_datetime?: string,
}
let x: FB_IMAGES = {
    fbci_id: null,
    fbci_name: null,
    fbc_id: null,
    fbci_datetime: null,
    fbc_name: null,
    fb_name: null,
    fb_id: null,
    fc_id: null,
    fbc_gender: null,
    fav_id: 0,

}

export interface FBC_DATES {
    fash_dates_id: number,
    fbc_genders: number,
    fbc_date: string,
    fbc_type: string,

}


// so_id TEXT, so_title TEXT, sot_id TEXT, so_amount TEXT,  so_notification TEXT, so_notification_type TEXT,
// so_is_active TEXT, so_expiry  TEXT, sal_id TEXT, so_days TEXT, so_time TEXT 

export interface OFFERS {
    so_id: string,
    so_title: string,
    sot_id: string,
    so_amount: string,
    so_notification: string,
    so_notification_type: string,
    so_is_active: string,
    so_expiry: string,
    sal_id: string,
    so_days: string,
    so_time: string

}

export interface ServiceSubCATEGORIES {
    ssc_gender: string,
    ssc_id: string,
    ssc_featured: number,
    ssc_image: string,
    ssc_name: string,
    ssc_status: string,
    sc_id: number,
    count: number,
    ssc_keywords: string,
    ssc_modify_datetime: string,

}

export interface FASHION_BRANDS_COLLECTION {
    fbc_modify_datetime: string,
    fbc_gender: string,
    fbc_id: number,
    fbc_featured: number,
    fbc_image: string,
    fbc_name: string,
    fbc_status: string,
    fb_id: number,
    fc_id: number,
    fbc_keywords: string,
    fbc_link: string,
}
//p_setas_editor_img int(10) DEFAULT '0');"
export interface Posts {
    pp_id: number,
    pp_creator: string,
    pp_datetime: string,
    pb_id: number,
    p_urlssc_status: string,
    p_link: string,
    p_imageUrl: string,
    p_pin_id: string,
    p_metadata: string,
    p_keywords: string,
    p_fetch_date: string,
    ssc_id: number,
    p_status: string,
    update_date: string,
    p_setas_editor_img: number,
    admin_id: number,
    favpost: number,
}
export interface SALON_CARD_OPTIONS {
    CardHeight: string,
    calledFrom?: string,
    ShouldShowSalonImage: Boolean,
    isViewPinRelatedSalon?: Boolean,
    shouldShowPromotionTitle?: Boolean,
    shouldScroll?: Boolean,
    shouldNotLoadSalonAgain?: Boolean,
    salon?: Salon[],
    shouldShowQualitifcation? : Boolean,
}
export interface NOPSO_Header_OPTIONS {
    miniHeaderHeight: string,
    calledFrom?: string,
    // ShouldShowSalonImage: Boolean,
    // isViewPinRelatedSalon?: Boolean,
    // shouldShowPromotionTitle?: Boolean,
    // shouldScroll?: Boolean,
    // shouldNotLoadSalonAgain?: Boolean,
}
export interface SALON_PROMOTION {
    sot_id: number,
    so_id: number,
    sal_id: number,
    so_notification_type: string,
    so_days: string,
    so_time: string,
    so_is_active: number,
    so_notification: string,
    so_amount: number,
    so_title: string,
    so_expiry: string,
}
export interface SWITCH_TO_THE_TAB {
    shouldSwitchToAppointmentsTabs?: Boolean,
    shouldSwitchToNotificationsTabs?: Boolean,
    sectionName?: String,
}
export interface APP_CONFIG {
    app_path?: string,
    client_api_path?: string,
    admin_api_path?: string,
    cust_initial_credit?: string,
    environment?: string,
    scraping_interval?: string,
    last_fetch_id?: string,
    last_exported_pin?: string,
    search_radius?: string,
    currency?: string,
    distance_unit?: string,
    delivery_charges?: number,
}
export interface BEAUTY_TIPS {
    btc_id: number,
    btc_name: string,
    btc_image: string,
    btc_status: number,
    btc_created_date: string,
    btc_updated_date: string,
}
export interface TIPS_DETAIL {
    bt_id?: number,
    btc_id?: number,
    bt_title?: string,
    bt_short_description?: string,
    ssc_id?: number,
    bt_status?: number,
    bt_URL?: string,
    bt_description?: string,
    bt_imageUrl?: string,
    bt_datetime?: string,
    bt_modified_date?: string,
    bt_title_urdu?: string,
    bt_description_urdu?: string,
    bt_short_description_urdu?: string,
    bt_short_description_english?: string
    bt_is_featured?: number
}
export interface PRODUCT {
    p_id?: number
    p_name?: string,
    p_description?: string,
    p_image?: string,
    p_status?: string
    p_cost?: number,
    p_rrp?: number,
    p_price?: number,
    p_weight?: string,
    p_dimention?: string,
    pc_id?: string
    pb_id?: number
    p_created_at?: string,
    p_modified_date?: string,
    productImages?: PRODCUT_IMAGES[],
    isAddedtoCart?: boolean,
    isChecked?: boolean,
    p_quantity?: number,
    CalculatedPrice?: number,
    isShowingSeeMoreOptions?: boolean,
    pb_name?: string,
    shouldShowDropDown?: boolean,
    pc_name?: string,
    is_featured? : boolean,
}
export interface PRODUCT_CATEGORY {
    pc_id: number,
    pc_name: string,
    pc_description: string,
    pc_image: string,
    pc_status: number,
    pc_created_at: string,
    pc_modified_date: string,
}
export interface PRODUCT_COLOR {
    pcl_id: number,
    pcl_image: string,
    pcl_value: string,
    pcl_code: string,
    p_id: number,
    pcl_created_at: string,
    pcl_modified_date: string,
}
export interface PRODCUT_IMAGES {
    pi_id: number,
    pi_name: string,
    pi_caption: string,
    pi_datetime: string,
    p_id: number,
    pi_created_at: string,
    pi_modified_date: string,
}
export interface SHIPPING_ADDRESS_DATA {
    o_id?: number,
    o_address?: string,
    o_city?: string,
    o_province?: string,
    o_completeAddress?: string,
    isdefaultaddress?: boolean,
    checked?: boolean,
    o_contact_person?: string,
    o_phone?: string,
}
export interface MY_ORDERS {
    o_id?: string,
    cust_id?: string
    o_address?: string,
    o_city?: string,
    o_province?: string
    o_amount?: string,
    o_datetime?: string,
    o_delivery_charges?: string,
    o_status?: string,
    o_created_at?: string,
    o_modified_date?: string,
    order_items?: PRODUCT[]
    o_contact_person?: string,
    o_phone?: string,
}
export interface ORDER_ITEMS {
    od_id: string,
    od_quantity: string,
    od_price: string,
    od_cost: string,
    od_rrp: string,
    p_id: string,
    o_id: string,
    od_created_at: string,
    od_modified_date: string,
    p_name: string,
    p_image: string,
    isChecked?: boolean,
}
export interface AUTH_CODE_VALIDATION {
    wrongAuthCodeEnteredCount?: number,
    remainingUnblockTime?: string,
    isUserBlocked?: boolean,
    serverTimeWhenUserWasBlocked?: string,
    currentServerTime?: string,
    resendAuthCodeCount?: number,
}
export interface PRODUCT_BRANDS {
    pb_id?: number,
    pb_name?: string,
    pb_image?: string,
    pb_manufacturer?: string,
    pb_status?: string,
    pv_id?: string,
    pb_created_at?: string,
    pb_modified_date?: string,
}

export interface PROMOTIONS {
    pc_id?: number,
    pc_title?: string,
    pc_code?: string,
    pc_max_value?: number,
    pc_percent?: number,
    pc_expiry?: string,
    pc_total?: number,
    pc_used?: number,
    pc_balance?: number,
    cust_id?: number,
}

