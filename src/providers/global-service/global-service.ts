import { Injectable } from '@angular/core';

@Injectable()
export class GlobalServiceProvider {
 // public tabIndex: string = "0";
  public search_keyword: string = "";
  public sty_id: string = "";
  public currencySymbol?= ''
  public distance_unit?= ''
  public isFirstlaunch: boolean=true
}
