import { config } from '../config/config';
import { Storage } from '@ionic/storage';

import { Http, Headers, RequestOptions, HttpModule } from '@angular/http';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { SALON_SERVICES_SEARCH } from '../SalonAppUser-Interface';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { configJazzCash, JAZZ_Cash_INTERFACE } from "../../providers/configJazzCash/configJazzCash";


//import { Salon } from '../../providers/salon-interface'

import {
  ToastController, LoadingController, Platform

} from 'ionic-angular';
import { Loading } from 'ionic-angular/components/loading/loading';
import { MainHomePage } from '../../pages/main-home/main-home';
import { NavController } from 'ionic-angular/navigation/nav-controller';
const API: string = config.BaseURL //"https://nopso.qwpcorp.com/salon/json58.php?json=";
const NOTI: string = "https://nopso.qwpcorp.com/salon/push_test_1.php";
@Injectable()
export class GlobalProvider {
  isHideTab: string;
  TabsIndex: string = "";
  api_key: string = "";


  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  headers: Headers
  options: RequestOptions
  /*
 *  user app  Global Constants
 */
  CUSTOMER_DATA = 'customer'
  LAST_UPDATE_DATE_TIME = 'last_update_time'
  PRIVACY_POLICY='privacy_policy'
  IS_APP_DATA = 'is_app_data'
  IS_USER_LOGIN = 'isLogin'
  PUBLIC_USER_ID='pub_user_id'
  IS_SPLAH_SHOWN='is_shown_splash'
  SP_VIEW_STRIP='sp_view_strip'
  LATEST_TRENDS_DESCRIPTION = 'latest_trends_description'


  SHOULD_SHOW_ENGLISH_POSTS = 'shouldShowPostsInEnglish'
  BEAUTY_TIPS_LAST_FETCHED_DATE = 'BeautyTipsFetchedDate'
  TIPS_DETAILS_LAST_FETCHED_DATE = 'TipsDetailsFetchedDate'
  PRODUCT_CATEGORIES_LAST_FETCHED_DATE = 'ProductCategoriesFetchedDate'
  PRODUCT_BRANDS_LAST_FETCHED_DATE = 'ProductBrandsFetchedDate'
  PRODUCTS_LAST_FETCHED_DATE = 'ProductsFetchedDate'
  FASH_BRAND_DETAIL_LAST_FETCHED_DATE = 'fash_brand_detail_ls_fetch'
  /*
   *  user app  Global Constants
   */


  loader: any
  alreadyLoading: boolean = false
  toastAlreadyShowing = false
  DEVICE_DATETIME = 'device_datetime'
  LOGED_IN_SALON_DATA = 'salon_data'
  SALON_TECHS = 'salon_techs'
  LOGED_IN_SALON_OFFERS = 'salon_offers'
  LOGED_IN_SALON_PROFILE_IMG = 'salon_profile_img'

  LOGED_IN_SALON_TECHS_SERVICES = 'techs_services'
  LOGED_IN_SALON_SALON_SETTINGS = 'salon_Settings'
  GCM_TOKEN = 'gcmToken'
  INVALID_POSTCODE = 'invalid postcode'
  SLOW_INTERNET_ERROR = 'Cannot reach server due to no/slow internet. Please try after a while.'
  NO_POST_FOR_SUBSTYLE_FOUND = 'no post for this sub-style'
  OBJ_SUB_STYLE = 'subStyle_object'
  OBJ_PIN = 'pin_object'
  Customer_Gender = ''
  CUSTOMER_GENDER_MALE = '1'
  CUSTOMER_GENDER_FEMALE = '2'
  NEAREST_SALON_ASSOCIATED_WITH_PIN = 'sal_associated_with_pin'
  SALONS_NEAR_CUSTOMER = 'salo_near_customer'
  // SALON_DATA='salons'
  LAST_FETCH_DATE_SALONS = 'salon_last_fetched_on'
  NEAREST_SALON_SERVICES = 'sal_services'
  APP_STATUS_SERVING = 'serving'
  APP_STATUS_FINISHED = 'finished'
  APP_STATUS_ACCEPTED = 'accepted'
  APP_STATUS_REJECTED = 'rejected'
  APP_STATUS_DELETED = 'deleted'
  APP_STATUS_PENDING = 'pending'
  ACCEPT_BUTTON_TEXT = 'Accept'
  REMOVE_BUTTON_TEXT = 'Cancel'
  REJECT_BUTTON_TEXT = 'Reject'
  RESCHEDULE_BUTTON_TEXT = 'Reschedule'



  SUCCESS_STATUS = '1'
  FAILURE_STATUS = '0'
  IS_LOGGED_IN = 'looged_in'
  LOGGED_IN_SALON_ID = 'logged_in_salon_id'
  LOGGED_IN_SALON_NAME = 'logged_in_salon_name'
  LOGGED_IN_SALON_STATUS = 'logged_in_salon_status'
  LOGGED_IN_SALON_ADDRESS = 'logged_in_salon_address'

  SAL_24CLOCK = 'sal_24clock'
  SAL_AUTO_ACCEPT_APP = 'sal_auto_accept_app'
  SAL_FUTURE_APP_DAYS = 'sal_future_app_days'
  NO_MATCH_FOUND = 'No match found.'

  NO_APPOINTMENTS = 'No appointment exists.'
  

  IS_MY_ORDER_REQUESTED_ALREADY_INITIATED = 'is_my_order_First_Call'
  IS_PRODUCTS_FIRST_LAUNCH = 'is_product_first_Call'
  
  IS_FORCE_UPDATE_AVAILABLE = 'IS_FORCE_UPDATE_AVAILABLE'
  FORCE_UPDATE_MESSAGE = 'FORCE_UPDATE_MESSAGE'


  constructor(
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public datePipe: DatePipe,
    public http: Http,
    public storage: Storage,

    public nativePageTransitions: NativePageTransitions,


  ) {
    this.storage.get('api_key').then((value) => {
      if (value != undefined && value != null) {
        this.api_key = value;
        // this.base64Image = value;//MainHomePage;
      }
    })


    this.isHideTab = "0";
    this.loader = this.loadingCtrl.create({
    });
    this.setUpHeaderAndOptions()
  }

  // CUSTOME FUNCTIONS

  getDateFormattedForAppointmentConfirmation(dateString) {
    return this.datePipe.transform(dateString, 'dd MMMM yyy');
  }
  AvailableSlotsFor(date: any): string {
    let selectedDate = date.replace(/-/g, '/');
    return this.datePipe.transform(selectedDate, 'dd MMMM');
  }
  getFormattedTimeForMyorder(date: any): string {
    let selectedDate = date.replace(/-/g, '/');
    return this.datePipe.transform(selectedDate, 'dd/MM/yyyy');
  }
  getFormattedDate(appointmentTime) {
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (appointmentTime === '0000/00/00 00:00:00') {
      return 'N/A';
    }
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') {
      return this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      return this.datePipe.transform(appointmentTime, 'EEE MMM dd (HH:mm)');
    } else { return appointmentTime }
  }

  CalendarDateFormat(appointmentTime) {
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (appointmentTime === '0000/00/00 00:00:00') {
      return 'N/A';
    }
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') {
      return this.datePipe.transform(appointmentTime, 'EEE MMM dd yyyy HH:mm:ss');
    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      return this.datePipe.transform(appointmentTime, 'EEE MMM dd yyyy HH:mm');
    } else { return appointmentTime }
  }

  getFormattedSlotTime(time) {
    // 13:30
    const timePart: any[] = time.split(':')
    let timeString = ''

    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') { // 12 hours format
      if (timePart.length === 2) {
        timeString = time
      } else if (timePart.length === 3) {
        timeString = timePart[0] + ':' + timePart[1]
      }
      let hours = timeString.split(':')[0];
      const suffix = Number(hours) >= 12 ? 'PM' : 'AM';
      const hour = ((Number(hours) + 11) % 12 + 1);
      if (hour < 10) {
        return '0' + hour + ':' + time.split(':')[1] + ' ' + suffix;
      }

      return hours = hour + ':' + time.split(':')[1] + ' ' + suffix;

    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      if (timePart.length === 2) {
        return time
      } else if (timePart.length === 3) {
        return timeString = timePart[0] + ':' + timePart[1]
      }
    }
  }

  getFormattedTime(time) {
    // 2018-01-11 11:00:00
    const extractedTime = time.split(' ')[1]
    if (extractedTime === undefined) {
      return time
    }
    const timePart: any[] = extractedTime.split(':')
    let timeString = ''
    if (this.getFromLocalStorage(this.SAL_24CLOCK) === '0') { // 12 hours format
      if (timePart.length === 2) {
        timeString = extractedTime
      } else if (timePart.length === 3) {
        timeString = timePart[0] + ':' + timePart[1]
      }
      let hours = timeString.split(':')[0];
      const suffix = Number(hours) >= 12 ? 'PM' : 'AM';
      const hour = ((Number(hours) + 11) % 12 + 1);
      if (hour < 10) {
        return '0' + hour + ':' + time.split(':')[1] + ' ' + suffix;
      }

      return hours = hour + ':' + time.split(':')[1] + ' ' + suffix;

    } else if (this.getFromLocalStorage(this.SAL_24CLOCK) === '1') {
      if (timePart.length === 2) {
        return extractedTime
      } else if (timePart.length === 3) {
        return timeString = timePart[0] + ':' + timePart[1]
      }
    }
  }

  getCurrentDeviceDateTime(): string {
    const format = 'yyyy-MM-dd HH:mm:ss'
    const currentDate = new Date();

    return this.datePipe.transform(currentDate, format)

  }

  getCurrentDeviceDate(date: string): string {

    const format = 'yyyy-MM-dd'

    let currentDate: Date

    if (date.toString().trim().length === 0) {
      currentDate = new Date()
    } else {
      currentDate = new Date(date);
      if (currentDate === undefined) {
        return ''
      }
    }
    if (currentDate === undefined) {
      return ''
    }
    return this.datePipe.transform(currentDate, format)

  }

  makeToastOnSuccess(message: string) {
    if (this.toastAlreadyShowing) { return false }
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top',
      dismissOnPageChange: false,
      cssClass: 'customToastClassz'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.toastAlreadyShowing = false
    });
    toast.present();
    this.toastAlreadyShowing = true
  }

  makeToastOnFailure(message: string) {
    if (this.toastAlreadyShowing) { return false }
    const toast = this.toastCtrl.create({
      message: message,
      duration: 4000,
      position: 'top',
      dismissOnPageChange: false,
      cssClass: 'failureToastClass'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
      this.toastAlreadyShowing = false
    });


    toast.present();
    this.toastAlreadyShowing = true


  }
  getLoggedInSalonID(): string {
    let loggedInsalon_ID = ''
    loggedInsalon_ID = this.getFromLocalStorage(this.LOGGED_IN_SALON_ID)
    if (loggedInsalon_ID !== undefined) {
      return loggedInsalon_ID
    }
    // let _salon_data = localStorage.getItem(this.LOGED_IN_SALON_DATA)
    // if (_salon_data == undefined) {
    //   return ''
    // }
    // let loggedInSalonData: Salon = JSON.parse(_salon_data)
    // loggedInsalon_ID = loggedInSalonData.sal_id
    // if (loggedInsalon_ID != undefined) {
    //   return loggedInsalon_ID
    // } else {
    //   return loggedInsalon_ID
    // }
  }
  showProgress() {
    if (this.alreadyLoading) { return false }
    console.log('alread Loading: ', this.alreadyLoading);
    console.log('loader started');

    this.loader = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loader.present()
    this.alreadyLoading = true

    if (this.alreadyLoading) {
      setTimeout(() => {
        try {
          console.log('alread Loading: ', this.alreadyLoading);
          if (this.alreadyLoading === true) {
            this.loader.dismiss()
            this.alreadyLoading = false
            console.log(' loader dissmissed in Time out')
          }
          console.log('alread Loading: ', this.alreadyLoading);
        }
        catch (Exception) {
          console.log('cannot dismiss loader ');
        }
      }, 15000);
    }

  }
  stopProgress() {
    if (!this.alreadyLoading) { return false }
    try {
      console.log('alread Loading: ', this.alreadyLoading);
      console.log('loader dismissed');
      this.loader.dismiss()
      this.alreadyLoading = false
    }
    catch (Exception) {
      console.log('cannot dismiss loader ');
    }
  }

  setInLocalStorage(key: string, Value: any) {
    localStorage.setItem(key, JSON.stringify(Value))
  }
  getFromLocalStorage(key: string): any {
    const obj = localStorage.getItem(key)
    if (obj === undefined) {
      return obj
    }
    return JSON.parse(obj)
  }

  removeItemFromStorage(postsKey: string, paginateKey: string, startTimeKey: string) {
    localStorage.removeItem(postsKey);
    localStorage.removeItem(paginateKey);
    localStorage.removeItem(startTimeKey);

  }
  removeFromStorageForTheKey(obj: string) {
    localStorage.removeItem(obj);
  }
  // getDataFromLaravelService(url) {
  //   return this.http.get(url, this.options)
  //     .map(response => response.json());
  // }
  getData(params) {
    console.log('without encoded');
    console.log(config.BaseURL + params);

    // const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(JSON.stringify(params))
    
    console.log(config.BaseURL + percentEncodedJsonData);
    
    return this.http.post(config.BaseURL + percentEncodedJsonData, this.options)
      .map(Response => Response.json());

  }

  getDataJazCash(jazzCashUrl, jazzCashData) {
    let url = 'https://sandbox.jazzcash.com.pk/payaxisapplicationapi/api/Payment/DoTransaction'
    let parmm = {
      "pp_Version": "1.1",
      "pp_TxnType": "MWALLET",
      "pp_Language": "EN",
      "pp_MerchantID": "zaheerhussain@gmail.com",
      "pp_SubMerchantID": "",
      "pp_Password": "nopso2016",
      "pp_BankID": "",
      "pp_ProductID": "",
      "pp_TxnRefNo": "T20181029203627",
      "pp_Amount": "100",
      "pp_TxnCurrency": "PKR",
      "pp_TxnDateTime": "20181029203729",
      "pp_BillReference": "billRef",
      "pp_Description": "Description",
      "pp_TxnExpiryDateTime": "20181030203729",
      "pp_ReturnURL": "www.beautyapp.pk",
      "pp_SecureHash": "a966a8231bf0ba82b500f21a570a4ef497e110e6e27f84a45ff3771fbf6fc968",
      "ppmpf_1": "03047660078",
      "ppmpf_2": "",
      "ppmpf_3": "",
      "ppmpf_4": "",
      "ppmpf_5": ""
    }
    // const jsonData = JSON.stringify(parmm)
    //const percentEncodedJsonData = encodeURIComponent(jsonData)
    

    return this.http.post(jazzCashUrl + jazzCashData, this.options)
      .map(Response => Response.json());

  }
  sendErrorToServer(errorMessage: string, query: string) {
    let params = {
      service: btoa('app_debug'),
      debug_msg: btoa(errorMessage),
      debug_qry: btoa(query)
    }
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    console.log('encoded ', config.BaseURL + percentEncodedJsonData);

    return this.http.post(config.BaseURL + percentEncodedJsonData, this.options)
      .map(Response => Response.json()).subscribe(myResponse => {

        return myResponse
      });

  }
  setUpHeaderAndOptions() {
    this.headers = new Headers();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'beautyapp.pk');
    this.headers.append('Access-Control-Allow-Methods', 'GET,POST')
    this.headers.append('Access-Control-Allow-Headers', 'Authorization, Content-Type')

    this.options = new RequestOptions({ headers: this.headers });
  }

  getCustomerData(): any {
    this.storage.get('customer').then((customer) => {
      return customer;
    }).catch((Error) => {
      console.log('Error', 'in login');
    });

  }

  //zahid
  getProducts() {
    return this.http.get('/assets/data.json')
      .map(response => response.json());
  }

  setTabsIndex(value) {
    this.TabsIndex = value;
  }
  getTabsIndex() {
    return this.TabsIndex;
  }

  setTabBar(value) {
    this.isHideTab = value;
  }
  getTabbar() {
    return this.isHideTab;
  }

  getTokenAndCallApi() {
  }

  test() {
    console.log("Test is called...");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers });
    return this.http.post("http://nopso.qwpcorp.com/apis_pk/salonionic/search_tag.php?tag=party", options)
      .map(response => response.json());
  }

  getNotifications(params) {

    console.log("Notification service called");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({ headers: headers });
    let jsonData = JSON.stringify(params)

    console.log('URL: ', API + jsonData);
    return this.http.post(API + jsonData, options)
      .map(response => response.json());
  }

  getDataFromLaravelService(url) {
    console.log("Laravel service called");
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Access-Control-Request-Headers', '*');

    let options = new RequestOptions({ headers: headers });

    //  console.log(NOTI)
    console.log(url);
    //  return  this.http.post(NOTI  , options)
    console.log('URL =>', url);
    console.log('***************************************');
    return this.http.get(url, options)
      .map(response => response.json());
  }

  uploadImageToServer(params, base64Image) {
    let headers = new Headers(
      {
        'Content-Type': 'application/json'
      });
    let options = new RequestOptions({ headers: headers });

    let data = JSON.stringify({
      cust_pic: base64Image,
    });

    let jsonData = JSON.stringify(params)
    return new Promise((resolve, reject) => {
      this.http.post(API + jsonData, data, options)
        .toPromise()
        .then((response) => {
          console.log('API Response : ', response.json());
          resolve(response.json());
        })
        .catch((error) => {
          console.error('API Error : ', error.status);
          console.error('API Error : ', JSON.stringify(error));
          reject(error.json());
        });
    });
  }

  getCurrentDateTime() {
    let date = new Date();
    let dateNow = this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
    // console.log("dateHere", dateNow);
    return dateNow;
  }
  // presentToast(message: string, color: number) {
  //   let cssClass = "customToastClassz";
  //   if (color == 0) {
  //     cssClass = "failureToastClass";
  //   } else {
  //     cssClass = "customToastClassz";
  //   }
  //   let toast = this.toastCtrl.create({
  //     message: message,
  //     duration: 4000,
  //     position: "top",
  //     dismissOnPageChange: false,
  //     cssClass: cssClass
  //   });
  //   toast.onDidDismiss(() => {
  //     console.log('Dismissed toast');
  //   });
  //   toast.present();

  // }

  getStringFromDateWithFormat(date,format) :string{
    return this.datePipe.transform(date,format)
  }
  get12HourFormatFrom24Hour(time){
    let hours = time.split(':')[0];
    var suffix = Number(hours) >= 12 ? "PM":"AM"; 
    var hour =  ((Number(hours) + 11) % 12 + 1);
    if (hour < 10){
      return "0" + hour +':'+time.split(':')[1]+' '+ suffix;
    }
    return hours = hour +':'+time.split(':')[1]+' '+ suffix;
  }
  get12HourFormatFrom24HourWithoutSuffix(time){
    let hours = time.split(':')[0];
    var hour =  ((Number(hours) + 11) % 12 + 1);
    if (hour < 10){
      return "0" + hour +':'+time.split(':')[1]+' ';
    }
    return hours = hour +':'+time.split(':')[1]+' ';
  }
  get12HourFormatSuffix(time){
    let hours = time.split(':')[0];
    var suffix = Number(hours) >= 12 ? "PM":"AM"; 
  
    return   suffix;
  }
}
