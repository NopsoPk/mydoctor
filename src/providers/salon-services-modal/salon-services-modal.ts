// import {  } from './../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Services, SALON_SERVICES_SUB_CATEGORIES, SalonServices, ServiceSubCATEGORIES, Salon, SALON_SERVICES_SEARCH, Category, SERVICE_CATEGORY, OFFERS, CATEGORY_SUB_STYLE, SalonHour, FASHION_BRANDS } from './../../providers/SalonAppUser-Interface';
import { GlobalProvider } from './../../providers/global/global';
import { BehaviorSubject } from "rxjs/Rx";
import { Storage } from "@ionic/storage";
import { first } from '../../../node_modules/rxjs/operators';


@Injectable()
export class SalonServicesModal {

  /* Global Vairables */
  database: SQLiteObject
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]

  public SalonServices: SalonServices[]

  public SALONS: Salon[]
  public SalonHours: SalonHour[]
  public ServiceSubCATEGORIE: ServiceSubCATEGORIES[]
  private databaseReady: BehaviorSubject<boolean>;
  // SQL_TABLE_SAL_SERVICES = 'salon_service'

  // SQL_TABLE_SER_SUB_CAT = ''
  SQL_TABLE_SEARCh = 'search'
  /*
   *  Databse fields name for salons services
   */

  // sser_id = 'sser_id'
  // ser_id = 'ser_id'
  // sser_name = 'sser_name'
  // sal_id = 'sal_id'
  // sser_rate = 'sser_rate'
  // sser_time = 'sser_time'
  // sser_enabled = 'sser_enabled'
  // sser_featured = 'sser_featured'
  // ssc_id = 'ssc_id'
  // sser_order = 'sser_order'
  // ssubc_id = 'ssubc_id'

  //service sub catogories
  SQL_SSC_GENDER = 'ssc_gender'
  SQL_SSC_ID = 'ssc_id'
  SQL_SSC_IMAGE = 'ssc_image'
  SQL_SSC_NAME = 'ssc_name'
  SQL_SSC_STATUS = 'ssc_status'


  //for search table
  SQL_SR_KEYWORD = 'keyword'
  SQL_SR_TYPE = 'type'
  SQL_SR_REFER_ID = 'refer_id'


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider,
  ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;

          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }
  getDatabaseState() {
    return this.databaseReady.asObservable()
  }



  create_table_offers() {
    let createTableQuery = 'CREATE TABLE IF NOT EXISTS offers (so_id TEXT, so_title TEXT, sot_id TEXT, so_amount TEXT,  so_notification TEXT, so_notification_type TEXT, so_is_active TEXT, so_expiry  TEXT, sal_id TEXT, so_days TEXT, so_time TEXT );'
    this.database.executeSql(createTableQuery, [])
      .then(res => {
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table offers', e.message)
      });
  }



  insert_salon_offer(ALLOFFERS: OFFERS[]) {
    if (!ALLOFFERS || ALLOFFERS.length === 0) {
      return
    }
    console.log('insertionTest: ' + ALLOFFERS.length + ' offers were sent for dumping');
    let fieledsPart = "INSERT INTO `offers` (`so_id`, `so_title`, `sot_id`, `so_amount`, `so_notification` , `so_notification_type`, `so_is_active` , `so_expiry` , `sal_id` , `so_days` , `so_time`  ) VALUES ";
    let ValuesPart = ""
    let so_ids = ""
    ALLOFFERS.forEach(offer => {
      ValuesPart += '("' + offer.so_id + '", "' + offer.so_title + '", "' + offer.sot_id + '", "' + offer.so_amount + '", "' + offer.so_notification + '","' + offer.so_notification_type + '","' + offer.so_is_active + '","' + offer.so_expiry + '","' + offer.sal_id + '","' + offer.so_days + '","' + offer.so_time + '"),';
      so_ids += offer.so_id + ','
    });

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    so_ids = so_ids.slice(0, so_ids.length - 1)

    return this.deleteMultipleSalonOffer(so_ids).then(isDeleted => {
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' offers were dumped');
          // console.log('serSubCat all Saved ' + ServiceSubCATEGORIE.length);

        })
        .catch(e => {
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
          console.log('eror: while inserting into offers' + e.message)
        });
    })
  }


  deleteMultipleSalonOffer(so_ids) {

    let deleteInQuery = 'DELETE FROM `offers` where so_id IN (' + so_ids + ') '

    return this.database.executeSql(deleteInQuery, [])
      .then(res => {
        return 1;

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteInQuery)
        return -1;


      });
  }

  create_table_salon_services() {
    let createTableQuery = 'CREATE TABLE IF NOT EXISTS salon_service ( sser_id int(10), ser_id int(10), sser_name varchar(500), sal_id  int(10), sser_rate int(10), sser_time varchar(100), sser_enabled tinyint(1), sser_featured tinyint(1), ssc_id int(10) , sser_order tinyint(1), ssubc_id int(10) )';
    this.database.executeSql(createTableQuery, [])
      .then(res => {
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

      });
  }


  getAllSalonServiceCategoriesBySalId(sal_id) {

    let query = 'SELECT ssc_id, ssc_name, sal_id   FROM  salon_service_categories where sal_id =  "' + sal_id + '"'

    let salonsServices = []
    return this.database.executeSql(query, [])
      .then(salonsServs => {

        for (var i = 0; i < salonsServs.rows.length; i++) {
          let salonService: SalonServices = salonsServs.rows.item(i)
          salonsServices.push(salonService)
        }
        return salonsServices
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, query)
        console.log('error while executing your query');
        return salonsServices

      });
  }


  getSalonBySalId(salId) {
    this.SALONS = []
    let Query = 'SELECT * FROM salons where sal_id= "' + salId + '" ';
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          let salon: Salon = res.rows.item(i)
          this.SALONS.push(salon)
        }
        return this.SALONS
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return this.SALONS = []
      });

  }


  getAllSalonServiceBySal(sal_id) {



    // let query=  'select *   FROM  salon_service where ssc_id =  "' + ssc_id + '"'

    let is_enable = 1;
    let query1 = 'SELECT  * FROM salon_service sc, salon_service_categories ssc where   sc.sser_enabled= "' + is_enable + '" and  sc.ssc_id = ssc.ssc_id and ssc.sal_id =  "' + sal_id + '"  order by ssc.ssc_id';

    let salonsServices = []
    return this.database.executeSql(query1, [])
      .then(salonsServs => {
        for (var i = 0; i < salonsServs.rows.length; i++) {
          let salonService: SalonServices = salonsServs.rows.item(i)
          salonsServices.push(salonService)
        }
        return salonsServices
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, query1)
        console.log('error while executing your query');
        return salonsServices

      });
  }




  create_table_salon_service_categories() {
    let createTableQuery = 'CREATE TABLE IF NOT EXISTS salon_service_categories (  ssc_name TEXT, `count` int(11), `ssc_id` int(11) ,  sal_id  int(10));'
    this.database.executeSql(createTableQuery, [])
      .then(res => {
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

      });
  }

  deleteMultipleSalonServices(salIds) {
    let Query = 'DELETE FROM `salon_service` where `sser_id` IN (' + salIds + ')';
    return this.database.executeSql(Query, [])
      .then(res => {

        return true
      })
      .catch(e => {

        this.serviceManager.sendErrorToServer(e.message, Query)
        return false
      });
  }


  deleteMultipleSalonServiceCatogries(salIds) {
    let Query = 'DELETE FROM salon_service_categories where sal_id IN (' + salIds + ')';
    return this.database.executeSql(Query, [])
      .then(res => {

        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return false
      });
  }

  saveSalonServicesIntoDB(allSalons: Salon[]) {
    if (!allSalons || (allSalons && allSalons.length === 0)) {
      // this.serviceManager.makeToastOnFailure('No Salon Services data to insert', 'top')
      return
    }

    let firstPart = "INSERT INTO `salon_service` (`sser_id`, `ser_id`, `sser_name`, `sal_id`, `sser_rate`, `sser_time`, `sser_enabled`, `sser_featured`, `ssc_id`, `sser_order`, `ssubc_id`) VALUES ";
    let secondPart = ''
    let count = 0
    let salServicesIDs = ''
    allSalons.forEach(salon => {
      let SalonServices = salon['services']
      count += SalonServices.length
      if (SalonServices && SalonServices.length > 0) {
        SalonServices.forEach(element => {
          let service = element
          secondPart += '("' + service.sser_id + '", "' + service.ser_id + '", "' + service.sser_name + '", "' + service.sal_id + '" , "' + Number(service.sser_rate) + '", "' + service.sser_time + '", "' + service.sser_enabled + '", "' + service.sser_featured + '","' + service.ssc_id + '","' + service.sser_order + '", "' + service.ssubc_id + '"),';
          salServicesIDs += service.sser_id + ','
        });
      }

    });
    console.log('insertionTest: ' + count + ' salon_service were sent for dumping');
    secondPart = secondPart.slice(0, secondPart.length - 1)
    salServicesIDs = salServicesIDs.slice(0, salServicesIDs.length - 1)

    let FinalIsertQuery = firstPart + secondPart
    return this.deleteMultipleSalonServices(salServicesIDs).then(isDeleted => {
      return this.database.executeSql(FinalIsertQuery, []).then(res => {
        console.log('insertionTest: ' + res.rowsAffected + ' salon_service were dumped');

      }).catch(e => {

        this.serviceManager.sendErrorToServer(e.message, FinalIsertQuery)
      })
    }, e => {

    })
  }

  saveSalonServicesCategoriesIntoDB(allSalons: Salon[]) {
    if (!allSalons || (allSalons && allSalons.length === 0)) {
      // this.serviceManager.makeToastOnFailure('No Salon Services data to insert', 'top')
      return
    }

    let firstPart = "INSERT INTO `salon_service_categories` (`ssc_name`, `count`, `ssc_id`, `sal_id` ) VALUES ";
    let secondPart = ''
    let count = 0
    allSalons.forEach(salon => {
      let SalonServicesCategory = salon['sal_ser_categories']
      let sal_id = salon['sal_id']

      count += SalonServicesCategory.length
      if (SalonServicesCategory && SalonServicesCategory.length > 0) {
        SalonServicesCategory.forEach(element => {
          let service = element
          secondPart += '("' + service.ssc_name + '", "' + service.count + '", "' + service.ssc_id + '", "' + sal_id + '"),';
        });
      }
    });
    secondPart = secondPart.slice(0, secondPart.length - 1)
    let FinalIsertQuery = firstPart + secondPart
    return this.database.executeSql(FinalIsertQuery, []).then(res => {
      console.log('insertionTest11:' + res.rowsAffected + ' salon_service were dumped');
    }).catch(e => {
      this.serviceManager.sendErrorToServer(e.message, FinalIsertQuery)
      console.log('insertionTestSalon:', JSON.stringify(e));

    })
  }


  insertSalonServicesTableByString(InsertQryData) {

    let firstPart = "INSERT INTO `salon_service` (`sser_id`, `ser_id`, `sser_name`, `sal_id`, `sser_rate`, `sser_time`, `sser_enabled`, `sser_featured`, `ssc_id`, `sser_order`, `ssubc_id`) VALUES ";
    let Query = first + InsertQryData;
    this.database.executeSql(Query, [])
      .then(res => {
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });

  }


  getSalonServicesBySerId(ser_id: string) {
    let salonsServices = []
    let Query = 'SELECT DISTINCT sal_id FROM salon_service WHERE ser_id= "' + ser_id + '" '
    return this.database.executeSql(Query, [])
      .then(salonsServs => {
        for (var i = 0; i < salonsServs.rows.length; i++) {
          let salonService: SalonServices = salonsServs.rows.item(i)
          salonsServices.push(salonService)
        }
        return salonsServices
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return salonsServices

      });
  }


  getAllSalonOffers() {
    let salonOffers = []
    return this.database.executeSql('SELECT * FROM offers', [])
      .then(offers => {

        for (var i = 0; i < offers.rows.length; i++) {

          let salonOffer: OFFERS = offers.rows.item(i)
          salonOffers.push(salonOffer)
        }
        return salonOffers
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, 'SELECT * FROM offers')
        return salonOffers

      });
  }

  getSalonOffers(sal_id) {
    if (!sal_id) {
      return
    }
    let Query = 'SELECT * FROM offers where sal_id=' + sal_id


    let salonOffers = []
    return this.database.executeSql(Query, [])
      .then(offers => {

        for (var i = 0; i < offers.rows.length; i++) {

          let salonOffer: OFFERS = offers.rows.item(i)
          salonOffers.push(salonOffer)
        }
        return salonOffers
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error while executing your query');
        return salonOffers

      });
  }

  getAllSalonServices() {
    let salonsServices = []
    let Query = 'SELECT * FROM salon_service'
    return this.database.executeSql(Query, [])
      .then(salonsServs => {
        for (var i = 0; i < salonsServs.rows.length; i++) {

          let salonService: SalonServices = salonsServs.rows.item(i)
          salonsServices.push(salonService)
        }
        return salonsServices
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error while executing your query');
        return salonsServices

      });
  }
  getSalonServicesForSubCategory(salon_id, ssc_id) {

    let salonsServices = []
    let Query = "SELECT * FROM salon_service  WHERE ssubc_id =  " + ssc_id + " AND sal_id = " + salon_id;

    return this.database.executeSql(Query, [])
      .then(salonsServs => {
        for (var i = 0; i < salonsServs.rows.length; i++) {

          let salonService: SalonServices = salonsServs.rows.item(i)
          salonsServices.push(salonService)
        }
        return salonsServices
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: while get salon services ' + e.message);
        return salonsServices
      });
  }
  //here salon service id's 
  getSalonServices(sal_id: string) {
    let salonsServices = []
    let Query = 'SELECT * FROM salon_service  WHERE sal_id= "' + sal_id + '" '
    return this.database.executeSql(Query, [])
      .then(salonsServs => {
        for (var i = 0; i < salonsServs.rows.length; i++) {
          let salonService: SalonServices = salonsServs.rows.item(i)
          salonsServices.push(salonService)
        }
        return salonsServices
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error while executing your query');
        return salonsServices

      });
  }

  getSalonsAndServicesForSearch(searchKeyWord): any {
    this.SALON_SERVICE_SEARCH = []

    let Query = 'SELECT * FROM ' + this.SQL_TABLE_SEARCh + ' where keyword like "%' + searchKeyWord + '%" AND (type="service" OR type="salon") limit 100'
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {
          let item = { keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).image_url }
          this.SALON_SERVICE_SEARCH.push(item)
        }
        return this.SALON_SERVICE_SEARCH;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      }
      );
  }


  deleteSearchDataByType(type) {
    let Query = 'DELETE FROM ' + this.SQL_TABLE_SEARCh + '   where type= ' + type
    this.database.executeSql(Query, [])
      .then(res => {
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });

  }

  deleteSalonService() {
    let Query = 'DELETE FROM salon_service'
    return this.database.executeSql(Query, [])
      .then(res => {
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: ' + e.message);
        return false
      });

  }

  deleteSalonServiceCategories() {
    let Query = 'DELETE FROM salon_service_categories'
    return this.database.executeSql(Query, [])
      .then(res => {
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: ' + e.message);
        return false
      });

  }
  //deleteOffers

  deleteOffers() {
    let Query = 'DELETE FROM offers'
    return this.database.executeSql(Query, [])
      .then(res => {
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: ' + e.message);
        return false
      });

  }


  getKeyWordsOnlyForLatestTrendsSearch(searchKeyWord): any {
    this.SALON_SERVICE_SEARCH = []
    let Query = 'SELECT * FROM ' + this.SQL_TABLE_SEARCh + ' where keyword like "%' + searchKeyWord + '%" AND type="ser_sub_categories"  limit 10'
    return this.database.executeSql(Query, [])
      .then(res => {

        for (var i = 0; i < res.rows.length; i++) {

          let item = { keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).image_url }
          this.SALON_SERVICE_SEARCH.push(item)
        }
        return this.SALON_SERVICE_SEARCH;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }



  getSearchForLatestTrendsSearch(searchKeyWord, ssc_gender): any {
    this.SALON_SERVICE_SEARCH = []
    let Query = 'SELECT  ssc_image,  sc_name || \' - \' || ssc_name as keyword, ssc_id as refer_id, \'ser_sub_categories\' as type  FROM service_categories sc, service_sub_categories ssc where (sc_name like "%' + searchKeyWord + '%" or ssc_name like "%' + searchKeyWord + '%") AND ssc.sc_id = sc.sc_id AND ssc_gender="' + ssc_gender + '"  limit 10'
    return this.database.executeSql(Query, [])
      .then(res => {
        for (var i = 0; i < res.rows.length; i++) {

          let item = { keyword: res.rows.item(i).keyword, type: res.rows.item(i).type, refer_id: res.rows.item(i).refer_id, image_url: res.rows.item(i).ssc_image }
          this.SALON_SERVICE_SEARCH.push(item)
        }
        return this.SALON_SERVICE_SEARCH;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }


  createServiceCategoryTable() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `service_categories` (`sc_featured` int(4), `sc_type` int(4), `sc_id` int(4), `sc_name` varchar(64) ,`sc_status` tinyint(1)  DEFAULT '1',`sc_image` varchar(255) , `sc_gender` tinyint(1) )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table service_categories', e.message)
      });
  }

  createFash_BrandsTable() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table service_categories', e.message)
      });
  }




  saveIntoFashBrandsTable(serviceCategories: FASHION_BRANDS[]) {
    console.log('insertionTest: ' + serviceCategories.length + ' fash_brands were sent for dumping');
    if (!serviceCategories || serviceCategories.length === 0) {
      return
    }
    //  let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"

    let fieledsPart = "INSERT INTO `fash_brands` (`fb_featured`,  `fb_id`, `fb_name`, `fb_status`, `fb_image`, `fb_gender`) VALUES ";
    let ValuesPart = ""
    // let scIDs = ""
    let fb_IDS = ""
    serviceCategories.forEach(fash_brand => {
      console.log('sc_featured', fash_brand.fb_featured)
      ValuesPart += '("' + fash_brand.fb_featured + '", "' + fash_brand.fb_id + '", "' + fash_brand.fb_name + '", "' + fash_brand.fb_status + '", "' + fash_brand.fb_image + '", "' + fash_brand.fb_gender + '"),';
      fb_IDS += fash_brand.fb_id + ','
    });

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    fb_IDS = fb_IDS.slice(0, fb_IDS.length - 1)

    return this.deleteAllFashionBrands(fb_IDS).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' service_categories were dumped');
        })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
            console.log('eror: while inserting into service_categories' + e.message)
          });
      }
    })
  }

  deleteAllFashionBrands(fb_ids) {

    let deleteCategorires = 'DELETE FROM fash_brands where fb_id IN (' + fb_ids + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        return false;
      });
  }
  saveIntoServiceCategoryTable(serviceCategories: SERVICE_CATEGORY[]) {
    console.log('insertionTest: ' + serviceCategories.length + ' service_categories were sent for dumping');
    if (!serviceCategories || serviceCategories.length === 0) {
      return
    }

    let fieledsPart = "INSERT INTO `service_categories` (`sc_featured`, `sc_type`, `sc_id`, `sc_name`, `sc_status`, `sc_image`, `sc_gender`) VALUES ";
    let ValuesPart = ""
    let scIDs = ""
    serviceCategories.forEach(serviceCategory => {
      // console.log('sc_featured',serviceCategory.sc_featured)
      ValuesPart += '("' + serviceCategory.sc_featured + '", "' + serviceCategory.sc_type + '", "' + serviceCategory.sc_id + '", "' + serviceCategory.sc_name + '", "' + serviceCategory.sc_status + '", "' + serviceCategory.sc_image + '", "' + serviceCategory.sc_gender + '"),';
      scIDs += serviceCategory.sc_id + ','
    });

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    scIDs = scIDs.slice(0, scIDs.length - 1)

    return this.deleteAllServiceCategories(scIDs).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' service_categories were dumped');
        })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
            console.log('eror: while inserting into service_categories' + e.message)
          });
      }
    })
  }
  getServiceCategoriesArchive() {


    let LatestTrendStyles: SERVICE_CATEGORY[] = []
    let Query = "select * from `service_categories`   where sc_type=2 order by sc_featured desc"

    // let Query = "SELECT * FROM `service_categories`  "
    return this.database.executeSql(Query, [])
      .then(style => {
        if (style.rows.length > 0) {

          for (var i = 0; i < style.rows.length; i++) {
            // console.log('sc_type:'+style.rows.item(i).sc_type)
            let subCategory: SERVICE_CATEGORY = style.rows.item(i)
            LatestTrendStyles.push(subCategory)
          }
          return LatestTrendStyles
        } else {
          return LatestTrendStyles
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return LatestTrendStyles

      });
  }
  getFashionTrends() {
    let LatestTrendStyles: SERVICE_CATEGORY[] = []
    let fashions = 'select * from `service_categories`   where sc_type=2 order by sc_featured desc'
    //let  Query ='select sc.*, max(ssc_modify_datetime) as modify_datetime, (select ssc.ssc_name from service_sub_categories as ssc where ssc.sc_id = sc.sc_id order by ssc.ssc_featured limit 0, 1) as ssc_name from `service_categories` as sc,`service_sub_categories` as ssc where ssc.sc_id = sc.sc_id group by sc.sc_id order by sc.sc_type desc, sc.sc_featured desc, modify_datetime desc'

    return this.database.executeSql(fashions, [])
      .then(style => {
        if (style.rows.length > 0) {

          for (var i = 0; i < style.rows.length; i++) {
            console.log('sc_type:' + style.rows.item(i).sc_type)
            let subCategory: SERVICE_CATEGORY = style.rows.item(i)
            LatestTrendStyles.push(subCategory)
          }
          return LatestTrendStyles
        } else {
          return LatestTrendStyles
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, fashions)
        return LatestTrendStyles

      });
  }
  getServiceCategories() {
    let LatestTrendStyles: SERVICE_CATEGORY[] = []

    let Query = 'select sc.*, max(ssc_modify_datetime) as modify_datetime, (select ssc.ssc_name from service_sub_categories as ssc where ssc.sc_id = sc.sc_id order by ssc.ssc_featured limit 0, 1) as ssc_name from `service_categories` as sc, `service_sub_categories` as ssc where ssc.sc_id = sc.sc_id and sc.sc_type= 1 group by sc.sc_id order by  sc.sc_featured desc, modify_datetime desc'

    return this.database.executeSql(Query, [])
      .then(style => {
        console.log('TotalTrends', style.rows.length)
        if (style.rows.length > 0) {

          for (var i = 0; i < style.rows.length; i++) {
            // console.log('sc_type:'+style.rows.item(i).sc_type)
            let subCategory: SERVICE_CATEGORY = style.rows.item(i)
            LatestTrendStyles.push(subCategory)
          }
          return LatestTrendStyles
        } else {
          return LatestTrendStyles
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return LatestTrendStyles

      });
  }
  getFashionServiceCategories() {
    let LatestTrendStyles: SERVICE_CATEGORY[] = []
    let Query = 'select sc.*, ssc.ssc_name  from `service_categories` as sc, `service_sub_categories` as ssc where sc.sc_id= ssc.sc_id order by sc.sc_type desc, sc.sc_featured desc, ssc.ssc_featured desc  limit 1'

    //let Query = "SELECT * FROM `service_categories`    ORDER BY sc_type DESC,  sc_featured DESC "
    return this.database.executeSql(Query, [])
      .then(style => {

        if (style.rows.length > 0) {


          for (var i = 0; i < style.rows.length; i++) {

            // console.log('sc_typeFashion:'+style.rows.item(i).sc_type)
            let subCategory: SERVICE_CATEGORY = style.rows.item(i)
            LatestTrendStyles.push(subCategory)
          }

          return LatestTrendStyles
        } else {
          return LatestTrendStyles
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        return LatestTrendStyles

      });
  }
  deleteAllServiceCategories(sc_ids) {

    let deleteCategorires = 'DELETE FROM service_categories where sc_id IN (' + sc_ids + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        return false;
      });
  }




  insert_services_sub_cat_table(ServiceSubCATEGORIE: ServiceSubCATEGORIES[]) {

    if (!ServiceSubCATEGORIE || ServiceSubCATEGORIE.length === 0) {
      return
    }

    let fieledsPart = "INSERT INTO `service_sub_categories` (`ssc_modify_datetime`, `ssc_featured`, `ssc_id`, `ssc_name`, `ssc_image`, `ssc_gender`, `count` , `sc_id`) VALUES ";
    let ValuesPart = ""
    let sscIDs = ""
    ServiceSubCATEGORIE.forEach(subCat => {
      ValuesPart += '("' + subCat.ssc_modify_datetime + '","' + subCat.ssc_featured + '","' + subCat.ssc_id + '", "' + subCat.ssc_name + '", "' + subCat.ssc_image + '", "' + subCat.ssc_gender + '", "' + subCat.count + '","' + subCat.sc_id + '"),';
      sscIDs += subCat.ssc_id + ','
    });

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    sscIDs = sscIDs.slice(0, sscIDs.length - 1)

    return this.deleteMultipleSubCategories(sscIDs).then(isDeleted => {
      return this.database.executeSql(slicedQuery, []).then(res => {

      })
        .catch(e => {
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
        });
    })
  }
  create_table_service_sub_categories() {
    let createTableQuery = 'CREATE TABLE IF NOT EXISTS service_sub_categories ( ssc_modify_datetime TEXT, ssc_featured tinyint(1), ssc_gender TEXT , ssc_id int(11), ssc_image TEXT, ssc_name TEXT, ssc_status  TEXT , `count` int(11), `sc_id` int(11) );'
    this.database.executeSql(createTableQuery, [])
      .then(res => {
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

      });
  }


  deleteSserviceSubCategories() {
    let Query = 'DELETE FROM service_sub_categories'
    this.database.executeSql(Query, [])
      .then(res => {

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }

  deleteSserviceCategories() {
    let Query = 'DELETE FROM service_categories'
    this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }



  deleteMultipleSubCategories(ssc_ids) {
    let Query = 'DELETE FROM service_sub_categories where ssc_id IN (' + ssc_ids + ')';
    return this.database.executeSql(Query, [])
      .then(res => {


        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: not deleted ' + e.message)
        return false
      });

  }

  getSubCategoriesOfCategory(sc_id) {

    let subCategories = []
    let Query = 'SELECT * FROM service_sub_categories where sc_id = ' + sc_id + ' ORDER BY `ssc_name` asc, `ssc_gender` asc '
    return this.database.executeSql(Query, [])
      .then(salonsServs => {
        // 



        (salonsServs.rows.length)
        for (var i = 0; i < salonsServs.rows.length; i++) {

          let subCategory: ServiceSubCATEGORIES = salonsServs.rows.item(i)
          subCategories.push(subCategory)
        }
        return subCategories
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error while executing your query ' + e.message);
        return subCategories

      });
  }



  getSubCategories() {




    let subCategories = []
    let Query = 'SELECT * FROM service_sub_categories ORDER BY `ssc_name` asc, `ssc_gender` asc '
    return this.database.executeSql(Query, [])
      .then(salonsServs => {
        for (var i = 0; i < salonsServs.rows.length; i++) {

          let subCategory: ServiceSubCATEGORIES = salonsServs.rows.item(i)
          subCategories.push(subCategory)
        }
        return subCategories
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error while executing your query');
        return subCategories

      });
  }

  getSubCategory(ssc_id) {

    let Query = 'SELECT * FROM service_sub_categories where ssc_id =' + ssc_id;


    let subCategory: ServiceSubCATEGORIES
    return this.database.executeSql(Query, [])
      .then(res => {

        subCategory = res.rows.item(0)
        return subCategory;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error while executing your query....');


        return subCategory

      });
  }

  createCategorySubStyle() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `category_sub_style` (`ip_id` varchar(40),`ssc_id` varchar(40) ,`imageUrl` varchar(1000) ,`p_keywords` varchar(255) , `favpost` varchar(20) , `p_setas_editor_img` varchar(255) , `update_date` varchar(255) )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('success: category_sub_style table created.');
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }

        console.log('Error while creating table category_sub_style', e.message)
      });
  }

  saveIntoCategorySubStyle(serviceCategories: CATEGORY_SUB_STYLE[]) {
    // here from tips detail page
    console.log('insertionTest: ' + serviceCategories.length + ' category_sub_style were sent for dumping');
    if (!serviceCategories || serviceCategories.length === 0) {
      return
    }
    let fieledsPart = "INSERT INTO `category_sub_style` (`ip_id`, `ssc_id`, `imageUrl`, `p_keywords`, `favpost`, `p_setas_editor_img`, `update_date`) VALUES ";
    let ValuesPart = ""
    let ipdIds = ""
    serviceCategories.forEach(serviceCategory => {
      ValuesPart += '("' + serviceCategory.ip_id + '", "' + serviceCategory.ssc_id + '", "' + serviceCategory.imageUrl + '", "' + serviceCategory.p_keywords + '", "' + serviceCategory.favpost + '", "' + serviceCategory.p_setas_editor_img + '", "' + serviceCategory.update_date + '"),';
      ipdIds += serviceCategory.ip_id + ','
    });
    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    ipdIds = ipdIds.slice(0, ipdIds.length - 1)
    return this.deleteAllCategorySubStyle(ipdIds).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, [])
          .then(res => {
            console.log('insertionTest: ' + res.rowsAffected + ' category_sub_style were dumped');
            // console.log('InsertQuesry',slicedQuery)
          })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
            console.log('eror: while inserting into service_categories' + e.message)
          });
      }
    })
  }

  updateFavPost(favpost, ssc_id) {
    let Query = 'UPDATE category_sub_style SET favpost ="' + favpost + '" where ssc_id="' + ssc_id + '" '
    return this.database.executeSql(Query, [])
      .then(style => {

        return true;

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error:  ' + e.message);
        return false;
      });
  }


  deleteAllCategorySubStyle(ipdIds) {

    let deleteCategorires = 'DELETE FROM category_sub_style where ip_id IN (' + ipdIds + ')';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {
        console.log('success: duplicate service_categories found ' + res.rowsAffected);
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        console.log('error: ' + e.message);
        return false;
      });
  }



  getCategorySubStyle(sc_id, update_date, type) {
    let selectQuery = ''
    if (!update_date || update_date === 0) {
      selectQuery = 'select sc.*,  af.fav_id from `category_sub_style` as  sc LEFT JOIN all_favorites af ON sc.ip_id = af.fav_id and af.cft_id=' + type + ' where  sc.ssc_id in ( select ssc.ssc_id from service_sub_categories as ssc where ssc.sc_id = "' + sc_id + '" ) ORDER BY  update_date DESC limit 3 '
    }
    else {
      selectQuery = ' select sc.*, af.fav_id from `category_sub_style` as  sc  LEFT JOIN all_favorites af ON sc.ip_id = af.fav_id and af.cft_id=' + type + '  where  sc.ssc_id in ( select ssc.ssc_id from service_sub_categories as ssc where ssc.sc_id = "' + sc_id + '" ) and sc.update_date <   "' + update_date + '"  ORDER BY  update_date DESC   limit 8 '
    }

    let categorySubStyle: CATEGORY_SUB_STYLE[] = []
    return this.database.executeSql(selectQuery, [])
      .then(style => {
        if (style.rows.length > 0) {
          for (var i = 0; i < style.rows.length; i++) {
            let subCategory: CATEGORY_SUB_STYLE = style.rows.item(i)
            if (subCategory.fav_id === undefined || subCategory.fav_id === null) {
              subCategory.fav_id = 0
            }
            console.log('Items:', JSON.stringify(subCategory))
            categorySubStyle.push(subCategory)
          }
          return categorySubStyle
        } else {
          return categorySubStyle
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)
        console.log('error:  ' + e.message);
        return categorySubStyle

      });
  }


  getFavouriteBeautyTrends(type) {

    let selectQuery = ''

    selectQuery = 'select sc.*,  af.fav_id from `category_sub_style` as  sc, all_favorites af     where sc.ip_id = af.fav_id and af.cft_id=' + type + '  ORDER BY  update_date DESC'


    // let Query='select sc.* from `category_sub_style` as  sc where  sc.ssc_id in ( select ssc.ssc_id from service_sub_categories as ssc where ssc.sc_id = "' + sc_id + '" ) ORDER BY p_setas_editor_img DESC, favpost DESC,  update_date DESC '
    console.log('slect:' + selectQuery)
    let categorySubStyle: CATEGORY_SUB_STYLE[] = []

    //let fieledsPart = "INSERT INTO `service_sub_categories` (`ssc_featured`, `ssc_id`, `ssc_name`, `ssc_image`, `ssc_gender`, `count` , `sc_id`) VALUES ";
    //let Query='select sc.*, max(ssc_modify_datetime) as modify_datetime, (select ssc.ssc_name from service_sub_categories as ssc where ssc.sc_id = sc.sc_id order by ssc.ssc_featured limit 0, 1) as ssc_name from `service_categories` as sc,`service_sub_categories` as ssc where ssc.sc_id = sc.sc_id group by sc.sc_id order by sc.sc_type desc, sc.sc_featured desc, modify_datetime desc'

    return this.database.executeSql(selectQuery, [])
      .then(style => {
        if (style.rows.length > 0) {
          for (var i = 0; i < style.rows.length; i++) {
            let subCategory: CATEGORY_SUB_STYLE = style.rows.item(i)

            if (subCategory.fav_id === undefined || subCategory.fav_id === null) {
              subCategory.fav_id = 0
            } else {

            }
            console.log('subCategory', JSON.stringify(subCategory))
            categorySubStyle.push(subCategory)
          }

          return categorySubStyle
        } else {
          return categorySubStyle
        }

      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)
        console.log('error:  ' + e.message);
        return categorySubStyle

      });
  }

  getCategorySubStyleMaxDate() {
    let dateMax = "";
    let Query = 'SELECT  update_date   FROM category_sub_style  ORDER BY update_date DESC limit 1 '
    return this.database.executeSql(Query, [])
      .then(style => {

        if (style.rows.length > 0) {

          for (var i = 0; i < style.rows.length; i++) {
            dateMax = style.rows.item(i).update_date;

          }
          return dateMax;
        } else {
          return dateMax;
        }
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error:  ' + e.message);
        return dateMax

      });
  }


  deleteCategorySubStyle() {
    let Query = 'DELETE FROM category_sub_style'
    this.database.executeSql(Query, [])
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
      });
  }

  //latest-trends-last-fetch date
  //for brands dates table
  createTrend_Dates_Table() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `trend_dates` (`dates_id` int(4),   `gender` tinyint(1)  DEFAULT '1' , `t_date` varchar(255)  )"
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        console.log('created table fash_date')
        // alert('FashionBrandTable created')
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('Error while creating table fash_date', e.message)
      });
  }
  getTrendPostMaxDate(sc_id, gender) {


    let dateMax = "";

    let Query = 'SELECT  t_date FROM trend_dates where dates_id = ' + sc_id + ' and gender= "' + gender + '"  limit 1 '


    return this.database.executeSql(Query, [])
      .then(style => {

        if (style.rows.length > 0) {

          for (var i = 0; i < style.rows.length; i++) {
            dateMax = style.rows.item(i).t_date;

          }
          return dateMax;
        } else {
          return dateMax;
        }
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error:  ' + e.message);
        return dateMax

      });
  }
  saveIntoTrendPostDatesTable(dateObj) {
    if (!dateObj) {
      return
    }
    console.log('TryingToInsertDates')
    //  let createTableQuery = "CREATE TABLE IF NOT EXISTS `fash_brands` (`fb_featured` int(4),  `fb_id` int(4), `fb_name` varchar(64) ,`fb_status` tinyint(1)  DEFAULT '1',`fb_image` varchar(255) , `fb_gender` tinyint(1) )"
    let fieledsPart = "INSERT INTO `trend_dates` (`dates_id`,  `gender`, `t_date`  ) VALUES ";
    let ValuesPart = ""
    // let scIDs = ""
    let fc_IDS = ""
    ValuesPart += '("' + dateObj.dates_id + '", "' + dateObj.gender + '", "' + dateObj.t_date + '"),';
    fc_IDS += dateObj.dates_id + ','

    let finalQuery = fieledsPart + ValuesPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    fc_IDS = fc_IDS.slice(0, fc_IDS.length - 1)
    return this.deleteMultipleTrendDates(fc_IDS, dateObj.gender).then(isDeleted => {
      if (isDeleted) {
        return this.database.executeSql(slicedQuery, []).then(res => {
          // alert('insertionTest: ' + res.rowsAffected + ' fash_collections were dumped')
          console.log('insertionTest: ' + res.rowsAffected + ' fash_dates were dumped');
        })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, slicedQuery)
            // alert('eror: while inserting into fash_collections' + e.message)
            console.log('eror: while inserting into fash_dates' + e.message)
          });
      }
    })
  }
  deleteMultipleTrendDates(dates_id, gender) {

    let deleteCategorires = 'DELETE FROM trend_dates where dates_id IN (' + dates_id + ') and gender= "' + gender + '"';
    return this.database.executeSql(deleteCategorires, [])
      .then(res => {

        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteCategorires)
        return false;
      });
  }
}