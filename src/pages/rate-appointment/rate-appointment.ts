import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Platform } from 'ionic-angular/platform/platform';
import { MainHomePage } from '../main-home/main-home';
/**
 * Generated class for the RateAppointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rate-appointment',
  templateUrl: 'rate-appointment.html',
})
export class RateAppointmentPage {

  public appointment;
  public selectedRating = 1;
  public wordCount = "0/500";
  public review = '';
  public cust_id;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loader: LoadingController,

    public serviceManager: GlobalProvider,
    public storage: Storage,
    public platform: Platform,
    public nativePageTransitions: NativePageTransitions
  ) {
    this.storage.get(this.serviceManager.CUSTOMER_DATA).then((value) => {
      console.log('LoginStatus', value);
      this.cust_id = value.cust_id;
      value ? true : false
    }).catch((Error) => {
      console.log('Error', 'in login');
    });
    this.appointment = this.navParams.get("appo");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RateAppointmentPage');
  }

  onModelChange(value) {
    this.selectedRating = value;
    
  }

  onInput(value) {
    this.wordCount = value.length + "/500";
    this.review = value;
  }

  submitTapped() {
    let loading = this.loader.create({ content: "Please Wait..." });
    loading.present();

    var params = {
      service: btoa('review_appointment'),
      cust_id: btoa(this.cust_id),
      app_id: btoa(this.appointment.app_id),
      app_rating: btoa(this.selectedRating.toString()),
      app_review: btoa(this.review),
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        loading.dismissAll();

        this.serviceManager.makeToastOnSuccess('Thanks for submitting your review.');
        this.navCtrl.pop();
      },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }

      );

  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }

}
