
import { Customer, BEAUTY_TIPS, TIPS_DETAIL } from './../../providers/SalonAppUser-Interface';
import { GlobalProvider } from './../../providers/global/global';
import { CustomerModal } from './../../providers/CustomerModal/CustomerModal';
import { MainHomePage } from './../main-home/main-home';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Component, ViewChild, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar, Content , Events, Platform} from 'ionic-angular';
import { BeautyTipsDetailPage } from '../beauty-tips-detail/beauty-tips-detail';
import { BeautyTipsModal } from '../../providers/BeautyTipsModal/BeautyTipsModal';
import { config } from '../../providers/config/config';
import { TipsDetailModal } from '../../providers/TipsDetailModal/TipsDetailModal';

import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { MenuController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-beauty-tips-view',
  templateUrl: 'beauty-tips-view.html',
})
export class BeautyTipsViewPage {
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Content) content: Content;

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public shouldShowMiniHeader = false
  public showMoreTips:boolean=false
  public btc_id:any
  public beautyTips: BEAUTY_TIPS[] = []
  public beautyTipsDetailEnglish: TIPS_DETAIL[] = []
  public beautyTipsDetailUrdu: TIPS_DETAIL[] = []
  public beautyTipsDetail: TIPS_DETAIL[] = []
  public heroArticle: TIPS_DETAIL
  public shouldShowPostsInEnglish = true
  public noStyleFound = false
  public GenderBasedTrends: string
  public slider: HTMLElement
  public isFirstTime=false
  public unregisterBackButtonAction: any;
 // public categoryImageUrl = config.CategoryImageURL
  public categoryImageUrl = config.BeautyTipsImageURL


  public myRating = 4.5
 
  constructor(
    private menu: MenuController,
    public platform: Platform,
    public events: Events,
    public navCtrl: NavController,
    public navParams: NavParams,
    public customerModal: CustomerModal,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public beautyTipsModal: BeautyTipsModal,
    public tipsDetailModal: TipsDetailModal,
    private zone: NgZone,
  ) {
    let beautyTip = this.navParams.get('beautyTip')
    if(beautyTip){
      this.isFirstTime=true;
      this.showMoreTips=true;
      this.btc_id=beautyTip.btc_id;
    }
    this.PageReloadCode();
   
  }
  getbeautyTipFromDb(){
    this.beautyTipsDetailEnglish=[]
    this.beautyTipsDetailUrdu=[]
    this.beautyTipsDetail=[]
    this.tipsDetailModal.getBeautyTipsNEW(this.btc_id).then(beautyTips => {
      let _beautyTipsDetail: TIPS_DETAIL[] = beautyTips
     // console.log("TipsCategories",JSON.stringify(beautyTips))
      _beautyTipsDetail.forEach(beautyTipPost => {
        if (beautyTipPost.bt_description && beautyTipPost.bt_description.trim().length > 0) {
          this.beautyTipsDetailEnglish.push(beautyTipPost)
        }
        if (beautyTipPost.bt_description_urdu && beautyTipPost.bt_description_urdu.trim().length > 0) {
          this.beautyTipsDetailUrdu.push(beautyTipPost)
        }
      });
      if (this.serviceManager.getFromLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS) === true) {
        this.slider = document.getElementById('slider_bw');
        this.slider.style.cssFloat = 'left';
        this.shouldShowPostsInEnglish = true
        this.GenderBasedTrends = 'English'
        this.beautyTipsDetail = this.beautyTipsDetailEnglish
        !this.beautyTipsDetail || this.beautyTipsDetail.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
      } else if (this.serviceManager.getFromLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS) === false) {

        this.slider = document.getElementById('slider_bw');
        this.slider.style.cssFloat = 'right';

        this.shouldShowPostsInEnglish = false
        this.GenderBasedTrends = 'Urdu'
        this.beautyTipsDetail = this.beautyTipsDetailUrdu
        !this.beautyTipsDetail || this.beautyTipsDetail.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
        this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, false)
      } else {

        this.slider = document.getElementById('slider_bw');
        this.slider.style.cssFloat = 'left';

        this.shouldShowPostsInEnglish = true
        this.GenderBasedTrends = 'English'
        this.beautyTipsDetail = this.beautyTipsDetailEnglish
        !this.beautyTipsDetail || this.beautyTipsDetail.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
        this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, true)
      }
    })
  }
  PageReloadCode() {
    this.tipsDetailModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.tipsDetailModal.getLatestHeroArticle(this.btc_id).then(heroArticle => {
        this.heroArticle = heroArticle
        })
        this.getbeautyTipFromDb();
      }
    })
    let isBTFetched = this.serviceManager.getFromLocalStorage(this.serviceManager.BEAUTY_TIPS_LAST_FETCHED_DATE)
    if (isBTFetched) {
      this.beautyTipsModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.beautyTips=[]
          this.beautyTipsModal.getBeautyTips(this.btc_id).then(beautyTips => {
            this.beautyTips = beautyTips
           // console.log("beautyTips",JSON.stringify(this.beautyTips))
            if(!this.btc_id){
              this.getBeautyTipsFromServer()
            }
           })
        }
      })
    } else {
      if(!this.btc_id){
        this.getBeautyTipsFromServer()
      }
    }
  }
  tagClickek(event, tipItem, index){
    event.stopPropagation();
    if(tipItem){
      this.showMoreTips=true;
      this.btc_id=tipItem.btc_id;
    }
 
    this.PageReloadCode();
    this.scrollToTop();
  }
 
  goBack() {
    this.navCtrl.canGoBack() ? this.navCtrl.pop() : this.navCtrl.setRoot(MainHomePage)
  }
  categoryClicked(event ,beautyTip, index) {
    event.stopPropagation();
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(BeautyTipsDetailPage, {
      beautyTip: beautyTip
    })
  }
  beautyTipClicked(beautyTip, index){
      //let beautyTip = this.navParams.get('beautyTip')
       if(beautyTip){
         this.showMoreTips=true;
         this.btc_id=beautyTip.btc_id;
        }
        this.PageReloadCode();
        this.scrollToTop();
        this.navCtrl.setRoot(BeautyTipsViewPage,{
        beautyTip:beautyTip,
        isFromBeautyTipsDetailPage:true,
       }) 
  }
  getBeautyTipsFromServer() {
    // let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.LAST_FETCH_DATE_FOR_BEAUTY_TIPS)
    let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.BEAUTY_TIPS_LAST_FETCHED_DATE)

    let responseTime = lastFetchDate ? lastFetchDate : ''

    const params = {
      service: btoa('get_tip_categories'),
      last_fetched: btoa(responseTime)
    }
     console.log('Params1',JSON.stringify(params))
    !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.showProgress() : ''

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.stopProgress() : ''
          if (!res.TipCategories) {
            this.serviceManager.setInLocalStorage(this.serviceManager.BEAUTY_TIPS_LAST_FETCHED_DATE, res.response_datetime)
          
            this.getTipsDetailFromServer()
            return
          }
          this.beautyTips = res.TipCategories
          this.beautyTipsModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.beautyTipsModal.InsertInToBeautyTipsTable(this.beautyTips).then(done => {
                this.serviceManager.setInLocalStorage(this.serviceManager.BEAUTY_TIPS_LAST_FETCHED_DATE, res.response_datetime)
              }).then(jobsDone => {
              
                this.getTipsDetailFromServer()
              })
            }
          })
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  ionViewWillEnter() { 
    // let beautyTip = this.navParams.get('beautyTip')
    // if(beautyTip){
    //   this.isFirstTime=true;
    //   this.showMoreTips=true;
    //   this.btc_id=beautyTip.btc_id;
    //   this.PageReloadCode();
    //   this.scrollToTop();
    // }
    

  }
   //custom back button for android
   ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
  }
//end custom back button for android
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
 //page refresh on comming back from detail page
//  if(!this.isFirstTime){
//   let options: NativeTransitionOptions = {
//     direction: 'top',
//     duration: 500,
//     slowdownfactor: 3,
//     slidePixels: 20,
//     iosdelay: 150,
//     androiddelay: 0,
//     fixedPixelsTop: 0,
//     fixedPixelsBottom: 0
//     };
//   this.nativePageTransitions.fade(options);
//   this.navCtrl.setRoot(BeautyTipsViewPage, {}, {animate: false});
// }else{
//   this.isFirstTime=false
// }    
  }
  scrollToTop() {
  this.content.scrollToTop();
  let options: NativeTransitionOptions = {
    direction: 'top',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 150,
    androiddelay: 0,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 0
    };
     this.nativePageTransitions.fade(options);
  }
  updateUrl() {
    this.categoryImageUrl = config.BeautyTipsImageURL + 'beauty_salon.png';
    // console.log(this.categoryImageUrl);
    // onError="this.src='https://nopso.qwpcorp.com/apis_pk/beautytips_images/beauty_salon.png'"
  }
  getTipsDetailFromServer() {
    let lastFetchDate = this.serviceManager.getFromLocalStorage(this.serviceManager.TIPS_DETAILS_LAST_FETCHED_DATE)
    let responseTime = lastFetchDate ? lastFetchDate : ''
    const params = {
      service: btoa('get_beauty_tips'),
      last_fetched: btoa(responseTime)
    }
    console.log('Params',JSON.stringify(params))
    !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.showProgress() : ''
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          !lastFetchDate || lastFetchDate.length === 0 ? this.serviceManager.stopProgress() : ''
          if (!res.beauty_tips) {
            this.serviceManager.setInLocalStorage(this.serviceManager.TIPS_DETAILS_LAST_FETCHED_DATE, res.response_datetime)
            return
          }
          let beautyTipsDetail: TIPS_DETAIL[] = res.beauty_tips
          //console.log("TipDetails1Total",res.beauty_tips.length)
          this.beautyTipsDetail = beautyTipsDetail;
          //console.log("TipDetails1",JSON.stringify(this.beautyTipsDetail))
          this.tipsDetailModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.tipsDetailModal.InsertInToTipsDetailTable(beautyTipsDetail).then(done => {
                this.tipsDetailModal.getLatestHeroArticle(this.btc_id).then(heroArticle => {
                  this.heroArticle = heroArticle
                })
                this.getbeautyTipFromDb();
                this.serviceManager.setInLocalStorage(this.serviceManager.TIPS_DETAILS_LAST_FETCHED_DATE, res.response_datetime)
              })
            }
          })

        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  // btnBackTapped() {
  //   let options: NativeTransitionOptions = {
  //     direction: 'right',
  //     duration: 500,
  //     slowdownfactor: 3,
  //     slidePixels: 20,
  //     iosdelay: 100,
  //     androiddelay: 150,
  //     fixedPixelsTop: 0,
  //     fixedPixelsBottom: 0
  //   };
  //   if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
  //   if (this.navCtrl.canGoBack()) {
  //     this.navCtrl.pop({
  //       animate: false,
  //       animation: 'ios-transition',
  //       direction: 'back',
  //       duration: 500,
  //     })
  //   } else {
  //     this.navCtrl.setRoot(MainHomePage)
  //   }
  // }
  postsInEnglishTapepd() {
    this.slider = document.getElementById('slider_bw');
    this.slider.style.cssFloat = 'left';
    if (this.shouldShowPostsInEnglish) {
      return
    }
    this.shouldShowPostsInEnglish = true
    this.beautyTipsDetail = this.beautyTipsDetailEnglish
    !this.beautyTipsDetail || this.beautyTipsDetail.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, true)
  }
  postsInUrduTapepd() {
    console.log('Urdu-tapped')
    this.slider = document.getElementById('slider_bw');
    this.slider.style.cssFloat = 'right';
    if (!this.shouldShowPostsInEnglish) {
      return
    }
    this.shouldShowPostsInEnglish = false
    this.beautyTipsDetail = this.beautyTipsDetailUrdu
    !this.beautyTipsDetail || this.beautyTipsDetail.length === 0 ? this.noStyleFound = true : this.noStyleFound = false
    this.serviceManager.setInLocalStorage(this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS, false)
    console.log('End-Urdu-tapped')
  }
  
}
