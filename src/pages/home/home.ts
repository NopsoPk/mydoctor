import { CustomerModal } from "./../../providers/CustomerModal/CustomerModal";
import { SalonServicesModal } from "./../../providers/salon-services-modal/salon-services-modal";
import { SalonServices } from "./../../providers/SalonAppUser-Interface";
import { SQLiteObject, SQLite } from "@ionic-native/sqlite";
import { Keyboard } from "@ionic-native/keyboard";
import {
  Salon,
  Services,
  SALON_SERVICES_SEARCH,
  Customer,
  OFFERS,
  SALON_CARD_OPTIONS,
  Homepage,
  SWITCH_TO_THE_TAB,
  SalonHour
} from "./../../providers/SalonAppUser-Interface";
import { config } from "./../../providers/config/config";

import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ViewController,
  Platform,
  Nav,
  Events,
  MenuController,
  App
} from "ionic-angular";
import { MainHomePage } from "../main-home/main-home";
import { timer } from "rxjs/observable/timer";
import { SalonDetailsPage } from "../salon-details/salon-details";
import { AllOffersPage } from "../all-offers/all-offers";
import { Storage } from "@ionic/storage";

import { GlobalServiceProvider } from "../../providers/global-service/global-service";
import { TabsPage } from "../tabs/tabs";
import { NgZone } from "@angular/core";
import { last } from "rxjs/operators";

import { GlobalProvider } from "../../providers/global/global";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import "rxjs/add/operator/retrywhen";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/scan";
import { SqliteDbProvider } from "../../providers/sqlite-db/sqlite-db";
import { GoogleAnalytics } from "../../../node_modules/@ionic-native/google-analytics";
import { PopoverController } from "ionic-angular";
import { NOPSO_Header_OPTIONS } from "../../providers/SalonAppUser-Interface";
import { AppointmentsCardPage } from "../appointments-card/appointments-card";
import { AppMessages } from "../../providers/AppMessages/AppMessages";
// import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { Content } from "ionic-angular";
import { LoginSignUpPage } from "../login-sign-up/login-sign-up";
//http://pointdeveloper.com/hide-ionic-2-tab-bar-specific-tabs/
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  public ALLOFFERS: OFFERS[];

  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content) content: Content;
  @ViewChild("scroll") scroll: any;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };
  public reRequestCounter = 0;
  public slideOutNow = false;
  public isFavourites = false;
  public infiniteScroll: any;
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS;
  public allOffers: OFFERS[];
  public offersForListing: OFFERS[];
  public subStyleImageName: string;
  public pinImage: string;
  public subStyleName: string;
  public txtSearchSalon: string;
  public serverSearchlbl: string = "Go";
  public unregisterBackButtonAction: any;
  public noSalonFound = false;
  public allSalon = [];
  public SalonHoursAll: SalonHour[];

  public serverSearchSalon = [];
  public allSalSerSubCat = [];
  public allSalonBackUp = [];
  public salonImagesURL = config.salonImgUrl;
  public techImagesURL = config.TechImageURL;
  public subStyleImagesBaseURL = config.StylesImagesURL;
  public defaultSalonPicName = "default.png";
  public showTagsPopOver = false;
  public myCustomer: Customer;
  // public salonCardOption: SALON_CARD_OPTIONS
  public sectionName = "";
  public SalonsServices: SalonServices[];
  public shouldShowMiniHeader = false;
  public sal_id = 0;
  public distance = 0;
  public distanceUnit = "";
  loading: any;
  public isServerSearching = false;
  constructor(
    public sqlProvider: SqliteDbProvider,
    private zone: NgZone,
    public loadingController: LoadingController,
    // public favModal: FavoriteModal,
    public events: Events,
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public platform: Platform,
    public globalSearch: GlobalServiceProvider,
    private viewCtrl: ViewController,
    private nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
    public keyboard: Keyboard,
    public salonModal: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    public customerModal: CustomerModal,
    private sqlite: SQLite,
    private ga: GoogleAnalytics,
    public popoverCtrl: PopoverController,
    public appCtrl: App
  ) // public favModel: FavoriteModal,
  {
    this.sectionName = this.navParams.get('sectionName')
    this.txtSearchSalon = "";
    this.distanceUnit = this.globalSearch.distance_unit;
    this.loading = this.loadingController.create({
      content: "Fetching Doctors, Please Wait...144"
    });
    // this.loading.present();

    this.loadSalons();

    // this.getCustomer();
  }

  loadSalons() {

    this.menu.swipeEnable(true);
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        !this.globalSearch.sty_id || this.globalSearch.sty_id.trim().length === 0 ? this.globalSearch.sty_id = "1" : ''
        this.salonModal.getAllSalonsByTypeFromDB(this.globalSearch.sty_id, this.sal_id, this.distance).then(myAllSalons => {
          this.allSalon = myAllSalons;
          !this.allSalon || this.allSalon.length == 0 ? this.noSalonFound = true : this.noSalonFound = false
          if (this.allSalon && this.allSalon.length > 0) {


            this.distance = this.allSalon[this.allSalon.length - 1].distance;
            this.sal_id = this.allSalon[this.allSalon.length - 1].sal_id;
          } else {
            this.allSalon = []
            !this.allSalon || this.allSalon.length == 0 ? this.noSalonFound = true : this.noSalonFound = false
            // alert('call here api to get salons against salon type ID')
          }

        });
      }

    });
  }
  wentToLoadMore = false;
  loadMoreSalons(infiniteScroll) {
    console.log("LazyLoadingCalled");
    if (this.wentToLoadMore || this.sal_id === -1) {
      if (infiniteScroll) {
        try {
          infiniteScroll.complete();
        } catch (ex) { }
      }
      return;
    }
    this.infiniteScroll = infiniteScroll;
    this.getPostFromDb();
  }
  getPostFromDb() {
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        let serviceCat = this.navParams.get("fashionTrends");
        this.salonModal
          .getAllSalonsByTypeFromDB(
            this.globalSearch.sty_id,
            this.sal_id,
            this.distance
          )
          .then(
            res => {
              console.log("MoreSalons", JSON.stringify(res));
              this.updatePageData(res);
            },
            error => { }
          );
      }
    });
  }
  updatePageData(res) {
    this.allSalon = this.allSalon.concat(res);
    this.infiniteScroll.complete();
    if (res && res.length > 0) {
      this.noSalonFound = false;
      if (this.allSalon && this.allSalon.length > 0) {
        this.distance = this.allSalon[this.allSalon.length - 1].distance;
        this.sal_id = this.allSalon[this.allSalon.length - 1].sal_id;
      }

      this.refreshSalonCard();
    } else {
      this.noSalonFound = true;
    }
    if (res && res.length < 8) {
      this.infiniteScroll.enable(false);
    }
    timer(4000).subscribe(() => {
      this.wentToLoadMore = false;
    });
  }
  public refreshSalonCard() {
    // this.salonCardOption = {
    //   CardHeight: '100vh',
    //   calledFrom: config.HomePage,
    //   ShouldShowSalonImage: true,
    //   shouldScroll: true,
    //   salon: this.allSalon,
    // }
  }
  public getSalDistance(val) {
    if (val.length > 3) {
      return val.substring(0, 3) + " miles";
    }
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    this.events.subscribe("canShowMiniHeader", isHidden => {
      this.zone.run(() => {
        this.shouldShowMiniHeader = isHidden;
      });
    });
  }
  ionViewDidLoad() {
    this.allOffers = [];
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getAllSalonOffers().then(
          offers => {
            this.allOffers = offers;
            this.showSalonDetail();
          },
          error => {
            this.showSalonDetail();
            console.log("Error In Getting Offerr");
          }
        );
      }
    });
  }
  showSalonDetail() {
    this.showTagsPopOver = false;
    let objPin = this.serviceManager.getFromLocalStorage(
      this.serviceManager.OBJ_PIN
    );
    let objSubStyle = this.serviceManager.getFromLocalStorage(
      this.serviceManager.OBJ_SUB_STYLE
    );

    if (objPin && objSubStyle) {
      this.pinImage = objPin.imageUrl;
      this.subStyleName = objSubStyle.ssc_name;
    }

    this.initializeBackButtonCustomHandler();
    // let index = this.globalSearch.tabIndex;
    // if (index != "1") {
    //   //zahid
    //   // this.getCustomer()
    // } else {
    //   this.globalSearch.tabIndex = "0";
    // }
  }
  ionViewWillEnter() {
    this.getCustomer();
  }
  getOfferBySalId(Id: string) {
    var found = this.allOffers.find(function (element) {
      //console.log('lololol',JSON.stringify(element))
      return element.sal_id == Id;
    });
    return found;
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  shouldBeginEditing(value) { }
  endEndEditing(value) {

  }
  isSearchResponseBack = true;
  shouldChangeCharacter(search) {

    let searchValue: string = search.value;
    if (searchValue.trim().length === 0) {

      this.filteredTags = [];
      this.showTagsPopOver = false;
      this.txtSearchSalon = "";
      this.keyboard.close();
      return;
    }

    searchValue = searchValue.toLowerCase();
    console.log(searchValue);
    this.salonModal
      .searchSalonsByTypeFromDB(this.globalSearch.sty_id, searchValue)
      .then(searchedResult => {
        this.isSearchResponseBack = false;
        if (searchedResult && searchedResult.length > 0) {
          this.filteredTags = [];
          this.filteredTags = searchedResult;
          if (this.filteredTags && this.filteredTags.length > 0) {
            this.showTagsPopOver = true;
            console.log("showTagsPopOver " + this.showTagsPopOver);
          }
        } else {
          this.filteredTags = [];
          this.showTagsPopOver = false;
        }
      });
  }
  handleReturnkey() {
    console.log("return key is tapped");
    this.keyboard.close();
  }
  getDataFromServer(cust: any) {
    alert('zahid 419')
    let loading = this.loadingController.create({
      content: "Fetching Doctors, Please Wait...421"
    });
    let cust_lat: string = cust.cust_lat;
    let cust_lng: string = cust.cust_lng;

    loading.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      service: btoa("lst_salons"),
      cust_id: btoa(cust.cust_id),
      keywords: btoa(this.globalSearch.search_keyword),
      page_no: btoa("1"),
      sal_last_fetched: btoa(""),
      platform: btoa("ios"),
      app_version: btoa("5.0"),
      cust_zip: btoa(cust.cust_zip),
      cust_lat: btoa(cust_lat),
      cust_lng: btoa(cust_lng)
    };

    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })

      .subscribe(
        response => {
          this.allSalon = response["salons"];
          this.allSalonBackUp = this.allSalon;
          if (this.allSalon) {
            if (this.allSalon.length == 0) {
              this.noSalonFound = true;
            } else {
              this.noSalonFound = false;
            }
          } else {
            this.noSalonFound = true;
          }

          loading.dismissAll();

          this.serviceManager.setInLocalStorage(
            this.serviceManager.SALONS_NEAR_CUSTOMER,
            this.allSalon
          );
        },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
  }
  ifPromotionMoreThan1(salId, salon) {
    this.offersForListing = [];

    this.allOffers.forEach(element => {
      if (element.sal_id == salId) {
        this.offersForListing.push(element);
      }
    });
    if (this.offersForListing.length > 1) {
      return true;
    } else {
      return false;
    }
  }
  getPromotions(salId) {
    let promotions = this.getOfferBySalId(salId);
    if (promotions == null) {
      return "";
    } else {
      // console.log("Offer", JSON.stringify(promotions))
      return promotions.so_title;
    }
  }
  btnAllOfferTaped(salId, sal_name, sal_pic) {
    this.offersForListing = [];
    this.allOffers.forEach(element => {
      if (element.sal_id == salId) {
        this.offersForListing.push(element);
      }
    });
    this.navCtrl.push(AllOffersPage, {
      offersForListing: this.offersForListing,
      sal_name: sal_name,
      sal_pic: sal_pic
    });


  }
  btnBackTapped() {

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage);
    }
  }
  openDetailsPage(salon) {
    let offerType;
    let promotions = this.getOfferBySalId(salon.sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }

    this.nativePageTransitions.slide(this.options);

    this.navCtrl.push(SalonDetailsPage, {
      isServerSearch: false,
      selectedSalon: salon,
      offerType: offerType,
      sal_id: salon.sal_id
      // distance: salon.distance,
    });
  }
  openDetailsPageSearch(salon) {
    let offerType;
    let promotions = this.getOfferBySalId(salon.sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }
    this.nativePageTransitions.slide(this.options);

    // let eventAction = this.myCustomer.cust_name + 'Tapped on Salon ' + salon.sal_name + ' of id ' + salon.sal_id
    // this.ga.trackEvent('Tap', eventAction, 'lab: general label', 200, true)
    this.navCtrl.push(SalonDetailsPage, {
      isServerSearch: true,
      selectedSalon: salon,
      offerType: offerType,
      sal_id: salon.sal_id
    });
  }
  getSalonNameFirstCharacte(salonName: string): string {
    if (salonName.trim().length === 0) {
      return salonName;
    }
    var shortSalonName = "";
    let charCount = 0;
    let SplitSalonNames = salonName.split(" ");
    SplitSalonNames.forEach(element => {
      let salName: string = element;

      if (charCount < 4 && salName.trim().length > 0) {
        if (
          charCount === 3 &&
          salName.length === 1 &&
          SplitSalonNames.length > 3
        ) {
        } else {
          shortSalonName += salName.slice(0, 1);
          charCount += 1;
        }
      }
    });

    return shortSalonName.toLocaleUpperCase();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.customHandleBackButton();
      },
      10
    );
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage);
      }
    }

    // this.nativePageTransitions.slide(this.options)
    // if (this.globalSearch.search_keyword.length != 0) {
    // this.navCtrl.push(LookingForPage, { isHideTab: "1" }, { animate: false })
    // } else {
    //   this.navCtrl.push(AppointmentsCardPage, { isHideTab: "1" }, { animate: false })
    // }
  }
  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  localSearchArr = [];
  SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[] = [];
  SQL_TABLE_SEARCh = "search";
  filteredTags = [];
  public highlight(SubStyleName: string) {
    // console.log(SubStyleName);

    if (!this.txtSearchSalon) {
      return SubStyleName;
    }
    return SubStyleName.replace(
      new RegExp(this.txtSearchSalon, "gi"),
      match => {
        return '<span class="highlightText">' + match + "</span>";
      }
    );
  }
  btnStyleTapped(objSearch, index) {
    let objSubStyle: SALON_SERVICES_SEARCH = objSearch;

    if (!objSubStyle) {
      return;
    }

    this.showTagsPopOver = false;

    if (objSubStyle.keyword.length == 0) {
      this.serviceManager.makeToastOnFailure("Tag Name is missing");
      return;
    }

    this.txtSearchSalon = objSubStyle.keyword;

    let type = objSubStyle.type;
    if (type == "service") {
      console.log(objSubStyle);
      let sal_ids = "";
      let index = 0;
      let salonsServices = [];
      this.salonModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          // this.salonModal.getSalonServicesBySerId(objSubStyle.refer_id).then(res => {
          this.salonServicesModal
            .getSalonServicesBySerId(objSubStyle.refer_id)
            .then(res => {
              salonsServices = res;
              console.log("AllSalonFromDb", salonsServices.length);
              salonsServices.forEach(element => {
                console.log("Block1", "Start");
                if (index == 0) {
                  sal_ids = element.sal_id;
                } else {
                  sal_ids = sal_ids + ", " + element.sal_id;
                }
                index = index + 1;
                console.log("SalId", element.sal_id + "SalId ");
              });

              console.log("Block1", "Completed");
              //block02 now we have all salId's get all salon from salon table against multiple sal_id's

              this.salonModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.salonModal
                    .getSalonsListingFromSqliteDB(sal_ids)
                    .then(res => {
                      this.allSalon = res;

                      this.allSalonBackUp = this.allSalon;
                    });
                }
              });
              //here get all salon agains all salid's
            });
        }
      });
      //  this.showAllSalons(objSubStyle.refer_id);
    } else if (type == "salon") {
      this.salonServicesModal.getSalonServices;
      let sal_id = objSubStyle.refer_id;
      this.salonModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.salonModal.getSalonDetailsFrom(sal_id).then(mySalon => {
            if (mySalon) {
              console.log(JSON.stringify(mySalon));
              this.nativePageTransitions.slide(this.options);

              this.navCtrl.push(SalonDetailsPage, {
                selectedSalon: mySalon,
                isFromBookAppointment: true,
                sal_id: mySalon.sal_id
              });
            }
          });
        }
      });
    }
  }
  // makeSalonFavoriteTapped(salon) {
  //   console.log(JSON.stringify(salon));

  //   let mySalon: Salon = salon
  //   let makeSalonFavorite: number
  //   if (mySalon.favsal === 1) {
  //     makeSalonFavorite = 0
  //   } else {
  //     makeSalonFavorite = 1
  //   }
  //   console.log(makeSalonFavorite);

  //   let params = {
  //     service: btoa('favorite'),
  //     sal_id: btoa(mySalon.sal_id),
  //     cust_id: btoa(this.myCustomer.cust_id),
  //     favorite: btoa(makeSalonFavorite.toString()),

  //   }

  //   this.serviceManager.getData(params).subscribe(res => {
  //     if (res['status'] === '1') {
  //       this.allSalon.forEach(salon => {
  //         if (salon.sal_id === mySalon.sal_id) {
  //           return salon.favsal = makeSalonFavorite
  //         }
  //       });
  //       this.salonModal.getDatabaseState().subscribe(ready => {
  //         if (ready) {
  //           this.salonModal.makeSalonFavorite(mySalon.sal_id, makeSalonFavorite).then(isSalonMarkedFavorite => {
  //             if (isSalonMarkedFavorite) {

  //               // this.salonModal.getSalonDetailsFrom().then()
  //             }
  //           })
  //         }
  //       })
  //     } else {

  //     }

  //   })
  // }
  getCustomer() {

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            if (customer) {
              this.myCustomer = customer;
              console.log("came in got customer");

              // this.getdata(this.myCustomer)
            } else {
              // this.navCtrl.setRoot(ActivateSalonPage)
            }
          },
          error => {
            console.log("error: while getting customer");
          }
        );
      }
    });
  }

  //end favorite post section
  isScrollUp = false;
  didHideMainHeader = false;
  scrollHandler(event) {
    // return
    if (this.allSalon && this.allSalon.length <= 2) return;

    let elem: HTMLElement = this.content._scrollContent.nativeElement;

    if (event.directionY === "down") {
      this.isScrollUp = true;
      elem.setAttribute("style", "margin-top: 0px !important;");
      this.didHideMainHeader = true;
      if (event.scrollTop > 150 && !this.shouldShowMiniHeader) {
        this.zone.run(() => {
          // elem.animate(slideOutHeader)
          this.slideOutNow = true;
          timer(500).subscribe(() => {
            this.shouldShowMiniHeader = true;
          });
        });
      }
    } else {
      this.zone.run(() => {
        elem.setAttribute("style", "margin-top: 130px !important;");
        this.isScrollUp = false;
        this.slideOutNow = false;
        this.didHideMainHeader = false;
        this.shouldShowMiniHeader = false;
      });
    }

    this.keyboard.close();
    if (event.scrollTop <= 0) {
      elem.setAttribute("style", "margin-top: 130px !important;");
    }
  }
  //search over server
  searchOverInternet() {
    if (this.txtSearchSalon.length > 2) {
      this.showTagsPopOver = false;
      this.isServerSearching = true;
      this.searchDataOverServer();

    }
  }
  searchDataOverServer() {

    let loading = this.loadingController.create({
      content: "Fetching Doctors, Please Wait... 855"
    });
    loading.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      service: btoa("search_salons"),
      keywords: btoa(this.txtSearchSalon)
    };
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        response => {
          loading.dismissAll();
          if (response.salons) {
            this.serverSearchSalon = response.salons;
          }
          console.log(
            "serverSearchSalon",
            JSON.stringify(this.serverSearchSalon)
          );
          this.handel_lstSalonsService_Response(this.serverSearchSalon);
          // console.log('response',JSON.stringify(response))
          // this.allSalon = response["salons"];
          // this.allSalonBackUp = this.allSalon
          // if (this.allSalon != undefined && this.allSalon != null) {
          //   if (this.allSalon.length == 0) {
          //     this.noSalonFound = true;
          //   } else {
          //     this.noSalonFound = false;
          //   }
          // } else {
          //   this.noSalonFound = true;
          // }
          // this.serviceManager.setInLocalStorage(this.serviceManager.SALONS_NEAR_CUSTOMER, this.allSalon)
        },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
  }
  //save saerch salon and services to local db
  handel_lstSalonsService_Response(response) {
    if (this.serverSearchSalon && this.serverSearchSalon.length > 0) {
      this.getAndSetSalonOffer(this.serverSearchSalon);
    }
    let salonIds = this.getSalonIds(this.serverSearchSalon);
    //  this.insertSalServiceSub(response.sal_ser_sub_categories, salonIds);//8 second delay
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal
          .deleteMultipleSalonServiceCatogries(salonIds)
          .then(res => {
            this.salonServicesModal.saveSalonServicesCategoriesIntoDB(
              this.serverSearchSalon
            );
          });
        this.salonServicesModal
          .deleteMultipleSalonServices(salonIds)
          .then(res => {
            this.insertSalonServices(salonIds);
          });
      }
    });
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.deleteMultipleSalon(salonIds).then(res => {
          if (
            this.serverSearchSalon != undefined &&
            this.serverSearchSalon != null
          ) {
            this.sqlProvider.insert_salon_table(this.serverSearchSalon);
          }
        });
      }
    });
    // this.salonIdAll = salonIds
    // let get_current_date_time = this.serviceManager.getCurrentDateTime();
    // this.serviceManager.setInLocalStorage(this.serviceManager.LAST_FETCH_DATE_SALONS, get_current_date_time);
    // this.serviceManager.setInLocalStorage(this.serviceManager.SALONS_NEAR_CUSTOMER, this.allSalon)
    console.log("SalHoursCalculationStart......");
    this.calCulateSalHours(salonIds);
    console.log("EndSalHoursCalculationStart......");
  }
  calCulateSalHours(inactive_salons) {
    this.SalonHoursAll = [];
    this.serverSearchSalon.forEach(salon => {
      let sal_Id = salon.sal_id;
      let sal_hours = salon.sal_hours1;
      // console.log("SalHoursTo",JSON.stringify(sal_hours));
      let salonHours = {};
      let salonHoursKeys = [];
      salonHoursKeys = Object.keys(sal_hours);
      salonHours = sal_hours;
      let temp = {};
      salonHoursKeys.forEach(element => {
        let split = sal_hours[element].split("&");
        if (split[0] == split[1]) {
          temp[element] = "Off";
        } else {
          temp[element] =
            this.convert24Hrto12Hr(split[0]) +
            " - " +
            this.convert24Hrto12Hr(split[1]);
        }
      });
      salonHours = temp;
      salonHoursKeys.forEach(dayKey => {
        // console.log("KeyHere", JSON.stringify(dayKey))
        // console.log("KeyValue", JSON.stringify(salonHours[dayKey]))
        let salonHoursObject = {
          sal_id: Number(sal_Id),
          sal_hours: salonHours[dayKey],
          sal_day: dayKey
        };
        // console.log("ItemHours",JSON.stringify(salonHoursObject))
        this.SalonHoursAll.push(salonHoursObject);
      });
    });
    //insert sal Hours to DB
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (inactive_salons !== undefined && inactive_salons !== null) {
          this.sqlProvider
            .deleteSalHours(inactive_salons)
            .then(isDataInserted => { });
        }
        if (
          this.SalonHoursAll !== undefined &&
          this.SalonHoursAll !== null &&
          this.SalonHoursAll.length > 0
        ) {
          this.sqlProvider
            .saveIntoSalHoursTable(this.SalonHoursAll)
            .then(isDataInserted => {
              console.log("InsertedAllSalHours", isDataInserted);
            });
        }
      }
    });
  }
  convert24Hrto12Hr(time) {
    if (time === undefined || time === null) {
      this.navCtrl.pop();
      return;
    }
    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {

      time = time.slice(1); // Remove full string match value
      time[5] = +time[0] < 12 ? " AM" : " PM"; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    time[0] = this.padToTwo(time[0]);
    return time.join(""); // return adjusted time or original string
  }
  padToTwo(number) {
    if (number <= 9) {
      number = ("0" + number).slice(-2);
    }
    return number;
  }
  getAndSetSalonOffer(salons) {
    let so_ids = "";
    this.ALLOFFERS = [];
    salons.forEach(element => {
      if (element.offers !== null) {
        for (let index = 0; index < element.offers.length; index++) {
          const item = element.offers[index];
          // so_ids += item['so_id'] + ','
          this.ALLOFFERS.push(item);
        }
      }
    });
    // so_ids = so_ids.slice(0, so_ids.length - 1)//after loop
    this.saveSalonOffers(this.ALLOFFERS);
  }
  saveSalonOffers(ALLOFFERS) {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (ALLOFFERS != undefined && ALLOFFERS != null) {
          this.salonServicesModal.insert_salon_offer(ALLOFFERS);
        }
      }
    });
  }
  insertSalonServices(salonIds) {
    if (!this.serverSearchSalon) {
      return;
    }
    this.SalonsServices = [];
    this.serverSearchSalon.forEach(salon => {
      this.SalonsServices = this.SalonsServices.concat(salon.services);
    });
    if (this.SalonsServices && this.SalonsServices.length > 0) {
      this.serviceManager.setInLocalStorage(
        this.serviceManager.NEAREST_SALON_SERVICES,
        this.SalonsServices
      );
    }
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.saveSalonServicesIntoDB(this.serverSearchSalon);
      }
    });
  }
  getSalonIds(response) {
    let salonIds = "";
    let i = 0;
    response.forEach(element => {
      salonIds += element.sal_id + ",";
    });
    salonIds = salonIds.slice(0, salonIds.length - 1);
    return salonIds;
  }
  roundRatingUpToOneDecimal(ratingValue) {
    return Math.round(ratingValue).toFixed(1);
  }

}
