import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { NavController, NavParams, reorderArray, LoadingController, ViewController, Platform, Config } from 'ionic-angular';

import { DatePipe } from '@angular/common'

// import {Base64Provider} from './../../providers/base64/base64';
import { Notification } from '../../Interfaces/Notification'

import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { MainHomePage } from '../main-home/main-home';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { Events, MenuController } from 'ionic-angular';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';

import {
  ToastController,
} from 'ionic-angular';
import { Customer } from '../../providers/SalonAppUser-Interface';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { config } from '../../providers/config/config';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  public unregisterBackButtonAction: any;
  cust_id: string;
  cards: any;
  isSideMenu: string = '0';
  category: string = 'gear';
  allNotifications: Notification[] = [];
  allNotificationToInser: Notification[] = [];

  baseUrl = config.salonImgUrl
  public publicUser=false
  public date: string = new Date().toISOString();
  public myCustomer: Customer



  constructor(
    public sqlProvider: SqliteDbProvider,
    private statusBar: StatusBar,
    private menu: MenuController,

    private loadingController: LoadingController,
    public navCtrl: NavController, public datepipe: DatePipe,
    public storage: Storage,
    private viewCtrl: ViewController,
    public navParams: NavParams,
    public platform: Platform,
    private toastCtrl: ToastController,
    public global: GlobalServiceProvider,
    public events: Events,
    public serviceManager: GlobalProvider,
    public customerModal: CustomerModal,
    private nativePageTransitions: NativePageTransitions,
  ) {

    //end temp
    platform.ready().then(() => {

      if (platform.is('cordova')) {

        //Subscribe on pause
        this.platform.pause.subscribe(() => {
          // alert("On Resume subscrice")

        });

        //Subscribe on resume
        this.platform.resume.subscribe(() => {
          // alert("On Resume")
          this.pageRefresh();
        });
      }
    });

    //temp code

    console.log("baseUrl", this.baseUrl)
    this.menu.swipeEnable(true);
    this.needToUpdateData();
    this.isSideMenu = navParams.get('isSideMenu');

    this.getDateFormated('2017-12-11');
    let tab: { tel: number, name: string }[] = [];
    //   this.myFunction();
    let date = new Date('MMM DD, YYYY HH:mm');

    this.cards = new Array(3);
    //2017-12-07 09:03:32
    //console.log('dateYear',date.getFullYear()+':'+date.getMonth()+':'+date.getDay()+'Time:'+date.getHours);
    var index = 0;
    this.allNotifications.forEach(element => {
      if (index == 0) {
        // this.allNotifications.splice(this.allNotifications.indexOf(element), 1);
      }
    });
    this.allNotifications.forEach(element => {
      // console.log('element', element);
    });
  }

  pageRefresh() {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
            this.sqlProvider.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.sqlProvider.getAllNotifications().then(notification => {
                  console.log("AllNotifications", JSON.stringify(notification))
                  this.allNotifications = notification;
                  if (this.allNotifications.length > 0) {
                    this.serviceManager.getData(false);
                  } else {
                    this.serviceManager.getData(true);
                  }
                })
              }
            })
          }
        }, error => {
          console.log('error: while getting customer');
        })
      }
    })
  }
  goToRegisterScreen(){
    this.navCtrl.push(LoginSignUpPage, { animate: false })
  }

  ionViewWillEnter() {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if(!customer){
            // alert('customer not exist')
            this.publicUser=true
          }else{
            this.publicUser=false
          }
          
          if (customer) {
           

            this.myCustomer = customer
            this.sqlProvider.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.sqlProvider.getAllNotifications().then(notification => {
                  console.log("AllNotifications", JSON.stringify(notification))
                  this.allNotifications = notification;
                  if (this.allNotifications.length > 0) {
                    this.getNotifications(null, false)
                  } else {
                    this.getNotifications(null, true)
                  }
                })
              }
            })
          }
        }, error => {
          console.log('error: while getting customer');
        })
      }
    })

  }
  //back button
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    //this.global.tabIndex = "2";
  }
  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    /*
    if (this.isSideMenu == '1') {
      this.navCtrl.setRoot(MainHomePage);
    } else {
      this.navCtrl.parent.select(0);
    }
    */
    //this.global.tabIndex = "0";

    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage)
      }
    }

    // this.nativePageTransitions.slide(this.options)
    // this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
  }

  //end back button

  getNotifications(ionRefresher, status) {

    if (!this.myCustomer) {
      setTimeout(() => {
        if (ionRefresher) ionRefresher.complete();
      }, 2000);
      return
    }
    let notificationController = this.loadingController.create({
      content: "Fetching Notifications"
    });
    if (status) {
      notificationController.present();
    }
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('get_notifications'),
      cust_id: btoa(this.myCustomer.cust_id),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        this.allNotificationToInser = []
        setTimeout(() => {
          if (ionRefresher) ionRefresher.complete();
        }, 2000);
        this.allNotificationToInser = response.notifications;

        this.sqlProvider.getDatabaseState().subscribe(ready => {
          if (ready) {
            if (this.allNotificationToInser !== undefined && this.allNotificationToInser !== null && this.allNotificationToInser.length > 0) {
              console.log("NotificatioNew", JSON.stringify(this.allNotificationToInser))
              this.sqlProvider.insert_notification(this.allNotificationToInser).then(status => {
                this.updateNotfication();
              })
            } else {
              this.updateNotfication();
            }

          }
        })
        if (status) {
          notificationController.dismiss();
        }

      },
        error => {
          setTimeout(() => {
            if (ionRefresher) ionRefresher.complete();
          }, 2000);
          if (status) {
            notificationController.dismiss();
          }
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });
  }
  updateNotfication() {
    this.sqlProvider.getAllNotifications().then(notification => {
      console.log("TottalNotInDb", notification.length)
      this.allNotifications = notification;
    })
  }
  needToUpdateData() {

    this.events.subscribe('NeedToUpdate', (value) => {

      let againPushNotification = false

      for (let i = 0; i < this.navCtrl.length(); i++) {
        if (!againPushNotification) {
          let v = this.navCtrl.getViews()[i];

          if (v.component.name === 'NotificationsPage') {
            againPushNotification = true
          }
        }

      }

      if (value && !againPushNotification) {

        let index = null
        try {
          index = this.navCtrl.parent.getSelected().index;
        } catch (error) {

        }

        if (index == 2) {
          this.serviceManager.getData(false)
        }
      }
    });
  }

  ionViewDidLoad() {
  }
  deleteNotif(notification, notifi) {
    this.deleteNotificationFromDb(notifi.not_id);
    this.allNotifications.splice(notification, 1);
  }

  deleteNotificationFromDb(notificationId) {
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.deleteSalNotifiById(notificationId).then(stauts => {

        })
      }
    })
  }
  itemReordered($event) {
    reorderArray(this.allNotifications, $event);
  }
  archiveTodo(item) {
    //this.todos.splice(item, 1);
    this.allNotifications.splice(item, 1);
  }
  getTime(Datetime) {
    let timeOnly = Datetime.split(" ");
    timeOnly = timeOnly[1];
    timeOnly = this.convert24Hrto12Hr(timeOnly);
    timeOnly = timeOnly.split(":");

    let removeSecond = timeOnly[2];
    removeSecond = removeSecond.split(" ");
    removeSecond = removeSecond[1];
    timeOnly = timeOnly[0] + ":" + timeOnly[1] + " " + removeSecond;
    return timeOnly;
  }
  convert24Hrto12Hr(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string  
  }

  getDateFormated(Datetime) {
    //Datetime yyyy-MM-dd hh:mm:ss
    let fullDate = Datetime.split(" ");
    fullDate = fullDate[0];
    let date = new Date();
    let latest_date = this.datepipe.transform(date, 'yyyy-MM-dd');//2017-12-12 04:38:54 full format yyyy-MM-dd hh:mm:ss

    if (fullDate == latest_date) {
      return 'Today';
    } else {
      let dt1 = new Date(latest_date);
      let dt2 = new Date(fullDate);

      let diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));

      if (diff == -1) {
        return 'Yesterday';
      } else {
        let dateFormated = this.datepipe.transform(fullDate, 'EEE, MMM dd yyyy');
        return dateFormated;
      }
    }
  }
  isDateChanged(dateInList, datePreviousInList) {
    //allNotifications.notifications[i-1].not_datetime!=notification.not_datetime
    dateInList = dateInList.split(" ");
    dateInList = dateInList[0];
    datePreviousInList = datePreviousInList.split(" ");
    datePreviousInList = datePreviousInList[0];
    if (dateInList != datePreviousInList) {
      return true
    } else {
      return false;
    }
  }
  isDateSame(dateInList, datePreviousInList) {

    dateInList = dateInList.split(" ");
    dateInList = dateInList[0];
    datePreviousInList = datePreviousInList.split(" ");
    datePreviousInList = datePreviousInList[0];
    if (dateInList == datePreviousInList) {
      return true
    } else {
      return false;
    }
  }
  popPage() {
    this.navCtrl.parent.select(0);
    /*
    this.navCtrl.setRoot(MainHomePage,{
      isHideTab:"1"
     });
     this.navCtrl.popToRoot()
      .then(data => {
        const index = this.viewCtrl.index; 
        for(let i = index; i > 0; i--){
            this.navCtrl.remove(i);
      }
     });
  */
  }
  goBack() {
    //this.global.tabIndex = "0";
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
  }
}
