import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { MainHomePage } from '../main-home/main-home';
import { LoadingController } from 'ionic-angular';
import { GlobalProvider } from './../../providers/global/global';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { MenuController } from 'ionic-angular';
/**
 * Generated class for the TermsAndConditionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-terms-and-conditions',
  templateUrl: 'terms-and-conditions.html',
})
export class TermsAndConditionsPage {
  public unregisterBackButtonAction: any;
  public terms_and_conditions: any;

  constructor(
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    private loadingController: LoadingController,
    public nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
  ) {
    this.customBackButton();
  }
  customBackButton() {
    this.platform.registerBackButtonAction(() => {
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsAndConditionsPage');
  }
  goBack() {
    this.navCtrl.setRoot(MainHomePage);
  }

  ionViewWillEnter() {
    this.getTermsAndConditions();

  }

  //custom back button
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.setRoot(MainHomePage);
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }

  }
  //end custom back button 
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) this.navCtrl.pop()
    else this.navCtrl.setRoot(MainHomePage)
  }
    getTermsAndConditions() {
      let loading = this.loadingController.create({ content: "Please Wait..." });
      loading.present();
      var params = {
        device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
        service: btoa("get_config"),
      }
      this.serviceManager.getData(params)
        .retryWhen((err) => {
          return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            }
            else {
              throw (err);
            }
          }, 0).delay(1000)
        })
        .subscribe((response) => {
          loading.dismiss()
          // alert('response')
          //  this.saveHomePageCounts(response)
          if (response.config) {
            this.terms_and_conditions = response.config.terms_and_conditions;

            this.serviceManager.removeFromStorageForTheKey(this.serviceManager.PRIVACY_POLICY)
            localStorage.removeItem(this.serviceManager.PRIVACY_POLICY)

          }
          console.log('privacy_policy_test', this.terms_and_conditions)
          // console.log('Response',JSON.stringify(response))
        },
          error => {
            loading.dismissAll();
            this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          });
    }

  }