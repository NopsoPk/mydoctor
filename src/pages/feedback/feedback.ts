import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { MainHomePage } from '../main-home/main-home';
import { MenuController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular'
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { Customer } from '../../providers/SalonAppUser-Interface';
import { Keyboard } from '@ionic-native/keyboard';
import { AppVersion } from '@ionic-native/app-version';
/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {
  versionName: any;
  versionCode: any;
  platformName:any;
  AppName: any;
  packageName: any;
  valueforngif = true;
  public myCustomer: Customer
  cust_id: string = "";
  UserFeedback: string = "";
  public unregisterBackButtonAction: any;
  icon: string = "./assets/imgs/app_feedback.png";
  tokenGcm: any;
  constructor(
    private appVersion: AppVersion,
    public keyBoard: Keyboard,
    public navCtrl: NavController, public navParams: NavParams,
    private menu: MenuController,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public platform: Platform,
    public storage: Storage,
    public customerModal: CustomerModal,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
  ) {

    this.storage.get(this.serviceManager.CUSTOMER_DATA).then((value) => {
      this.cust_id = value.cust_id;
    }).catch((Error) => {
    });
    this.getAppVersionDetails();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }

  goBack() {
    this.navCtrl.setRoot(MainHomePage);
  }

  sendFeedback() {
    console.log('cust_id', this.cust_id);
    console.log('feedback', this.UserFeedback.length);
    if (this.UserFeedback.length == 0) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgAppointmentFeedback)
    } else {
      let notificationController = this.loadingController.create({
        content: "Please wait..."
      });
      notificationController.present();
      var params = {
        service: btoa('sal_app_comments'),
        cust_id: btoa(this.myCustomer.cust_id),
        sac_comments: btoa(this.UserFeedback),
        app_version: btoa( 'packageName:'+this.packageName+':platform:'+this.platformName+':versionCode:'+this.versionCode+':versionName:'+this.versionName),
      }
      console.log('paramsNew'+JSON.stringify(params))
      this.serviceManager.getData(params)
        .retryWhen((err) => {
          return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            }
            else {
              throw (err);
            }
          }, 0).delay(1000)
        })
        .subscribe((response) => {
          console.log('AllResponse', response.notifications);
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure("Thanks for your feedback!");
          this.navCtrl.setRoot(MainHomePage);

        },
          error => {
            notificationController.dismissAll();
            this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
            console.log('something went wrong', error);
          }
        );
    }
    /*
        let notificationController = this.loadingController.create({
          content: "Updating preferences!"
        });
        notificationController.present();
        var params = {
          service: btoa('sal_app_comments'),
          cust_id: btoa(this.cust_id),
          sac_comments: btoa(this.UserFeedback),
        }
        this.serviceManager.getData(params)
          .subscribe((response) => {
            console.log('AllResponse', response.notifications);
            notificationController.dismiss();
          }
          );
          */
  }
  checkAppVersion(){
    
  }
// 
  getAppVersionDetails(){

    if (this.platform.is('ios')) {
        this.platformName="ios";
    }else{
      this.platformName="android";
    }

    this.platform.ready().then((readySource) => {
      console.log('Platform ready from', readySource);
     // this.checkLocation();
      // Platform now ready, execute any required native code
    });
    this.appVersion.getAppName().then((s) => {
      this.AppName = s;
      console.log('AppName'+this.AppName)
    })
    this.appVersion.getVersionCode().then((s) => {
      this.versionCode = s;
      console.log('versionCode'+this.versionCode)
    })
    this.appVersion.getVersionNumber().then((s) => {
      this.versionName = s;
      console.log('versionName'+this.versionName)
      this.appVersion.getPackageName().then((s) => {
        this.packageName = s;
        console.log('packageName'+this.packageName)
      })
    })
  }

  getCustomer() {
    if (this.myCustomer ) {
      return
    }
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer
          } 
        }, error => {
          
        })
      }
    })
  }
  //custom back button
  ionViewDidEnter() {
    this.keyBoard.onKeyboardShow().subscribe(() => { this.valueforngif = false })
    this.keyBoard.onKeyboardHide().subscribe(() => { this.valueforngif = true })
    this.getCustomer();
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.setRoot(MainHomePage);
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
  }
  //end custom back button 
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
}
