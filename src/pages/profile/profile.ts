import { CustomerModal } from "./../../providers/CustomerModal/CustomerModal";
import { config } from "./../../providers/config/config";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { Component } from "@angular/core";
import { Http, HttpModule, Headers, RequestOptions } from "@angular/http";
import {
  IonicPage,
  NavParams,
  NavController,
  ViewController,
  LoadingController,
  Platform,
  ModalController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { MainHomePage } from "../main-home/main-home";
import { MenuController } from "ionic-angular";

import { EmailComposer } from "@ionic-native/email-composer";
import { File } from "@ionic-native/file";
import { FileTransfer } from "@ionic-native/file-transfer";
import { FilePath } from "@ionic-native/file-path";
import { Camera } from "@ionic-native/camera";
import { ActionSheetController, ToastController } from "ionic-angular";
declare var cordova: any;
import { Base64 } from "@ionic-native/base64";
import { Events } from "ionic-angular";
import { GlobalServiceProvider } from "../../providers/global-service/global-service";
import { GlobalProvider } from "../../providers/global/global";
import { Customer } from "../../providers/SalonAppUser-Interface";
import { Keyboard } from "@ionic-native/keyboard";
import { Geolocation } from "@ionic-native/geolocation";
import { AppMessages } from "../../providers/AppMessages/AppMessages";

@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };
  valueforngif = true;

  isCustLatLngMissing: boolean = false;
  isGenderMale = true;
  lastImage: string = null;
  filePathLocal: string = null;
  objLogedInCustomer: any;
  myCustomer: Customer;
  public latitude;
  public longitude;

  public unregisterBackButtonAction: any;

  isSideMenu: string = "0";
  cust_pic_dummy_base =
    "iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAALvSURBVHhe7dq/axRBGMbxU1FQC8FGsRdEKwkWgmjjj525UxANiIiIf4BWomBxKBa52QvYBLQQtFDwWhtFiLnZJFoEBdHawtZCCxGJcs6EqS6Tc3dvL77E7weeKrmZ5X2ySfbmagAAAACAv7v7+uSOVpZcTjN1x1j1MLX6gcttY/WFyfnj28O3YdTaWXIotWra5Xea6d4KWXR51p5J9oeXoWrpi2NbTaae9A1+cFxp7jVT9xfGNoZlUIXW7KldbrjvokPPEVdKd+Ll0W1hOQyjudDY4gb6JjboInFrPO90xjeEZVGWuzPuxQZcJq6UW2FZlDGRndjnhvgrNtxSseq7//UXlkdR7t/Yx9HBDpfJsDyKaH4Y3+SG97VvmFXkU9gCRbRsciQyzErSnmvsCdsgL9PVl2LDrCLuaT4J2yCvtKuvxYZZRfxbLmEb5NXO1NXYMCuJrZ8L2yAv01VnosOsIl19OGyDvFoz9d3RYQ6fRd4NLslk+mNkoMPFqumwPIoaxR929+R/MSyPoibnz252Q/zcP9SycXfc+2avuT4sjzJaVjX8uUZswIVi1U8z2zgYlsUw3E/2jeiQC4Rnj4qlVl9xg/VHs9GBD8gP93fjfFgGVfLn6e5ueRsZ+kqZ4Vx9xHq92jqT1U8bq54aq78tK8GqL+5uepRmSoWXYLX4I9ml8/a55IC/E8wrvdMXFr4MAADwH1r6oLTV11c3yd6wPfr5N/qWPUOMODyxD0AhwlCIMBQiDIUIQyHCUIgwFCIMhQhDIcJQiDAUIgyFCEMhwlCIMBQijD8samf1sbwxVt+MDblIKKRCfpixIRcJhVSIQoShEGEoRBgKEYZChKEQYShEGAoRhkKEoRBhKEQYChGGQoShEGEoRBgKEYZChKEQYShkxMw/+NRJ0fhrDJe79lGIMBQiDIUIQyHCUIgwFCIMhQhDIcJQiDAUIgyFCEMhwlCIMBQiDIUIY6xup5nqSI6/xnC5AAAAALCG1Wp/AB9dbqj3kRxtAAAAAElFTkSuQmCC";
  custDetails: string = "";
  cust_id: string;
  cust_name: string = "";
  cust_name_temp = "";
  cust_email: string = "";
  cust_address: string = "";
  cust_city: string = "";
  cust_province: string = "";

  cust_pic: string;
  // cust_zip: string;
  cust_pic_name: string = "riz";
  cust_phone: string;
  cust_gender: string;
  customerGender: string;

  errorMessageName = "";
  errorMessageEmail = "";

  errorMessageAddress = "";
  errorMessageCity = "";
  errorMessageProvince = "";

  errorMessageLocation = "";

  isValidAdress = true;
  isValidCity = true;
  isValidProvince = true;

  isValidName = true;
  isValidEmail = true;
  isValidLocation = true;
  isZipCodeValid = true;
  currentImage = null;
  base64Image: any = "assets/imgs/user_profile_pic.png";
  base64ImageToSendServer: string = "";
  Image: string;
  public customerDeviceToken: string;
  public pkCitiesArray: string[] = []; // config.pakistan.cities

  constructor(
    private menu: MenuController,
    public http: Http,
    public events: Events,
    public toastCtrl: ToastController,
    private platform: Platform,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private filePath: FilePath,
    public actionSheetCtrl: ActionSheetController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    private viewCtrl: ViewController,
    private loadingController: LoadingController,

    private emailComposer: EmailComposer,
    public global: GlobalServiceProvider,
    public serviceManager: GlobalProvider,
    public customerModal: CustomerModal,
    private nativePageTransitions: NativePageTransitions,
    public keyBoard: Keyboard,
    public geolocation: Geolocation,
    public modalCtrl: ModalController,
    private base64: Base64
  ) {
    this.customerDeviceToken = this.serviceManager.getFromLocalStorage(
      this.serviceManager.GCM_TOKEN
    );

    this.storage.get("base64Image").then(value => {
      if (value && value.length != 0) {
        this.base64Image = value;
      }
    });
    this.isSideMenu = navParams.get("isSideMenu");
    let _isCustLatLngMissing = navParams.get("isCustLatLngMissing");

    if (_isCustLatLngMissing) {
      this.isCustLatLngMissing = _isCustLatLngMissing;
    }
  }

  ionViewWillEnter() {
    for (let i = 0; i < this.navCtrl.length(); i++) {
      let v = this.navCtrl.getViews()[i];
      console.log(v.component.name);
    }

    this.errorMessageName = "";
    this.errorMessageEmail = "";
    this.errorMessageLocation = "";
    this.isValidName = true;
    this.isValidEmail = true;
    this.isValidLocation = true;
    if (!this.myCustomer) {
      this.getCustomer();
    } else {
      this.populateCustomerProfileData(this.myCustomer);
    }
  }
  endEndEditing(value) {
    if (this.cust_name && this.cust_name.trim().length === 0) {
    }
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options);
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage);
    }

    // let options: NativeTransitionOptions = {
    //   direction: 'right',
    //   duration: 500,
    //   slowdownfactor: 3,
    //   slidePixels: 20,
    //   iosdelay: 100,
    //   androiddelay: 150,
    //   fixedPixelsTop: 0,
    //   fixedPixelsBottom: 0
    // };
    // if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    // this.appCtrl.getRootNav().setRoot(MainHomePage)
  }
  handleReturnkey() {
    this.keyBoard.close();
  }

  ionViewDidEnter() {
    this.keyBoard.onKeyboardShow().subscribe(() => {
      this.valueforngif = false;
    });
    this.keyBoard.onKeyboardHide().subscribe(() => {
      this.valueforngif = true;
    });

    if (this.isCustLatLngMissing) {
      this.serviceManager.makeToastOnFailure(AppMessages.msglocationGetting);
    }
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.customHandleBackButton();
      },
      10
    );
  }
  private customHandleBackButton(): void {
    // this.global.tabIndex = "0";
    // this.nativePageTransitions.slide(this.options)
    // this.navCtrl.push(MainHomePage, {
    //   isHideTab: "1",
    //   myCustomer: this.myCustomer
    // }, { animate: false })
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage);
      }
    }
  }
  //picture code
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Load from Library",
          handler: () => {
            this.accessGallery();
          }
        },
        {
          text: "Use Camera",
          handler: () => {
            this.captureImageFromCamera();
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }
  accessGallery() {
    var options = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL
    };
    this.camera.getPicture(options).then(
      imagePath => {
        this.base64Image = "data:image/jpeg;base64," + imagePath;
        this.base64ImageToSendServer = imagePath;
        this.storage.remove("base64Image").then(res => {
          this.storage.set("base64Image", this.base64Image);
          this.events.publish("base64Image:changed", this.base64Image);
        });
      },
      err => {
        console.log(err);
      }
    );
  }
  captureImageFromCamera() {
    var options = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL
    };
    this.camera.getPicture(options).then(
      imagePath => {
        this.base64Image = "data:image/jpeg;base64," + imagePath;
        this.base64ImageToSendServer = imagePath;

        this.storage.remove("base64Image").then(res => {
          this.storage.set("base64Image", this.base64Image);
          this.events.publish("base64Image:changed", this.base64Image);
        });
      },
      err => {
        console.log(err);
      }
    );
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: "top"
    });
    toast.present();
  }

  populateCustomerProfileData(customerData: Customer) {
    this.cust_id = customerData.cust_id;
    this.cust_name = customerData.cust_name;
    this.cust_address = customerData.cust_address;
    this.cust_city = customerData.cust_city;
    this.cust_province = customerData.cust_province;
    this.cust_email = customerData.cust_email;
    this.cust_phone = customerData.cust_phone;
    //this.cust_zip = customerData.cust_zip;
    this.cust_pic_name = customerData.cust_pic;
    this.cust_gender = customerData.cust_gender;
    console.log("this.cust_pic_name " + this.cust_pic_name);

    let gender = Number(customerData.cust_gender);
    if (gender && gender === 1) {
      this.isGenderMale = true;
      this.customerGender = "male";
    } else {
      this.isGenderMale = false;
      this.customerGender = "female";
    }

    if (
      this.base64Image === "assets/imgs/user_profile_pic.png" &&
      this.cust_pic_name &&
      this.cust_pic_name.length > 0
    ) {
      this.base64Image = config.custImg + this.cust_pic_name;
      this.storage.set("base64Image", this.base64Image);
    } else {
      console.log("came in else block finally");
      console.log(this.base64Image);
      console.log(this.base64Image);
    }
    // this.txtCustZipEndEditing(this.cust_zip)
    this.txtCustNameEndEditing(this.cust_name);
    this.txtCustEmailEndEditing(this.cust_email);
  }

  popPage() {
    this.navCtrl.parent.select(0);
  }
  goBack() {
    if (this.isCustLatLngMissing) {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.popToRoot().then(res => {
          this.navCtrl.setRoot(MainHomePage);
        });
      } else {
        this.navCtrl.setRoot(MainHomePage);
      }
    } else {
      //this.global.tabIndex = "0";
      this.nativePageTransitions.slide(this.options);
      this.navCtrl.push(
        MainHomePage,
        {
          isHideTab: "1",
          myCustomer: this.myCustomer
        },
        { animate: false }
      );
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  updateCustomer() {
    if (!this.isValidEmail || !this.isValidName || !this.isValidLocation) {
      return;
    }

    let notificationController = this.loadingController.create({
      content: "Updating preferences!"
    });
    notificationController.present();
    this.myCustomer.cust_name = this.cust_name;
    this.myCustomer.cust_email = this.cust_email;
    this.myCustomer.cust_address = this.cust_address;
    this.myCustomer.cust_city = this.cust_city;
    this.myCustomer.cust_province = this.cust_province;

    console.log("customer fresh data", this.myCustomer);

    let cust_Gender = "";
    if (this.isGenderMale) {
      cust_Gender = this.serviceManager.CUSTOMER_GENDER_MALE;
    } else {
      cust_Gender = this.serviceManager.CUSTOMER_GENDER_FEMALE;
    }
    // alert('cust_province'+this.myCustomer.cust_province)
    var params = {
      service: btoa("register"),
      cust_id: btoa(this.myCustomer.cust_id),
      cust_address: btoa(this.myCustomer.cust_address),
      cust_city: btoa(this.myCustomer.cust_city),
      cust_province: btoa(this.myCustomer.cust_province),
      cust_name: btoa(this.myCustomer.cust_name),
      cust_email: btoa(this.myCustomer.cust_email),
      cust_gender: btoa(cust_Gender),
      cust_phone: btoa(this.myCustomer.cust_phone),
      cust_device_id: btoa(this.customerDeviceToken),
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      cust_device_type: btoa("2")
    };
    let headers = new Headers({
      "Content-Type": "application/json"
    });
    let options = new RequestOptions({ headers: headers });

    var data = "";
    if (this.base64ImageToSendServer.length > 0) {
      data = JSON.stringify({
        cust_pic: this.base64ImageToSendServer
      });
    }
    let jsonData = JSON.stringify(params);
    console.log("url");
    // console.log(config.BaseURL + jsonData);
    // console.log(config.BaseURL + jsonData);

    this.http
      .post(config.BaseURL + jsonData, data, options)
      .map(Response => Response.json())
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          notificationController.dismiss();

          if (res["status"] === "1") {
            this.customerModal.TruncCateCustomerTable().then(res => {});
            this.saveCustomerIntoDB();
          }
        },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("error: ");
        }
      );
  }
  checkResgisterStatus(objCustomer) {
    if (objCustomer.status == "1") {
      //here save the status of login
      console.log("Customer Updated");

      var _customer: Customer;

      if (this.myCustomer) {
        this.customerModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.customerModal.createCustomerTable().then(isTableCreated => {
              if (isTableCreated === true) {
                this.customerModal
                  .InsertInToCustomerTable(this.myCustomer)
                  .then(
                    isCustomerInserted => {
                      if (isCustomerInserted === true) {
                        console.log("success: InsertInToCustomerTable ");
                      } else {
                        console.log(
                          "error: InsertInToCustomerTable ",
                          isCustomerInserted
                        );
                      }
                    },
                    error => {
                      console.log("error: ", error.message);
                    }
                  );
              }
            });
          }
        });
      }
    }
  }

  GenderTapped() {
    this.isGenderMale = !this.isGenderMale;
  }
  genderMaleTapped() {
    this.myCustomer["cust_gender"] = "1";
    this.isGenderMale = true;
  }
  genderFemaleTapped() {
    this.isGenderMale = false;
    this.myCustomer["cust_gender"] = "0";
  }

  saveCustomerIntoDB() {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.createCustomerTable().then(
          isTableCreated => {
            if (isTableCreated === true) {
              this.customerModal.InsertInToCustomerTable(this.myCustomer).then(
                isCustomerInserted => {
                  if (isCustomerInserted === true) {
                    console.log("success: InsertInToCustomerTable ");
                    console.log(JSON.stringify(this.myCustomer));
                    this.serviceManager.makeToastOnSuccess(
                      AppMessages.msgSaveSettings
                    );
                    this.serviceManager.setInLocalStorage(
                      this.serviceManager.IS_USER_LOGIN,
                      true
                    );
                    this.storage.set("isLogin", true);
                    this.events.publish("customer:changed", this.myCustomer);
                    this.events.subscribe("getCustomer", () => {
                      this.events.publish("sendCustomer", this.myCustomer);
                    });

                    this.goBack();
                  } else {
                    // this.serviceManager.makeToastOnFailure('error: InsertInToCustomerTable ' + isCustomerInserted, 'top')
                    console.log(
                      "error: InsertInToCustomerTable ",
                      isCustomerInserted
                    );
                  }
                },
                error => {
                  // this.serviceManager.makeToastOnFailure('error: ' + error.message, 'top')
                  console.log("error: ", error.message);
                }
              );
            } else {
              // this.serviceManager.makeToastOnFailure('Error occoured while creating Customer Table', 'top')
            }
          },
          error => {
            // this.serviceManager.makeToastOnFailure('error: ' + error.message, 'top')
            console.log("error: ", error.message);
          }
        );
      }
    });
  }
  getCustomer() {
    console.log("came in get customer");

    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            if (customer) {
              console.log("my customer FROM DB");
              console.log(JSON.stringify(customer));
              this.myCustomer = customer;
              this.populateCustomerProfileData(this.myCustomer);
            } else {
              this.myCustomer = {};
              // this.navCtrl.setRoot(ActivateSalonPage)
            }
          },
          error => {
            console.log("error: while getting customer");
          }
        );
      }
    });
  }

  txtCustNameEndEditing(value) {
    if (this.cust_name.trim().length === 0) {
      this.errorMessageName = "Please provide your name";
      this.isValidName = false;
    } else {
      this.isValidName = true;
      this.errorMessageName = "";
    }
  }

  txtCustEmailEndEditing(value) {
    if (
      this.cust_email.trim().length === 0 ||
      !this.validateEmail(this.cust_email)
    ) {
      this.errorMessageEmail = "Please provide your valid email address";
      this.isValidEmail = false;
    } else {
      this.isValidEmail = true;
      this.errorMessageEmail = "";
    }
  }

  txtCustAddressEndEditing(value) {
    if (this.cust_address.trim().length === 0) {
      this.errorMessageAddress = "Please provide your address";
      this.isValidAdress = false;
    } else {
      this.isValidAdress = true;
      this.errorMessageAddress = "";
    }
  }

  txtCustCityEndEditing(value) {
    if (this.cust_city.trim().length === 0) {
      this.errorMessageCity = "Please provide your city name";
      this.isValidCity = false;
    } else {
      this.isValidCity = true;
      this.errorMessageCity = "";
    }
  }

  txtCustProvinceEndEditing(value) {
    if (this.cust_province.trim().length === 0) {
      this.errorMessageProvince = "Please provide your province name";
      this.isValidProvince = false;
    } else {
      this.isValidProvince = true;
      this.errorMessageProvince = "";
    }
  }

  txtCustZipEndEditing(value) {
    // if (this.cust_zip.trim().length === 0) {
    //   this.errorMessageLocation = 'Please provide your post code'
    //   this.isValidLocation = false
    // } else {
    //   this.isValidLocation = true
    //   this.errorMessageLocation = ''
    // }
  }

  getCurrentLocation() {
    this.geolocation
      .getCurrentPosition()
      .then(location => {
        this.latitude = location.coords.latitude;
        this.longitude = location.coords.longitude;
      })
      .catch(e => {
        this.latitude = "";
        this.longitude = "";
        console.log("error: while getting locaiton");
      });
  }

  shouldBeginEditing(event) {
    // if (this.lastSearcheArray && this.lastSearcheArray.length > 0 && this.txtSearchStyles.trim().length === 0) {
    //   this.showCloseButton = false
    //   this.shouldShowRecentSearches = true
    // }
  }
  shouldChangeCharacter(searchText: string) {
    // console.log(config.pakistan.cities.filter(city => city.includes(searchText)))
    this.pkCitiesArray = config.pakistan.cities.filter(city =>
      city.includes(searchText)
    );
    // words.filter(word => word.length > 6);
  }

  public highlight(SubStyleName: string) {
    if (!this.cust_phone) {
      return SubStyleName;
    }
    return SubStyleName.replace(new RegExp(this.cust_phone, "gi"), match => {
      return '<span class="highlightText">' + match + "</span>";
    });
  }
  customerCityTapped() {
    // let popover = this.modalCtrl.create(SearchPage, {
    //   cities: config.pakistan.cities,
    // }, { cssClass: 'city-popup-modal' });
    // popover.present({
    // });
    // popover.onDidDismiss(data => {
    //   alert(data)
    // })
  }
}
