import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, Navbar } from 'ionic-angular';
import { Tech_Slots, Sal_Techs, Tech_appointments, Services, SalonServices, Salon, OFFERS, SALON_PROMOTION } from '../../providers/SalonAppUser-Interface';

import { Content } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfirmBookingPage } from '../../pages/confirm-booking/confirm-booking';
import { retry } from 'rxjs/operators/retry';
import { concat } from 'rxjs/operator/concat';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { MainHomePage } from '../main-home/main-home';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import {  ViewController } from 'ionic-angular';
import { Customer } from '../../providers/SalonAppUser-Interface';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Keyboard } from '@ionic-native/keyboard';

@IonicPage()
@Component({
  selector: 'page-slots-view',
  templateUrl: 'slots-view.html',
})
export class SlotsViewPage {
  @ViewChild(Content) content: Content;
  valueforngif = true;

  @ViewChild(Navbar) navBar: Navbar;
  public appointmentTotalTime = 0
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  // params: any
  public unregisterBackButtonAction: any;
  allTechs: any;
  public calendarOption;
  selectedDateStr = "";
  existingAppo: Tech_appointments;
  dateToGetSlots: Date = new Date();
  // array = []
  techSlots: Tech_Slots[] = [];
  selectedSlot: Tech_Slots
  cust_id: string
  colorDay: Date;
  coloredDays = []
  objSubStyle: any
  objPin: any
  pinImage: any
  subStyleServicesObject: any
  public selectedTech: Sal_Techs;
  public selectedServicesObjects: SalonServices[]
  public selectedSalon: Salon
  public salonForComponent: Salon
  public rescheduleAppointment: Tech_appointments
  public salonOffers: SALON_PROMOTION[] = []
  public selectedPromotion : SALON_PROMOTION
  public salonOfferTitle = ''

  //customer information section
  customerProfileExist=false
  showCustomerProfile=false
  myCustomer: Customer
  firstname = '';
  email = '';
  PhoneNumber: '';
  responseJson: any[];
  response: string;
  location = '';
  deviceType: string;
  errorMessageName = '';
  errorMessageEmail = '';
  errorMessageLocation = '';
  isValidName = true;
  isValidEmail = true;
  isValidLocation = true;
  isValidLatLan=false;
  isGenderMale = true;
  customerGender: string
  postCode: string;
  longitude: any;
  latitude: any;
  emailExpression = '^[\w\.]+@([\w]+\.)+[A-Z]{2,7}$';
  //end customer information section
  constructor(
    public keyBoard: Keyboard,

    public navCtrl: NavController,
    public storage: Storage,
    public platform: Platform,
    public loader: LoadingController,
    public serviceManager: GlobalProvider,
    public salonServicesModal: SalonServicesModal,
    private toastCtrl: ToastController,
    private salonServiceModal: SalonServicesModal,

    
    private loadingController: LoadingController,
    

    private menu: MenuController,
     private navParams:NavParams,
    private viewCtrl: ViewController,
    public customerModal: CustomerModal,
    private nativePageTransitions: NativePageTransitions,
    public events: Events,
   ) {

    this.selectedTech = this.navParams.get('selectedTech');
    this.salonForComponent = this.navParams.get('salonForComponent');
    this.selectedServicesObjects = this.navParams.get('selectedServicesObjects')
    this.selectedSalon = this.navParams.get('selectedSalon')
    this.rescheduleAppointment = this.navParams.get('rescheduleAppointment')
    let offersObjects: OFFERS[] = this.navParams.get('offersObjects')

  

    this.calendarOption = {
      fontSize: 8,
      fontWeight: 600,
      numberOfDays: this.selectedSalon['sal_future_app_days'],
      calendarBackgroundColor: 'white',
      hieghtOfCalendar: '86pt',
      defaultColor: 'white',

      selectedDateColor: 'black',
      seletedDayColor: 'black',
      selectedMonthAndYearColor: 'white',
      unSelectedDateColor: 'rgb(150,150,150)',
      circleBorderColor: '1px solid #969696',
      circleBackgroundColor: 'white',
      sizeOfCircle: 18.7,

      selectedBackgroundColor: 'rgb(255,147,111)',
      selectedColor: 'white',
      selectedBorderColor: 'none',
      pomotionDaysColor: 'rgb(255,95,109)',
      offersObjects: offersObjects,


    };




    if (this.existingAppo != undefined) {
      let date = this.existingAppo.app_start_time.split(' ')[0];
      console.log('done from here...1');
      date = this.serviceManager.getStringFromDateWithFormat(new Date(date), 'MM/dd');
      console.log('done from here...2');
      this.coloredDays = [{
        d: date,
        background: 'red'
      }];
      console.log('done from here...3');
    }

    let $comp = this
    this.selectedDateStr = this.serviceManager.getStringFromDateWithFormat(new Date(), 'dd MMM');
    this.storage.get(this.serviceManager.CUSTOMER_DATA).then((value) => {
      console.log('LoginStatus', value);
      this.cust_id = value.cust_id;
      value ? true : false
    }).catch((Error) => {
      console.log('Error', 'in login');
    });

    let now = new Date();
    if (this.existingAppo != undefined) {
      let tempDate = this.existingAppo.app_start_time;
      tempDate = tempDate.replace(/-/g, '/');
      this.dateToGetSlots = new Date(tempDate);

    }

    this.techSlots = []

    if (this.existingAppo != undefined) {
      
      let tempDate = this.existingAppo.app_start_time;
      tempDate = tempDate.replace(/-/g, '/');
      this.calendarOption['appoDate'] = new Date(tempDate);
    }
    /** 
    else if (this.selectedSalon['sal_temp_enabled'] == "1") {
      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      this.calendarOption['tomorrow'] = tomorrow;
      this.dateToGetSlots = tomorrow;
      this.calendarOption['appoDate'] = "N/A";
    }*/
    else {
      
      this.calendarOption['appoDate'] = "N/A";
    }
    this.subStyleServicesObject = []
  }

  scrollHandler(event) {
    if(!event) return 
    let elem: HTMLElement = this.content._scrollContent.nativeElement
    if (event.directionY && event.directionY === 'down') {
      if (event.scrollTop > 300){
        this.keyBoard.close()
      }
    } else {
    }
  }
  handleReturnkey() {
    this.keyBoard.close()
  }
  onPageScroll(event) {
    console.log(event.target.scrollTop);
}
  ionViewDidEnter() {
    this.keyBoard.onKeyboardShow().subscribe(() => { this.valueforngif = false })
    this.keyBoard.onKeyboardHide().subscribe(() => { this.valueforngif = true })
    this.initializeBackButtonCustomHandler();
    this.salonOffers = []
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getSalonOffers(this.selectedSalon.sal_id).then(offers => {
          this.salonOffers = offers
          let selectedDayNumber = this.dateToGetSlots.getDay()
          if (selectedDayNumber === 0) {
            selectedDayNumber = 7
          }
          // console.log(JSON.stringify(this.salonOffers));
          this.salonOffers.forEach(salonOffer => {
            if (salonOffer.so_days.includes(selectedDayNumber.toString())) {
              this.salonOfferTitle = salonOffer.so_title
            }
          });
        }, error => {
        })
      }
    })
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
    // this.navCtrl.pop();
  }
  //end custom back button 
  ionViewWillEnter() {
    this.showCustomerProfile=false
    this.customerProfileExist=false
    
    this.getTechSlots();
    this.getCustomer();
  }

  ionViewDidLoad() {
    this.selectedServicesObjects.forEach(objService => {
      this.appointmentTotalTime += Number(objService.sser_time)
    });
    // let objSubStyle = this.serviceManager.getFromLocalStorage(this.serviceManager.OBJ_SUB_STYLE)
    let objPin = this.serviceManager.getFromLocalStorage(this.serviceManager.OBJ_PIN)
    if (objPin) {
      this.pinImage = objPin.imageUrl
      this.objPin = objPin
    }
    if (this.techSlots == undefined) {
      return
    }
    this.techSlots.forEach(element => {
      let slot: Tech_Slots = element
      if (slot == undefined) {
        console.log('slots is undefined');
        return
      }
      console.log('slots Object', slot);
    });


    // this.navBar.backButtonClick = (e: UIEvent) => {

    //   let options: NativeTransitionOptions = {
    //     direction: 'right',
    //     duration: 500,
    //     slowdownfactor: 3,
    //     slidePixels: 20,
    //     iosdelay: 100,
    //     androiddelay: 150,
    //     fixedPixelsTop: 0,
    //     fixedPixelsBottom: 0
    //   };
    //   if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    //   this.navCtrl.pop({
    //     animate: false,
    //     animation: 'ios-transition',
    //     direction: 'back',
    //     duration: 500,
    //   })
    // }

  }
  getTechSlots() {
    let loading = this.loader.create({ content: "Fetching Slots, Please Wait..." });
    loading.present();
  
    var params = {
      service: btoa('get_tech_slots'),
      tech_id: btoa(this.selectedTech.tech_id.toString()),
      app_duration: btoa(this.appointmentTotalTime.toString()),
      app_date: btoa(this.serviceManager.getStringFromDateWithFormat(this.dateToGetSlots, 'yyyyMMdd')),
      sal_app_after_hours: btoa(this.salonForComponent.sal_app_after_hours.toString()),
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
    }
     console.log('ParamSLots',JSON.stringify(params))
    this.selectedDateStr = this.serviceManager.getStringFromDateWithFormat(this.dateToGetSlots, 'dd MMM');
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {
        console.log('res',JSON.stringify(res))
        // this.sortedSlots = [];
        this.techSlots = [];
        loading.dismissAll();

        this.techSlots = res['tech_slots'];

        this.techSlots.forEach(slot => {
          if (this.selectedSlot && this.selectedSlot.ts_number === slot.ts_number) {
            return slot.isSelected = true
          } else {
            return slot.isSelected = false
          }

        });
        if (this.techSlots) {

          // let length = 0;
          // for (var key in this.techSlots) {
          //   if (this.techSlots.hasOwnProperty(key)) {
          //     length++;
          //   }
          //   let obj = this.techSlots[length];
          //   if (obj != undefined && obj.ts_start_time != undefined) {
          //     this.array.push(obj)
          //   }
          //   // this.sortedSlots = this.array.sort((obj1: Tech_Slots, obj2: Tech_Slots) => {
          //   // if (obj1.ts_number > obj2.ts_number) { return 1 }
          //   // if (obj1.ts_id < obj2.ts_id) { return -1 }
          //   // return 0;
          //   // });
          // }
          // if (this.selectedSlot) {

          //   console.log(JSON.stringify(this.selectedSlot));

          //   this.slotDidSelected(this.selectedSlot, '')
          // }
          if (this.existingAppo != undefined) {
            let firstSlot = this.existingAppo.app_slots.split(',')[0];
            for (let slot = 0; slot < this.techSlots.length; slot++) {

              console.log('First Slot => ' + firstSlot);
              console.log(' Slots => ' + this.existingAppo.app_slots);
              console.log('All Slot => ' + this.techSlots[slot]['ts_number']);

              if (this.techSlots[slot]['ts_number'] == firstSlot) {
                // this.selectedSlot = slot;

                console.log('got the slot ... ' + this.techSlots[slot]['ts_number']);
                break;
              }
            }
          }

        } else {
          this.techSlots = []
        }


        // this.techSlots.forEach(myTech => {
        //   return myTech.isSelected = false
        // });
        // this.techSlots.forEach(myTech => {
        //   console.log("Value", JSON.stringify(myTech))
        // });




      },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);

        });
    loading.dismissAll();
  }

  getSlotFromattedTime(slotTime) {
    return this.serviceManager.get12HourFormatFrom24Hour(slotTime);
  }

  getSlotTimeWithoutSuffix(slotTime) {
    return this.serviceManager.get12HourFormatFrom24HourWithoutSuffix(slotTime);
  }

  getSlotAmPm(slotTime) {
    return this.serviceManager.get12HourFormatSuffix(slotTime);
  }


  slotDidSelected(objSlot, slot) {
    // alert('here')
    this.techSlots.map(slot => {
      slot === objSlot ? slot.isSelected = !slot.isSelected : slot.isSelected = false
    })
    objSlot.isSelected ? this.selectedSlot = objSlot : this.selectedSlot = null

  }

  btnBookAppointmentTapped() {

    if (!this.selectedSlot || this.selectedSlot === null) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgTimeSlotSelection)
      return;
    }

    if(!this.customerProfileExist){
      this.showCustomerProfile=true
      
    }else {
      //customer exist tap to next screen 
         this.goToConfirmBooking();
    }
    
  }
  goToConfirmBooking(){
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ConfirmBookingPage, {
      selectedSalon: this.selectedSalon,
      salonForComponent: this.salonForComponent,
      selectedServicesObjects: this.selectedServicesObjects,
      selectedTech: this.selectedTech,
      selectedSlot: this.selectedSlot,
      rescheduleAppointment: this.rescheduleAppointment,
      selectedPromotion: this.selectedPromotion
    });
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
  }
  dateClicked(date) {
    this.dateToGetSlots = date;
    this.selectedSlot = null
    this.getTechSlots();
    this.salonOfferTitle = null
    let selectedDayNumber = this.dateToGetSlots.getDay()
    if (selectedDayNumber === 0) {
      selectedDayNumber = 7
    }
    // console.log(JSON.stringify(this.salonOffers));
    this.salonOffers.forEach(salonOffer => {
      if (salonOffer.so_days.includes(selectedDayNumber.toString())) {
        this.salonOfferTitle = salonOffer.so_title
        this.selectedPromotion = salonOffer
      }
    });
  }
//customer information section events

getCustomer() {
  console.log('came in get customer');
  this.customerModal.getDatabaseState().subscribe(ready => {
    if (ready) {
      this.customerModal.getCustomer().then(customer => {
        if (customer) {
          console.log('my customer FROM DB');
          console.log(JSON.stringify(customer));
          this.myCustomer = customer
         
          if(this.myCustomer.cust_name){
            if(this.myCustomer.cust_name.length>0){
              this.customerProfileExist=true;
            }
          }else{
            this.customerProfileExist=false;
          } 
        } else {
          this.customerProfileExist=false;
        }
      }, error => {
        console.log('error: while getting customer');
      })
    }
  })
}

OnValueEnterName(name) {
  this.isValidNameInput();
}
OnValueEnterEmail(email) {
  this.isValidEmailInput();
}
presentToast(message) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 5000,
    position: 'top',
    cssClass: "ToastClass",
  });
  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });
  toast.present();
}
OnValueEnterLocation(location) {
  this.isValidLocationInput();
}
OnCompleteActivation() {
  this.checkValidation();
  if (this.isValidEmail && this.isValidName) {

    // if(this.isValidLatLan){
    //   this.resgisterUser();
    // }else{
    //   this.serviceManager.makeToastOnFailure("Can't get your location, please check your internet connection!",0);

    // }
    this.resgisterUser();
  } else {
    //this.serviceManager.makeToastOnFailure("Can't get your location, please check your internet connection!"+this.latitude,0);
    console.log("Activation can't completes with errors");
  }
}
checkValidation() {

  this.isValidNameInput();
  this.isValidEmailInput();
   //  this.isValidLocationInput();
 // this.checkLocationFormat();
  this.checkEmailExpression();
}
isValidNameInput() {
  if (this.firstname.length == 0) {
    this.isValidName = false;
    this.errorMessageName = 'Please provide your name'
  } else {
    this.isValidName = true;
    this.errorMessageName = ''
  }
}
isValidEmailInput() {



  if (this.email.length == 0) {
    this.isValidEmail = false;
    this.errorMessageEmail = 'Please provide your email'
   
  } else {
    this.isValidEmail = true;
    this.errorMessageEmail = ''
    //check email pattern
  }
}
checkEmailExpression() {
  if (this.email.length > 0) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
      this.isValidEmail = true;
      this.errorMessageEmail = ''
    } else {
      this.isValidEmail = false;
      this.errorMessageEmail = 'Please provide your valid email address'
    }
  }
}
checkLocationFormat() {
  if ((this.location.length < 6)) {

    this.isValidLocation = false;
    this.errorMessageLocation = 'Please enter a valid postcode including a space.'

  } else if (!this.checkSpaces()) {
    this.isValidLocation = false;
    this.errorMessageLocation = 'Invalid postcode'
  }

}
checkSpaces() {
  let spaceFound = false;
  let spaceCOunter = 0;
  let text = this.location;
  for (let i = 0; i < text.length; i++) {
    let valueATIndex = text.charAt(i);
    let value = valueATIndex.toString();
    console.log('value', value);
    if (value == ' ') {
      console.log('spaceFound', value);
      spaceFound = true;
    } else {
      spaceFound = false;
    }
    if (spaceFound) {
      spaceCOunter = (spaceCOunter) + 1;
    }
  }
  console.log('spaceCOunter', spaceCOunter);
  if (spaceCOunter < 1 || spaceCOunter > 1) {
    return false;
  }
  return true;
}
isValidLocationInput() {
  if (this.location.length == 0) {
    this.isValidLocation = false;
    this.errorMessageLocation = 'Please provide your post code'
  } else {
    this.isValidLocation = true;
    this.errorMessageLocation = ''
    //check location pattren 
  }

}
resgisterUser() {
  let notificationController = this.loadingController.create({
    content: "Please wait..."
  });

  notificationController.present();
  let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN)

  let cust_Gender = ''
  if (this.isGenderMale) {
    cust_Gender = this.serviceManager.CUSTOMER_GENDER_MALE
  } else {
    cust_Gender = this.serviceManager.CUSTOMER_GENDER_FEMALE
  }
  
  
  
  var params = {
    cust_id: btoa(this.myCustomer.cust_id),
    service: btoa('register'),
    cust_name: btoa(this.firstname),
    // cust_zip: btoa(this.location),
    cust_lat:btoa(this.latitude),
    cust_lng:btoa(this.longitude),
    cust_phone: btoa(this.myCustomer.cust_phone),
    cust_email: btoa(this.email),
    cust_gender: btoa(cust_Gender),
  
    cust_device_id: btoa(token),// for now its 2 but its will cahnge 
    device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
    cust_device_type: btoa(this.deviceType),
  }
  console.log(params);
  
  this.serviceManager.getData(params)
    .retryWhen((err) => {
      return err.scan((retryCount) => {
        retryCount += 1;
        if (retryCount < 3) {
          return retryCount;
        }
        else {
          throw (err);
        }
      }, 0).delay(1000)
    })

    .subscribe((response) => {
      this.response = response.status;
      this.responseJson = response;
      notificationController.dismiss();
      console.log('AllResponse', this.response);
      // this.checkResgisterStatus(response);
      if (response.status === '1') {
        this.storage.set('isLogin', true);
        this.saveCustomerIntoDB(response.customer)
      }
    },
      error => {
        notificationController.dismiss();
        this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        console.log('something went wrong', error);
      }
    );
  // 
}
getPhoneFromPref() {
  return this.PhoneNumber;
}
OnValueEnter(vale) {
}
MaleGenderTapped() {
  this.isGenderMale = !this.isGenderMale
}
genderMaleTapped() {
  this.isGenderMale = true
}
genderFemaleTapped() {
  this.isGenderMale = false
}
saveCustomerIntoDB(customer:Customer) {
  if (customer) {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.createCustomerTable().then(isTableCreated => {
          if (isTableCreated === true) {
            this.customerModal.InsertInToCustomerTable(customer).then(isCustomerInserted => {
              if (isCustomerInserted === true) {
                console.log('success: InsertInToCustomerTable ');
              
                this.goToConfirmBooking();
               
                this.serviceManager.setInLocalStorage(this.serviceManager.IS_USER_LOGIN, true)
                this.storage.set('isLogin', true);
                this.events.publish('customer:changed', customer);
                //this.nativePageTransitions.slide(this.options)
               // this.btnBookAppointmentTapped();
               // this.navCtrl.setRoot(MainHomePage, { myCustomer: customer })
              } else {
                console.log('error: InsertInToCustomerTable ', isCustomerInserted);
              }
            }, error => {
              console.log('error: ', error.message);
            })
          }
        })
      }
    })
  }
}
//end customer information section events
}