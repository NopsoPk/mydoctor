import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Platform,
  Navbar
} from "ionic-angular";
import {
  Salon,
  Sal_Techs,
  Services,
  SalonServices,
  Tech_Slots,
  Customer,
  Tech_appointments,
  SALON_CARD_OPTIONS,
  SALON_PROMOTION
} from "../../providers/SalonAppUser-Interface";
import { MenuController } from "ionic-angular";
import { ThanksVcPage } from "../../pages/thanks-vc/thanks-vc";

import "rxjs/add/operator/retrywhen";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/scan";
import { GlobalProvider } from "../../providers/global/global";
import { MainHomePage } from "../main-home/main-home";
import { CustomerModal } from "../../providers/CustomerModal/CustomerModal";
import { config } from "../../providers/config/config";
import { GlobalServiceProvider } from "../../providers/global-service/global-service";
import { AppVersion } from "@ionic-native/app-version";
import { AppMessages } from "../../providers/AppMessages/AppMessages";

@IonicPage()
@Component({
  selector: "page-confirm-booking",
  templateUrl: "confirm-booking.html"
})
export class ConfirmBookingPage {
  @ViewChild(Navbar) navBar: Navbar;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };

  public days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  public unregisterBackButtonAction: any;
  public objSubStyle: any;
  public pinImage: any;
  public AppointmentDayName = "";
  public AppointmentDateForConfirmation = "";
  selectedSalon: Salon;
  public salonForComponent: Salon;
  selectedServicesObjects: SalonServices[];
  selectedTech: Sal_Techs;
  selectedSlot: Tech_Slots;
  public appointmentServices = "";
  public appointmentSer_ids = "";
  public appointmentTotalTime = 0;
  public appointmentTotalPrice = 0.0;
  public params: any;
  public myCustomer: Customer;
  public rescheduleAppointment: Tech_appointments;
  public selectedPromotion: SALON_PROMOTION;
  public currencySymbol = "";
  // servicesArray = [];
  // appo_Services: any[]
  // subStyleServicesObject: any[]
  // objAppoServices: any[]
  // objSelectedServices: Services[] = []
  objSelectedServices = [1, 2, 3, 4, 5, 6];
  height = 0;
  public salonCardOption: SALON_CARD_OPTIONS;

  constructor(
    private menu: MenuController,
    public platform: Platform,

    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public customerModal: CustomerModal,
    public gsp: GlobalServiceProvider,
    // private admobFree : AdMobFree,
    public loader: LoadingController,
    public appVersion: AppVersion
  ) {
    this.currencySymbol = this.gsp.currencySymbol;

    this.getCustomer();
    this.selectedSalon = this.navParams.get("selectedSalon");
    this.salonForComponent = this.navParams.get("salonForComponent");
    this.selectedServicesObjects = this.navParams.get(
      "selectedServicesObjects"
    );
    this.selectedTech = this.navParams.get("selectedTech");
    this.selectedSlot = this.navParams.get("selectedSlot");
    this.rescheduleAppointment = this.navParams.get("rescheduleAppointment");
    this.selectedPromotion = this.navParams.get("selectedPromotion");
    let Salons: Salon[] = [];
    let sal_rating = Number(this.salonForComponent.sal_rating);
    var _CardHeight = "100pt";
    if (sal_rating > 0) {
      _CardHeight = "80pt";
    }

    Salons.push(this.salonForComponent);
    this.salonCardOption = {
      CardHeight: _CardHeight,
      calledFrom: config.ConfirmBookingPage,
      ShouldShowSalonImage: false,
      shouldShowQualitifcation: true,
      shouldScroll: false,
      salon: Salons
    };

    this.params = {};

    // let response = this.serviceManager.getFromLocalStorage('_salonDetail')
    // this.selectedSalon = response["salons"][0];

    // this.params = this.navParams.get("params");
    // this.servicesArray = atob(this.params["app_services"]).split(',');
    // this.height = this.servicesArray.length * 40;
    // this.selectedSalon = this.navParams.get("selectedSalon");
    // this.selectedTech = this.navParams.get("selectedTech");
    // this.appo_Services = this.navParams.get("appo_Services");
    // this.subStyleServicesObject = []
    // this.objAppoServices = []
    // this.appo_Services.forEach(ser => {
    //   let service: Services = ser

    //   if (service.ssubc_id) {
    //     this.subStyleServicesObject.push(service)
    //   } else {
    //     this.objAppoServices.push(service)
    //   }
    // });
  }

  //custom back button
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    this.getAppVersion();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.customHandleBackButton();
      },
      10
    );
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage);
      }
    }

    // this.navCtrl.pop();
  }
  //end custom back button

  ionViewDidLoad() {
    this.selectedServicesObjects.forEach(objService => {
      this.appointmentServices += objService.sser_name + ",";
      this.appointmentSer_ids += objService.sser_id + ",";
      this.appointmentTotalTime += Number(objService.sser_time);
      this.appointmentTotalPrice += Number(objService.sser_rate);
    });

    this.appointmentServices = this.appointmentServices.slice(
      0,
      this.appointmentServices.length - 1
    );
    this.appointmentSer_ids = this.appointmentSer_ids.slice(
      0,
      this.appointmentSer_ids.length - 1
    );

    var dateObject = new Date(this.selectedSlot.ts_date);
    this.AppointmentDayName = this.days[dateObject.getDay()];
    this.AppointmentDateForConfirmation = this.serviceManager.getDateFormattedForAppointmentConfirmation(
      this.selectedSlot.ts_date
    );
  }

  getBookingDate() {
    let tempDate = atob(this.params["app_start_time"]);
    let splitted = new Date(tempDate.split(" ")[0]);
    return this.serviceManager.getStringFromDateWithFormat(splitted, "EEE dd, MMM");
  }

  getBookingTime() {
    let tempDate = atob(this.params["app_start_time"]);
    let splitted = tempDate.split(" ")[1];
    return this.serviceManager.get12HourFormatFrom24Hour(splitted);
  }
  getTotalPrice() {
    return atob(this.params["app_price"]);
  }
  getTotalTime() {
    return atob(this.params["app_est_duration"]);
  }
  btnBookAppointmentTapped() {
    if (!this.myCustomer) {
      console.log("Customer ID is null");

      return;
    }
    let start_time =
      this.serviceManager.getStringFromDateWithFormat(
        this.selectedSlot.ts_date,
        "yyyy-MM-dd"
      ) +
      " " +
      this.selectedSlot.ts_start_time +
      ":00";

    this.params = {
      service: btoa("make_appointment"),
      app_type: btoa("appointment"), // this string is as it is. not know whats benefit of it
      sal_id: btoa(this.selectedSalon.sal_id),
      cust_id: btoa(this.myCustomer.cust_id.toString()),
      tech_id: btoa(this.selectedTech.tech_id.toString()),
      app_id: btoa(""),
      app_services: btoa(this.appointmentServices),
      app_service_ids: btoa(this.appointmentSer_ids),
      app_price: btoa(this.appointmentTotalPrice.toString()),
      app_est_duration: btoa(this.appointmentTotalTime.toString()),
      app_start_time: btoa(start_time),
      app_slots: btoa(this.selectedSlot.ts_number),
      app_user_info: btoa(this.appInfo), //version info build,api,version etc
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime())
    };
    if (this.selectedSalon.sal_auto_accept_app == 0) {
      this.params["app_status"] = btoa(this.serviceManager.APP_STATUS_PENDING);
    } else if (this.selectedSalon.sal_auto_accept_app == 1) {
      this.params["app_status"] = btoa(this.serviceManager.APP_STATUS_ACCEPTED);
    }
    if (this.selectedPromotion) {
      this.params["so_id"] = btoa(this.selectedPromotion.so_id.toString());
    }
    if (this.rescheduleAppointment) {
      this.params["service"] = btoa("update_appointment");
      this.params["app_id"] = btoa(this.rescheduleAppointment.app_id);
      console.log("Updating...");
    }

    let loading = this.loader.create({
      content: "Booking Appointment, Please Wait..."
    });
    loading.present();
    this.serviceManager
      .getData(this.params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })

      .subscribe(
        res => {
          console.log(JSON.stringify(res));

          loading.dismissAll();
          if (Number(res.status) === 1) {
            if (Number(this.selectedSalon.sal_temp_enabled) === 1) {
              this.serviceManager.makeToastOnSuccess(
                AppMessages.msgOnRequestAppointment
              );
            }
            this.nativePageTransitions.slide(this.options);
            (this.gsp.sty_id = this.salonForComponent.sty_id),
              this.navCtrl
                .push(ThanksVcPage, {
                  sal_name: this.salonForComponent.sal_name,
                  sal_typeID: this.salonForComponent.sty_id
                })
                .then(done => {
                  if (this.selectedSalon.sal_temp_enabled === "1") {
                    this.serviceManager.makeToastOnSuccess(
                      AppMessages.msgOnRequestAppointment
                    );
                  }
                });
          } else {
            this.serviceManager.makeToastOnFailure(res.msg);
          }
        },
        error => {
          loading.dismissAll();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
    console.log(this.params);
  }

  // watchAd(){
  //   const bannerConfig: AdMobFreeRewardVideoConfig = {
  //     // id: 'ca-app-pub-5015443288210932/1448822399',
  //     id:'ca-app-pub-3940256099942544/1712485313',
  //     isTesting: false,
  //     autoShow: true
  //     };
  //     this.admobFree.rewardVideo.config(bannerConfig);

  //     this.admobFree.rewardVideo.prepare()
  //     .then(() => {
  //         // this.admobFree.rewardVideo.show()
  //     })
  //     .catch(e => console.log(e));
  // }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options);

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage);
    }
  }
  getSlotFromattedTime(slotTime) {
    return this.serviceManager.get12HourFormatFrom24Hour(slotTime);
  }
  getCustomer() {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            this.myCustomer = customer;
          },
          error => {}
        );
      }
    });
  }

  appInfo = "";
  getAppVersion() {
    this.appVersion.getAppName().then(appName => {
      this.appInfo += "app_Name: " + appName + ",\n";
      this.appVersion.getPackageName().then(packageName => {
        this.appInfo += "package_Name: " + packageName + ",\n";
        this.appVersion.getVersionCode().then(versionCode => {
          this.appInfo += "version_Code: " + versionCode + ",\n";
          this.appVersion.getVersionNumber().then(versionNumber => {
            this.appInfo += "versionNumber: " + versionNumber + ",\n";
          });
        });
      });
    });
  }
  roundAmount(amount: number | string) {
    return Math.round(Number(amount));
  }
}
