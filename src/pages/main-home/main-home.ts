// import { FavoriteSalonsPage } from './../favorite-salons/favorite-salons';
import { HomeModal } from './../../providers/homeModal/homeModal';
import { SalonServicesModal } from './../../providers/salon-services-modal/salon-services-modal';
// import { FashionBrandModal } from './../../providers/FashionBrandModal/FashionBrandModal';
import { SALON_SERVICES_SUB_CATEGORIES, SalonServices, Salon, SALON_SERVICES_SEARCH, SAL_SER_SUB_CATEGORIES, Customer, ServiceSubCATEGORIES, SERVICE_CATEGORY, OFFERS, SalonHour, APP_CONFIG, FASHION_BRANDS, FASHION_BRANDS_COLLECTION, FASHION_COLLECTION } from './../../providers/SalonAppUser-Interface';
import { SQLite } from '@ionic-native/sqlite';
import { config } from './../../providers/config/config';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ViewChild, Renderer, ElementRef } from '@angular/core';
import { Content, ModalController } from 'ionic-angular';

import { AppointmentsCardPage } from '../appointments-card/appointments-card';
// import { AppCenterCrashes } from '@ionic-native/app-center-crashes';
import { Homepage } from '../../providers/SalonAppUser-Interface';
import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams, ViewController, LoadingController, Events, App } from 'ionic-angular';
import { Platform } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs'
import { StatusBar } from '@ionic-native/status-bar';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
// import { LatestTrendsPage } from '../latest-trends/latest-trends'
// import { LatestTrendsListingPage } from '../latest-trends-listing/latest-trends-listing'
// import { LatestTrendsArchiveCollectionPage } from '../latest-trends-archive-collection/latest-trends-archive-collection'
// import { LatestTrendsDetailPage } from '../latest-trends-detail/latest-trends-detail'
import { BeautyTipsViewPage } from '../beauty-tips-view/beauty-tips-view'

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { timeout } from 'rxjs/operator/timeout';
// import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { GoogleAnalytics } from '../../../node_modules/@ionic-native/google-analytics';
import { reorderArray } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ConfigModal } from '../../providers/ConfigModal/ConfigModal';
import { BeautyTipsModal } from '../../providers/BeautyTipsModal/BeautyTipsModal';
import { TipsDetailModal } from '../../providers/TipsDetailModal/TipsDetailModal';

import { DatePipe } from '@angular/common';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { AlertController } from 'ionic-angular';
import { timer } from 'rxjs/observable/timer';

import { Diagnostic } from '@ionic-native/diagnostic';
// import { PromotionModal } from '../../providers/PromotionModal/PromotionModal';
import { AppInfoPage } from '../app-info/app-info';
import * as $ from "jquery";
import { ForceUpdatePage } from '../force-update/force-update';
import { AppVersion } from '@ionic-native/app-version';
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-main-home',
  templateUrl: 'main-home.html',
})
export class MainHomePage {
  @ViewChild(Content) content: Content;
  public isFirstLaunch = true
  public alertShown: boolean = false;
  isUserOutOfRange = false
  public ServiceSubCATEGORIE: ServiceSubCATEGORIES[]
  public serviceCategories: SERVICE_CATEGORY[]
  // public SALONS: Salon[]
  public ALLOFFERS: OFFERS[]
  public SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[]
  public SAL_SER_SUB_CATEGORIE: SAL_SER_SUB_CATEGORIES[]
  public isenabled = true;
  public loading: any
  //temp code

  expenses: any = [];
  totalIncome: number = 0;
  totalExpense: number = 0;
  balance: number = 0;
  public strAllSalonIDs = ''
  public myLastFetchDate = ""
  public homeImageUrl = config.salonImgUrl//"https://nopso.qwpcorp.com/salon/salonimages/";
  public noSalonFound = false;
  public allSalon: Salon[] = [];
  public SalonHoursAll: SalonHour[]
  public salonIds = []
  deviceType: string;
  TabsPage: any;
  progressShowing: any;
  servicesInQueue: Number[] = [];
  resultSet: String[] = [];
  nextPage: any;
  sectionParameters: any;
  public SalonsAssociatedWithPins: SALON_SERVICES_SUB_CATEGORIES[]
  public SalonsServices: SalonServices[]
  public unregisterBackButtonAction: any;
  public salonLastFetchDate
  tabBarElement: any;
  locationRequestCounter = 0
  // custId: any;
  salonIdAll: any
  public myCustomer: Customer
  public LAST_UPDATE_DATE_TIME = ""
  public AppDataStatus = null
  public isFirstTime = false
  // public homePageStatsArray:any
  public homePageStatsArray = Array()
  public _isForceUpdateRequired = false
  public hideHompePage = false
  public isPushToNextPage = true
  public customSplashHtml: any
  public customerExist = true
  public sp_view_strip: any
  public sp_background: any
  public splashBackground = 'https://www.beautyapp.pk/images/gif_congrats.gif'
  Configcount: any;
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  homepageData: Homepage[] = [];
  constructor(
    public elementRef: ElementRef,
    public renderer: Renderer,
    public appCtrl: App,
    public alertCtrl: AlertController,
    public customerModal: CustomerModal,
    // public favModel: FavoriteModal,
    public events: Events,
    public sqlProvider: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    // public fashionBrandModal: FashionBrandModal,
    public configModal: ConfigModal,
    public beautyTipsModal: BeautyTipsModal,
    public tipsDetailModal: TipsDetailModal,
    
    private sqlite: SQLite,
    private loadingController: LoadingController,
    public statusBar: StatusBar,
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private platForm: Platform,
    public serviceManager: GlobalProvider,
    public globalService: GlobalServiceProvider,
    public homeModal: HomeModal,
    public http: Http,
    public storage: Storage,
    private nativePageTransitions: NativePageTransitions,
    public ga: GoogleAnalytics,
    public geolocation: Geolocation,
    public datePipe: DatePipe,
    // public androidPermissions: AndroidPermissions,
    public diagnostic: Diagnostic,
    // public promotionModal: PromotionModal,
    public modalCtrl: ModalController,
    public appVersion: AppVersion,

  ) {
    this.menu.swipeEnable(false);
    this.ga.startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
      })
      .catch(e => {
      });
    if (this.platForm.is('android')) {
      this.statusBar.styleLightContent();
    } else {
      this.statusBar.styleDefault();
    }
    this.initializeBackButtonCustomHandler();
    let homePageStats = {
      Id: 0,
      total_salons: "1000",
      toal_fashion: "10000",
      toal_beauty_trends: "10000",
      toal_beauty_tips: "1000",
      total_product: "10000"
    }
    this.getHomePageStatic(homePageStats);
  }
  //app life cycle events.
  ionViewCanEnter() {
    // this.getHomePageStatic(homePageStats);
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer

            this.serviceManager.setInLocalStorage('zcustomer', this.myCustomer)
            this.geolocation.getCurrentPosition().then(location => {
              let lat = location.coords.latitude
              let lng = location.coords.longitude
              let distanceDifference = this.getDistanceDifference(this.myCustomer.cust_lat, this.myCustomer.cust_lng, lat, lng, 'K')
              if (distanceDifference > config.distance && distanceDifference < 1000) {
                this.myLastFetchDate = ''
                this.serviceManager.removeFromStorageForTheKey(this.serviceManager.LAST_UPDATE_DATE_TIME)
                localStorage.removeItem(this.serviceManager.LAST_UPDATE_DATE_TIME)
                this.LAST_UPDATE_DATE_TIME = ''
                this.isUserOutOfRange = true
                this.ionViewWillEnter()
              }
            }).catch(e => {
              console.log('error: while getting locaiton');
            })
            if (this.myCustomer.cust_lat.trim().length === 0 || this.myCustomer.cust_lng.trim().length === 0) {

              this.updatelocation()
            }
            // this.checkCustomerData()
          } else {
            //this.navCtrl.setRoot(ActivateSalonPage)//commented by riz on dec 21
          }
        }, error => {
        })
      }
    })
    this.events.publish('getCustomer')
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.getAllSalonIDs().then(strSalonIDs => {
          this.strAllSalonIDs = strSalonIDs
        })
      }
    })
  }
  close() {
    this.hideHompePage = false
  }
  ionViewWillEnter() {
    this.isPushToNextPage = true
    this.htmlOnClick()
    let customSpashStatus = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_SPLAH_SHOWN)
    if (!customSpashStatus) {
      this.getCustomSplash();
    } else {
      // this.getCustomSplash();
    }

    this.hideHompePage = false
    this._isForceUpdateRequired = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_FORCE_UPDATE_AVAILABLE)
    this.createAllDatabaseTables();
    this.deleteTableOnUpdateLocation();
    this.AppDataStatus = this.serviceManager.getFromLocalStorage(this.serviceManager.IS_APP_DATA)
    this.requestToServerForAppData();

  }
  ionViewDidEnter() {
    this.homeModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.homeModal.createHomePageStatsTable().then(istblHomePageSectionsCreated => {
          if (istblHomePageSectionsCreated) {
            this.getHomePageCounts();
          }
        }, error => {
        })
      }
    })

    this.content.scrollTo(0, 0).then(res => {
      this.content.resize()
      this.content.scrollTo(0, 0, 900).then(res => {

      })
    })


    this.selectedEnviroment = this.serviceManager.getFromLocalStorage('zahidzahidzahid')
    if (!this.selectedEnviroment) this.selectedEnviroment = 1

    let HideTabe = this.navParams.get('isHideTab');
    if (HideTabe !== null && HideTabe == '1') {
      //  this.navCtrl.pop();
      if (this.tabBarElement) {
        this.tabBarElement.style.display = 'none';
      }
    }
    this.getAndSaveAppConfig()

    this.checkShouldCallForceUpdate()
  }

  checkShouldCallForceUpdate() {
    this.platForm.ready().then(() => {
      this.platForm.is('android') ? this.appPlatform = 'android' : this.appPlatform = 'ios'


      let LastForceUpdateTime = this.serviceManager.getFromLocalStorage('lastForceUpdateFetchTime')

      if (!this.LAST_UPDATE_DATE_TIME || !LastForceUpdateTime || LastForceUpdateTime.length === 0 || this.LAST_UPDATE_DATE_TIME.length === 0) {

        timer(4000).subscribe(() => {
          this.platForm.ready().then(() => {
            this.getAppVersionAndcheckForceUpdate()
          })
        })
      } else {

        let _LAST_UPDATE_DATE_TIME = this.LAST_UPDATE_DATE_TIME.replace(/-/g, '/')
        let objCurrentdate = new Date(_LAST_UPDATE_DATE_TIME);

        let firstDateString = this.datePipe.transform(objCurrentdate, 'yyyy/MM/dd HH:mm:ss');


        let latestDateFinal = new Date(firstDateString);
        LastForceUpdateTime = LastForceUpdateTime.replace(/-/g, '/')
        let objPreviousDate = new Date(LastForceUpdateTime);

        let secondDateString = this.datePipe.transform(objPreviousDate, 'yyyy/MM/dd HH:mm:ss');


        let oldDate = new Date(secondDateString);

        let foreceUpdateCheckInterval = Math.abs(latestDateFinal.getTime() - oldDate.getTime());
        foreceUpdateCheckInterval = foreceUpdateCheckInterval / 1440000
        if (this._isForceUpdateRequired || foreceUpdateCheckInterval >= 24) { // hours
          timer(4000).subscribe(() => {
            this.platForm.ready().then(() => {
              this.getAppVersionAndcheckForceUpdate()
            })
          })
        }
      }
    })
    //
  }
  ionViewWillLeave() {

    let HideTabe = this.navParams.get('isHideTab');
    if (HideTabe !== null && HideTabe == '1') {
      // this.navCtrl.pop();

      if (this.tabBarElement) {
        this.tabBarElement.style.display = 'flex';
      }

    }
  }
  checkUpdateInServices(isShowProgress, LAST_UPDATE_DATE_TIME, cust: any) {
    this.loading = this.loadingController.create({ content: "Fetching HomePage, Please Wait..." });
    if (!this.AppDataStatus) {
      isShowProgress = true;
      this.progressShowing = true;
    }
    let sal_id = ''
    if (this.strAllSalonIDs) {
      sal_id = this.strAllSalonIDs
    }
    let cust_lat = null
    let cust_lng = null
    let cust_id = null
    let cust_zip = null
    if (cust !== undefined && cust !== null) {
      cust_lat = cust.cust_lat
      cust_lng = cust.cust_lng
      cust_id = cust.cust_id
      cust_zip = cust.cust_zip
    }
    var params = {
      //lst_salons params
      service: btoa('local_data'),
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      cust_id: btoa(cust_id),
      sal_ids: btoa(sal_id),
      sal_last_fetched: btoa(this.LAST_UPDATE_DATE_TIME),
      platform: btoa('ios'),
      app_version: btoa('5.0'),
      cust_zip: btoa(cust_zip),
      cust_lat: btoa(cust_lat),
      cust_lng: btoa(cust_lng),
      //lst_salons params
      last_fetched: btoa(this.LAST_UPDATE_DATE_TIME),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        console.log('date:', this.LAST_UPDATE_DATE_TIME)
        console.log('responseLocalData', JSON.stringify(response))
        this.serviceManager.setInLocalStorage(this.serviceManager.IS_APP_DATA, 'true')

        if (isShowProgress) {
          // this.loading.dismissAll();
        }
        if (response !== undefined && response !== null) {
          this.Response_Salons_HomePage_SalServiceSabCat(response, response.lds_ids);
        }


        if (this.isUserOutOfRange) {
          this.serviceManager.setInLocalStorage(this.serviceManager.LAST_UPDATE_DATE_TIME, response.response_datetime)
          this.isUserOutOfRange = false
        }
      },
        error => {
          this.serviceManager.setInLocalStorage(this.serviceManager.IS_APP_DATA, 'true')
          if (isShowProgress) {
            // this.loading.dismissAll();
          }
          //this.loading.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        }
      );
  }
  getBackGroundData(lds_ids, sal_ids) {
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('bg_local_data'),
      lds_ids: btoa(lds_ids),
      sal_ids: btoa(sal_ids),
      last_fetched: btoa(this.LAST_UPDATE_DATE_TIME),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        this.sqlProvider.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.sqlProvider.InsertInTblSearchByIndex(response.sal_ser_sub_categories);
            this.insertSalServiceSub(response.sal_ser_sub_categories, sal_ids);

          }
        })


        this.serviceManager.setInLocalStorage(this.serviceManager.LAST_UPDATE_DATE_TIME, response.response_datetime)
      },
        error => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        });
  }
  Response_Salons_HomePage_SalServiceSabCat(response, lds_ids) {
    if (this.LAST_UPDATE_DATE_TIME.length == 0) {//this.LAST_UPDATE_DATE_TIME.length == 0
      this.getBackGroundData('2,4', response.active_sal_ids);
      this.myLastFetchDate = response.response_datetime
      this.SaveHomePageDataIntoDB(response.sections)

      this.handel_lstSalonsService_Response(response)

      this.handle_serviceCategories_Response(response.data.ServiceCategories, response.data.inactive_ser_sub_categoires, response.data.inactive_ser_categoires);
      // this.handle_FashionBrands_Response(response.data.FashionBrands, response.data.inactive_fash_brand_collections, response.data.inactive_fash_brands)
      // this.handle_FashionCollection_Response(response.FashCollections)
    } else {
      let resultSet = [];
      if (lds_ids.length > 0) {
        let tempServices = lds_ids.split(",");
        tempServices.forEach(value => {
          resultSet.push(String(value))
        });
      }
      if (resultSet.indexOf('1') !== -1) {
        this.SaveHomePageDataIntoDB(response.sections)
      }
      if (resultSet.indexOf('2') !== -1) {
        //2_salonServices
        this.handel_lstSalonsService_Response(response)
      }
      //['5','6']
      if (resultSet.indexOf('3') !== -1) {
        //_service_cat
        this.handle_serviceCategories_Response(response.data.ServiceCategories, response.data.inactive_ser_sub_categoires, response.data.inactive_ser_categoires);
      }



      if (resultSet.indexOf('4') !== -1 && resultSet.indexOf('2') !== -1) {
        //4,2_bg_data
        this.getBackGroundData('2,4', response.active_sal_ids);
      }
      if (resultSet.indexOf('4') !== -1 && resultSet.indexOf('2') === -1) {
        //4_bg_data
        this.getBackGroundData('4', response.active_sal_ids);
      }
      if (resultSet.indexOf('4') === -1 && resultSet.indexOf('2') !== -1) {
        //2_bg_data
        this.getBackGroundData('2', response.active_sal_ids);
      }

      //for handle fashion brands and collections 
      if (resultSet.indexOf('5') !== -1) {

        // this.handle_FashionCollection_Response(response.FashCollections)
      }
      if (resultSet.indexOf('6') !== -1) {

        // this.handle_FashionBrands_Response(response.data.FashionBrands, response.data.inactive_fash_brand_collections, response.data.inactive_fash_brands)
      }
    }



  }
  // handle_FashionCollection_Response(FashCollections) {

  //   if (FashCollections && FashCollections != undefined && FashCollections.length > 0) {
  //     let FASHIONBRANDSCOLLECTIONARRAY: FASHION_COLLECTION[] = []
  //     FASHIONBRANDSCOLLECTIONARRAY = FashCollections

  //     this.fashionBrandModal.getDatabaseState().subscribe(ready => {
  //       if (ready) {
  //         this.fashionBrandModal.saveIntoFashCollectionTable(FASHIONBRANDSCOLLECTIONARRAY).then(categoryInsertedSuccessfully => {
  //         })
  //       }
  //     })
  //   }

  // }
  // handle_FashionBrands_Response(FashionBrands, inactive_fash_brand_collections, inactive_fash_brands) {



  //   if (FashionBrands && FashionBrands != undefined && FashionBrands.length > 0) {
  //     if (FashionBrands.length > 0) {
  //       let fashionBrands: FASHION_BRANDS[] = []
  //       fashionBrands = FashionBrands
  //       let FASHIONBRANDSCOLLECTIONARRAY: FASHION_BRANDS_COLLECTION[] = []
  //       fashionBrands.forEach(fashionBrands => {
  //         FASHIONBRANDSCOLLECTIONARRAY = FASHIONBRANDSCOLLECTIONARRAY.concat(fashionBrands.FashionBrandCollections)
  //       });
  //       this.sqlProvider.getDatabaseState().subscribe(ready => {
  //         if (ready) {
  //           if (inactive_fash_brand_collections !== undefined && inactive_fash_brand_collections !== null) {
  //             this.sqlProvider.deleteSearchKeyWordsBySerCatId(inactive_fash_brand_collections, 'key_word').then(isDataInserted => {
  //             })
  //           }
  //         }
  //       })
  //       this.fashionBrandModal.getDatabaseState().subscribe(ready => {
  //         if (ready) {
  //           if (inactive_fash_brand_collections !== undefined && inactive_fash_brand_collections !== null) {
  //             this.fashionBrandModal.deleteMultipleFashionBrandsCollection(inactive_fash_brand_collections).then(isDeleted => {
  //             })
  //           }
  //           if (inactive_fash_brands !== undefined && inactive_fash_brands !== null) {
  //             this.fashionBrandModal.deleteMultipleFashionBrands(inactive_fash_brands).then(isDataInserted => {
  //             })
  //           }
  //           this.fashionBrandModal.saveIntoFashBrandsTable(FashionBrands).then(categoryInsertedSuccessfully => {

  //             // this.resfreshHomepage();
  //             this.fashionBrandModal.saveIntoFashBranCollectionsTable(FASHIONBRANDSCOLLECTIONARRAY).then(isSubCategoriesSsaved => {

  //             })
  //           })
  //         }
  //       })

  //       //insert data to search table
  //       // this.SALON_SERVICE_SEARCH = []
  //       // FASHIONBRANDSCOLLECTIONARRAY.forEach(element => {
  //       //   let item = {
  //       //     keyword: element.bbc_name,
  //       //     type: 'fash_brand_collections',
  //       //     refer_id: String (element.fbc_id),
  //       //     image_url: element.fbc_image
  //       //   }
  //       //   this.SALON_SERVICE_SEARCH.push(item)
  //       // });
  //       // this.sqlProvider.deleteSearchDataByType('"fash_brand_collections"');
  //       // this.sqlProvider.getDatabaseState().subscribe(ready => {
  //       //   if (ready) {
  //       //     if (this.SALON_SERVICE_SEARCH.length > 0) {
  //       //       this.sqlProvider.InsertInTblSearch(this.SALON_SERVICE_SEARCH);
  //       //     }
  //       //   }
  //       // })
  //     } else {
  //     }
  //   }



  // }
  handle_serviceCategories_Response(ServiceCategories, inactive_ser_sub_categoires, inactive_ser_categoires) {
    if (ServiceCategories != undefined) {
      if (ServiceCategories.length > 0) {
        let serviceCategories: SERVICE_CATEGORY[] = []
        serviceCategories = ServiceCategories
        let SubCategoriesArray: ServiceSubCATEGORIES[] = []
        serviceCategories.forEach(serviceCategory => {
          SubCategoriesArray = SubCategoriesArray.concat(serviceCategory.ServiceSubCategories)
        });
        this.sqlProvider.getDatabaseState().subscribe(ready => {
          if (ready) {
            if (inactive_ser_sub_categoires !== undefined && inactive_ser_sub_categoires !== null) {
              this.sqlProvider.deleteSearchKeyWordsBySerCatId(inactive_ser_sub_categoires, 'key_word').then(isDataInserted => {
              })
            }
          }
        })
        this.salonServicesModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            if (inactive_ser_sub_categoires !== undefined && inactive_ser_sub_categoires !== null) {
              this.salonServicesModal.deleteMultipleSubCategories(inactive_ser_sub_categoires).then(isDeleted => {
              })
            }
            if (inactive_ser_categoires !== undefined && inactive_ser_categoires !== null) {
              this.salonServicesModal.deleteAllServiceCategories(inactive_ser_categoires).then(isDataInserted => {
              })
            }
            this.salonServicesModal.saveIntoServiceCategoryTable(serviceCategories).then(categoryInsertedSuccessfully => {
              this.salonServicesModal.insert_services_sub_cat_table(SubCategoriesArray).then(isSubCategoriesSsaved => {

              })
            })
          }
        })
        this.SALON_SERVICE_SEARCH = []
        SubCategoriesArray.forEach(element => {
          let item = {
            keyword: element.ssc_name,
            type: 'ser_sub_categories',
            refer_id: element.ssc_id,
            image_url: element.ssc_image
          }
          this.SALON_SERVICE_SEARCH.push(item)
        });
        this.sqlProvider.deleteSearchDataByType('"ser_sub_categories"');
        this.sqlProvider.getDatabaseState().subscribe(ready => {
          if (ready) {
            if (this.SALON_SERVICE_SEARCH.length > 0) {
              this.sqlProvider.InsertInTblSearch(this.SALON_SERVICE_SEARCH);
            }

          }
        })

      } else {
      }
    }
  }
  handel_lstSalonsService_Response(response) {

    this.allSalon = response["salons"];
    if (this.allSalon && this.allSalon.length > 0) {
      this.getAndSetSalonOffer(this.allSalon);
    }

    let salonIds = this.getSalonIdsAndInsertSalonsToSearchTable(this.allSalon, response.inactive_salons);
    //  this.insertSalServiceSub(response.sal_ser_sub_categories, salonIds);//8 second delay
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.salonServicesModal.deleteMultipleSalonServiceCatogries(salonIds).then(res => {
          this.salonServicesModal.saveSalonServicesCategoriesIntoDB(this.allSalon);
        })

        this.salonServicesModal.deleteMultipleSalonServices(salonIds).then(res => {
          this.insertSalonServices(salonIds);
        })
      }
    })
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.deleteMultipleSalon(salonIds).then(res => {
          if (this.allSalon != undefined && this.allSalon != null) {
            this.sqlProvider.insert_salon_table(this.allSalon);
          }
        })
      }
    })
    this.salonIdAll = salonIds
    let get_current_date_time = this.serviceManager.getCurrentDateTime();
    this.serviceManager.setInLocalStorage(this.serviceManager.LAST_FETCH_DATE_SALONS, get_current_date_time);
    this.serviceManager.setInLocalStorage(this.serviceManager.SALONS_NEAR_CUSTOMER, this.allSalon)
    console.log("SalHoursCalculationStart......");
    this.calCulateSalHours(response.inactive_salons);
    console.log("EndSalHoursCalculationStart......");
  }
  calCulateSalHours(inactive_salons) {
    this.SalonHoursAll = []
    this.allSalon.forEach(salon => {
      let sal_Id = salon.sal_id;
      let sal_hours = salon.sal_hours1;

      let salonHours = {};
      let salonHoursKeys = [];
      salonHoursKeys = Object.keys(sal_hours);
      salonHours = sal_hours;
      let temp = {};
      salonHoursKeys.forEach(element => {
        let split = sal_hours[element].split('&');
        if (split[0] == split[1]) {
          temp[element] = "Off";
        } else {
          temp[element] = this.convert24Hrto12Hr(split[0]) + " - " + this.convert24Hrto12Hr(split[1]);
        }
      });
      salonHours = temp;
      salonHoursKeys.forEach(dayKey => {

        let salonHoursObject = {
          sal_id: Number(sal_Id),
          sal_hours: salonHours[dayKey],
          sal_day: dayKey
        }

        this.SalonHoursAll.push(salonHoursObject);
      });
    });
    //insert sal Hours to DB
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (inactive_salons !== undefined && inactive_salons !== null) {
          this.sqlProvider.deleteSalHours(inactive_salons).then(isDataInserted => {
          })
        }
        if (this.SalonHoursAll !== undefined && this.SalonHoursAll !== null && this.SalonHoursAll.length > 0) {
          this.sqlProvider.saveIntoSalHoursTable(this.SalonHoursAll).then(isDataInserted => {
            console.log("InsertedAllSalHours", isDataInserted)
          })
        }
      }
    })
  }
  getAndSetSalonOffer(salons) {
    let so_ids = '';
    this.ALLOFFERS = []
    salons.forEach(element => {
      if (element.offers !== null) {
        for (let index = 0; index < element.offers.length; index++) {
          const item = element.offers[index];
          // so_ids += item['so_id'] + ',' 
          this.ALLOFFERS.push(item)
        }
      }
    });
    // so_ids = so_ids.slice(0, so_ids.length - 1)//after loop
    this.saveSalonOffers(this.ALLOFFERS)
  }
  saveSalonOffers(ALLOFFERS) {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (ALLOFFERS != undefined && ALLOFFERS != null) {
          this.salonServicesModal.insert_salon_offer(ALLOFFERS);

        }
      }
    })
  }
  insertSalonServices(salonIds) {
    if (!this.allSalon) {
      return
    }
    this.SalonsServices = []
    this.allSalon.forEach(salon => {
      this.SalonsServices = this.SalonsServices.concat(salon.services)
    });



    if (this.SalonsServices && this.SalonsServices.length > 0) {
      this.serviceManager.setInLocalStorage(this.serviceManager.NEAREST_SALON_SERVICES, this.SalonsServices)
    }


    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.saveSalonServicesIntoDB(this.allSalon)
      }
    })

    //zahid try to check this out
    // this.salonServicesModal.insert_salon_services_table(this.SalonsServices)
    this.SALON_SERVICE_SEARCH = []
    this.SalonsServices.forEach(element => {
      let item = {
        keyword: element.sser_name,
        type: 'service',
        refer_id: element.ser_id,
        image_url: ''
      }
      this.SALON_SERVICE_SEARCH.push(item)
    });



    // if (this.SALON_SERVICE_SEARCH && this.SALON_SERVICE_SEARCH.length > 0) 
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {



        this.sqlProvider.deleteSearchBySalIdFromService(salonIds, 'service').then(isDeleted => {
          if (this.SALON_SERVICE_SEARCH && this.SALON_SERVICE_SEARCH.length > 0) {
            this.sqlProvider.InsertInTblSearch(this.SALON_SERVICE_SEARCH).then(isDataInserted => {

            })
          }
        })


      }
    })
  }
  insertSalServiceSub(sualServiceSaubCat, salIds) {
    // this.SAL_SER_SUB_CATEGORIE = []
    // this.SAL_SER_SUB_CATEGORIE = sualServiceSaubCat
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.deleteMultipleSalServiceSubCat(salIds).then(res => {
          if (sualServiceSaubCat != undefined && sualServiceSaubCat != null) {
            this.sqlProvider.insert_sal_ser_sub_categories(sualServiceSaubCat);
          }
        })
      }
    })
  }
  isDateChanged(dateInList, datePreviousInList) {
    //allNotifications.notifications[i-1].not_datetime!=notification.not_datetime
    dateInList = dateInList.split(" ");
    dateInList = dateInList[0];
    datePreviousInList = datePreviousInList.split(" ");
    datePreviousInList = datePreviousInList[0];
    if (dateInList != datePreviousInList) {
      return true
    } else {
      return false;
    }
  }
  sideMenuAndTabBarEvents() {
    this.menu.swipeEnable(true);
    // if (this.globalService.tabIndex == "0") {
    //   let HideTabe = this.navParams.get('isHideTab');
    //   if (HideTabe !== null && HideTabe == '1') {
    //     for (let i = 0; i < (this.navCtrl.length() - 1); i++) {
    //       this.navCtrl.remove(this.navCtrl.getPrevious().index);
    //     }
    //     this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    //     if (this.tabBarElement) {
    //       this.tabBarElement.style.display = 'none';
    //     }
    //   }
    //   this.globalService.tabIndex = '-1'
    // }


  }
  requestToServerForAppData() {
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.getAllSalonIDs().then(strSalonIDs => {
          this.strAllSalonIDs = strSalonIDs

          if (this.myCustomer) {
            this.customerExist = true
            //this.LAST_UPDATE_DATE_TIME="";//temprarily
            this.googleAnalytics()
            this.checkUpdateInServices(true, this.LAST_UPDATE_DATE_TIME, this.myCustomer);
          } else {
            this.customerExist = false
            let publicUserId = this.serviceManager.getFromLocalStorage(this.serviceManager.PUBLIC_USER_ID)
            let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN);
            this.sp_view_strip = this.serviceManager.getFromLocalStorage(this.serviceManager.SP_VIEW_STRIP)
            if (!publicUserId) {
              this.getPublicUserLocation()
              // this.savePublicUser()
            } else {

            }

            this.geolocation.getCurrentPosition().then((resp) => {
              this.myCustomer.cust_lat = resp.coords.latitude.toString()
              this.myCustomer.cust_lng = resp.coords.longitude.toString()

              this.checkUpdateInServices(true, this.LAST_UPDATE_DATE_TIME, this.myCustomer);

            }).catch((error) => {

              this.checkUpdateInServices(true, this.LAST_UPDATE_DATE_TIME, this.myCustomer);
              console.log('Error getting location', error);
            });
            // this.checkUpdateInServices(true, this.LAST_UPDATE_DATE_TIME, this.myCustomer);

            this.getCustomer('checkUpdateInServices')

          }
        })
      }
    })
  }
  getPublicUserLocation() {
    let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN);
    this.geolocation.getCurrentPosition().then((resp) => {
      this.savePublicUser(token, resp.coords.latitude.toString(), resp.coords.longitude.toString())
    }).catch((error) => {
      this.savePublicUser(token, null, null)
      console.log('Error getting location', error);
    });

  }
  savePublicUser(token, latitude, longitude) {
    if (this.platForm.is('ios')) {
      this.deviceType = "1";
    }
    if (this.platForm.is('android')) {
      this.deviceType = "2";
    }
    const params = {
      service: btoa('save_public_user'),
      pu_device_id: btoa(token),
      pu_device_type: btoa(this.deviceType),
      pu_lat: btoa(latitude),
      pu_lng: btoa(longitude),
    }
    console.log('pubLicUser', JSON.stringify(params))
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          console.log('resPublicUser', JSON.stringify(res))

          if (res.pu_id) {
            this.serviceManager.setInLocalStorage(this.serviceManager.PUBLIC_USER_ID, res.pu_id)
          }
          // this.myCustomer.cust_lat = resp.coords.latitude.toString()
          // this.myCustomer.cust_lng = resp.coords.longitude.toString()
          // this.customerModal.getDatabaseState().subscribe(ready => {
          //   if (ready) {
          //     this.customerModal.updateCustomerTable(resp.coords.latitude, resp.coords.longitude)
          //   }
          // })

        },
        (error) => {
          // this.serviceManager.stopProgress()
          console.log(error);

          // this.serviceManager.makeToastOnFailure(error, 'top')
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        },
        () => {
          // this.serviceManager.stopProgress()
        }
      )




  }
  deleteTableOnUpdateLocation() {
    try {
      let lastFetchedDate = this.serviceManager.getFromLocalStorage(this.serviceManager.LAST_UPDATE_DATE_TIME)
      if (lastFetchedDate) {
        this.LAST_UPDATE_DATE_TIME = lastFetchedDate
      } else {
        this.salonServicesModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.salonServicesModal.deleteSserviceSubCategories();
            this.salonServicesModal.deleteSserviceCategories();
            this.salonServicesModal.deleteCategorySubStyle();
            this.salonServicesModal.deleteSalonService();
            //    this.tipsDetailModal.deleteBeautyTips()
            this.salonServicesModal.deleteSalonServiceCategories();
            this.salonServicesModal.deleteOffers();
            this.sqlProvider.deleteAllSalons();
            this.sqlProvider.deleteSalonTechnician();
            this.sqlProvider.deletesalon_hours();
          }
        })
        // this.fashionBrandModal.getDatabaseState().subscribe(ready => {
        //   if (ready) {
        //     this.fashionBrandModal.deleteFashinBrandsCollection();
        //     this.fashionBrandModal.deleteFashionBrands();
        //   }
        // })
      }
    } catch (error) {
    }
  }
  createAllDatabaseTables() {
    // this.updateHomePageStates();

    // this.fashionBrandModal.getDatabaseState().subscribe(ready => {
    //   if (ready) {
    //     this.fashionBrandModal.createFash_BrandsTable();
    //     this.fashionBrandModal.createFavoriteFashionTable();
    //     this.fashionBrandModal.create_fashion_collectionsTable();
    //     this.fashionBrandModal.create_table_fash_brand_collections();
    //     this.fashionBrandModal.createFBC_Images();
    //     this.fashionBrandModal.createFashion_Dates_Table();
    //     this.fashionBrandModal.createFashion_Dates_Bran_Table();
    //     this.fashionBrandModal.createFB_Images();
    //     this.favModel.createFavoriteTable();
    //   }
    // })
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.salonServicesModal.create_table_service_sub_categories();
        this.salonServicesModal.create_table_salon_service_categories();
        this.salonServicesModal.createTrend_Dates_Table();
        this.salonServicesModal.create_table_salon_services()
        this.salonServicesModal.createServiceCategoryTable();
        this.salonServicesModal.create_table_offers();
        this.salonServicesModal.createCategorySubStyle();
      }
    })

    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.create_table_notification();
        this.sqlProvider.create_table_salon();
        this.sqlProvider.createSalonTechnician();
        this.sqlProvider.createSalonHoursTable();
        this.sqlProvider.create_table_sal_ser_sub_categories();
        this.sqlProvider.create_table_search();

      }
    })

   

    this.configModal.getDatabaseState().subscribe(ready => {
      if (ready) { this.configModal.createAppConfigTable() }
    })

    this.beautyTipsModal.getDatabaseState().subscribe(ready => {
      if (ready) { this.beautyTipsModal.createBeautTipsTable() }
    })

    this.tipsDetailModal.getDatabaseState().subscribe(ready => {
      if (ready) { this.tipsDetailModal.createTipsDetailTable() }
    })
    
  }
  runAgainstTagName() {

  }
  selectedEnviroment: number
  getSalonIdsAndInsertSalonsToSearchTable(response, inactive_salons_ids) {
    //in this function we are doing 2 differnt task first is to get all salon id's and 2nd is get and prepare all salons for search table
    //  this.sqlProvider.deleteSearchDataByType('"salon"');
    this.SALON_SERVICE_SEARCH = []
    let salonIds = '';
    let i = 0;
    response.forEach(element => {
      //for search table 
      let item = {
        keyword: element.sal_name,
        type: "salon",
        refer_id: element.sal_id,
        image_url: element.sal_pic
      }
      this.SALON_SERVICE_SEARCH.push(item)
      salonIds += element.sal_id + ','
    });
    salonIds = salonIds.slice(0, salonIds.length - 1)
    if (salonIds.length > 0) {
      salonIds = salonIds + "," + inactive_salons_ids;
    } else {
      salonIds = inactive_salons_ids;
    }
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {

        if (salonIds !== undefined && salonIds !== null) {
          this.sqlProvider.deleteSearchKeyWordsBySalId(salonIds, 'salon').then(isDataInserted => {
          })
        }
        if (this.SALON_SERVICE_SEARCH !== undefined && this.SALON_SERVICE_SEARCH !== null && this.SALON_SERVICE_SEARCH.length > 0) {
          this.sqlProvider.InsertInTblSearch(this.SALON_SERVICE_SEARCH).then(isDataInserted => {
          })
        }
      }
    })
    // this.sqlProvider.insert_saerch_table_new(this.SALON_SERVICE_SEARCH);
    return salonIds;
  }
  //get home page total counts
  getHomePageCounts() {
    let notificationController = this.loadingController.create({
      content: "Fetching Home page"
    });
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('home_page_counts'),
      last_fetched: btoa(this.LAST_UPDATE_DATE_TIME),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {

        this.saveHomePageCounts(response)

      },
        error => {
          notificationController.dismissAll();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        });
  }
  saveHomePageCounts(response) {
    let total_salons = 0
    let total_producs = 0
    let total_fashion = 0
    let total_beauty_tips = 0
    let total_beauty_trends = 0
    if (response.home_page_counts) {
      total_salons = response.home_page_counts[0].count
      total_producs = response.home_page_counts[1].count
      total_fashion = response.home_page_counts[2].count
      total_beauty_trends = response.home_page_counts[3].count
      total_beauty_tips = response.home_page_counts[4].count
    }


    this.homeModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.homeModal.InsertInToHomeStatsTable('1', total_salons, total_fashion, total_beauty_trends, total_beauty_tips, total_producs).then(homePageStats => {
          this.homeModal.getHomePageStats().then(homePageStats => {

            if (homePageStats) {
              this.getHomePageStatic(homePageStats)
            }

          }, error => {
          })

        }, error => {
        })
      }
    })
  }
  //end get home page counts
  getPage(view) {
    // return AppointmentsCardPage;

    if (view == 'TabsPage') {
      return TabsPage;
    } else if (view == 'BeautyTipsViewPage') {

      return BeautyTipsViewPage;
    } else if (view == 'AppointmentsCardPage') {
      return AppointmentsCardPage;
    } else {
      return null;
    }
  }
  findNextPage(homePageSection: Homepage) {

    this.globalService.sty_id = homePageSection.sectionParameters;//to get salon for salon type id in salon listing page......
    this.serviceManager.setInLocalStorage('homePageSection', homePageSection)
    this.nextPage = this.getPage(homePageSection.view);
    this.sectionParameters = homePageSection.sectionParameters;
    // if (homePageSection.view == 'LookingForPage' || homePageSection.view == 'TabsPage') {
    //   this.serviceManager.removeFromStorageForTheKey(this.serviceManager.OBJ_SUB_STYLE)
    //   this.serviceManager.removeFromStorageForTheKey(this.serviceManager.OBJ_PIN)

    // }

    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(this.nextPage, {
      parentPage: homePageSection,
      sectionParameters: this.sectionParameters
    });



  }
  SaveHomePageDataIntoDB(homepageDataToSave) {
    if (!homepageDataToSave || homepageDataToSave.length === 0) {
      return
    }
    this.homeModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.homeModal.createMainHomeTable().then(istblHomePageSectionsCreated => {
          if (istblHomePageSectionsCreated) {
            this.homeModal.InsertInToHomeTable(homepageDataToSave).then(res => {
              // this.getHomePageSectionsFromDB();
            }, error => {
            })
          }
        }, error => {

        })

      } else {

      }
    })

  }
  getHomePageSectionsFromDB() {
    this.homepageData = this.serviceManager.getFromLocalStorage('homePageBackUpData')
    this.homeModal.getDatabaseState().subscribe(ready => {
      if (ready) {

        this.homeModal.getHomeSections().then(homepageData => {
          this.homepageData = homepageData

        }, error => {

        })
      }
    })
  }
  getHomePageStatic(homePageStats) {
    let totalBeautyTips = homePageStats.toal_beauty_tips
    if (totalBeautyTips == "0") {
      totalBeautyTips = "7"
    }
    this.homepageData = []
    // let itemLatestTrends = {
    //   sectionName: "<span> <font color='#4a9ee6'> Beauty Trends </font></span><br>",
    //   sectionDescription: "Must know trends you will\n love",
    //   sectionImage: "home_latesttrends.png",
    //   sectionBackground: '',
    //   Id: 1,
    //   is_header: '0',
    //   view: 'LatestTrendsPage',
    //   sort_order: '2',
    //   status: '1',
    //   sectionParameters: '',
    //   otherDescription: homePageStats.toal_beauty_trends + ' +\n Hot Trends',
    //   isDeal: '0',
    //   itemBgColour: '#4a9ee6',
    // }

    // let itemBeautyTipsFashion = {
    //   sectionName: "<span> <font color='#000000'> Fashion Trends </font></span><br> ",

    //   sectionDescription: "Must know trends you will\n love",
    //   sectionImage: "home_beauty_trends.png",
    //   sectionBackground: '',
    //   Id: 1,
    //   is_header: '0',
    //   view: 'BeautyTipsFashionPage',
    //   sort_order: '3',
    //   status: '1',
    //   sectionParameters: '',
    //   otherDescription: homePageStats.toal_fashion + '+\n Hot Trends',
    //   isDeal: '0',
    //   itemBgColour: '#681c9a',
    // }

    let ItemBookAppointments = {
      sectionName: " <span> <font color='#000000'>Find a specialist</font></span><br> ",
      sectionDescription: "with\n            salons of your choice",
      sectionImage: "beauty_salon.png",
      sectionBackground: '',
      Id: 2,
      is_header: '0',
      view: 'AppointmentsCardPage',
      sort_order: '1',
      status: '1',
      sectionParameters: '',
      otherDescription: 'You can easily search doctors by speciality.',
      isDeal: '0',
      itemBgColour: '#ed5598',
    }
    let itemBeautyTips = {

      sectionName: "<span> <font color='#000000'> Find Doctor By City </font></span><br>",
      sectionDescription: "Tips that will make you\n  superstart",
      sectionImage: "home_beautytips.png",
      sectionBackground: '',
      Id: 4,
      is_header: '0',
      view: 'BeautyTipsViewPage',
      sort_order: '5',
      status: '1',
      sectionParameters: '',
      otherDescription: 'You can easily search doctors by City.',
      isDeal: '0',
      itemBgColour: '#029aac',
    }
    let ItemProducts = {
      sectionName: "<span> <font color='#000000'> Find doctor by name, address, phone number </font></span><br> ",
      sectionDescription: "Buy Top Brand",
      sectionImage: "home_products.png",
      sectionBackground: '',
      Id: 5,
      is_header: '1',
      view: 'ProductsPage',
      sort_order: '4',
      status: '1',
      sectionParameters: '',
      otherDescription: 'You can easily search doctors by names, phone numbers and address.',
      isDeal: '0',
      itemBgColour: '#bf4565',
    }
    // let ItemLatestTrendsPageOld = {
    //   sectionName: "Contact an online doctor",
    //   sectionDescription: "Buy Top Brand",
    //   sectionImage: "home_products.png",
    //   sectionBackground: '',
    //   Id: 5,
    //   is_header: '0',
    //   view: 'LatestTrendsPageOld',
    //   sort_order: '5',
    //   status: '1',
    //   sectionParameters: '',
    //   otherDescription: 'You can easily search doctors by names, phone numbers and address.',
    //   isDeal: '0'
    // }


    // let ItemDealOfTheMoment = {
    //   sectionName: "Get prescription now",
    //   sectionDescription: "50%",
    //   sectionImage: "home_products.png",
    //   sectionBackground: '',
    //   Id: 5,
    //   is_header: '0',
    //   view: 'ProductsPage',
    //   sort_order: '1',
    //   status: '1',
    //   sectionParameters: '',
    //   otherDescription: 'You can easily get prescription with senior doctor.',
    //   isDeal: '1'
    // }
    this.homepageData.push(ItemBookAppointments);
    // this.homepageData.push(itemLatestTrends);
    this.homepageData.push(itemBeautyTips);
    this.homepageData.push(ItemProducts);
    // this.homepageData.push(itemBeautyTipsFashion);
    // this.homepageData.push(ItemLatestTrendsPageOld);
    // this.homepageData.push(ItemLatestTrendsPageOld);
  }
  getCustomer(funcName) {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          if (customer) {
            this.myCustomer = customer

            this.googleAnalytics()

            if (funcName === 'checkUpdateInServices') {
              this.checkUpdateInServices(true, this.LAST_UPDATE_DATE_TIME, this.myCustomer);
            }
          } else {
            //this.navCtrl.setRoot(ActivateSalonPage) //commented by riz on dec 21
          }
        }, error => {
        })
      }
    })
  }
  googleAnalytics() {
    this.ga.trackView(this.myCustomer.cust_name + ' visited Home Page');
    this.ga.trackTiming('cat: tracking timing', 600000, 'variable: not sure what will go here', 'label: and the same')
    this.ga.debugMode()
    this.ga.setAllowIDFACollection(true)
    this.ga.setUserId(this.myCustomer.cust_id)

  }
  getDistanceDifference(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") { dist = dist * 1.609344 }
    if (unit == "M") { dist = dist * 0.8684 }
    return dist
  }
  updatelocation() {

    this.geolocation.getCurrentPosition().then((resp) => {
      const params = {
        service: btoa('update_location'),
        sal_id: btoa(this.myCustomer.cust_id),
        sal_lat: btoa(resp.coords.latitude.toString()),
        sal_lng: btoa(resp.coords.longitude.toString()),
      }

      this.serviceManager.getData(params)
        .retryWhen((err) => {
          return err.scan((retryCount) => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            }
            else {
              throw (err);
            }
          }, 0).delay(1000)
        })
        .subscribe(
          (res) => {
            console.log(res.msg);
            this.myCustomer.cust_lat = resp.coords.latitude.toString()
            this.myCustomer.cust_lng = resp.coords.longitude.toString()
            this.customerModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.customerModal.updateCustomerTable(resp.coords.latitude, resp.coords.longitude)
              }
            })

          },
          (error) => {
            // this.serviceManager.stopProgress()
            console.log(error);

            // this.serviceManager.makeToastOnFailure(error, 'top')
            this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

            console.log('something went wrong', error);
          },
          () => {
            // this.serviceManager.stopProgress()
          }
        )

      console.log('resp.coords.latitude ' + resp.coords.latitude)
      console.log(resp.coords.longitude)

    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }
  saveCustomerIntoDB() {
    let customer: Customer = this.myCustomer
    if (customer) {
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.createCustomerTable().then(isTableCreated => {
            if (isTableCreated === true) {
              this.customerModal.InsertInToCustomerTable(customer).then(isCustomerInserted => {
                if (isCustomerInserted === true) {
                  console.log('success: InsertInToCustomerTable ');


                  this.serviceManager.setInLocalStorage(this.serviceManager.IS_USER_LOGIN, true)
                  this.storage.set('isLogin', true);
                  this.events.publish('customer:changed', customer);

                  this.nativePageTransitions.slide(this.options)
                  this.navCtrl.push(MainHomePage, { myCustomer: customer })
                    .then(() => {
                      const index = this.viewCtrl.index;
                      this.navCtrl.remove(1);
                      this.navCtrl.remove(0);
                    });

                } else {
                  console.log('error: InsertInToCustomerTable ', isCustomerInserted);

                }


              }, error => {
                console.log('error: ', error.message);

              })
            }

          })
        }
      })
    }

  }
  getAndSaveAppConfig() {

    let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')

    if (_appConfig && _appConfig.currency) {
      this.globalService.currencySymbol = _appConfig.currency
      this.globalService.distance_unit = _appConfig.distance_unit

    } else {
      this.configModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.configModal.getAllAppConfig().then(data => {

            if (data && data.currency) {
              this.globalService.currencySymbol = data.currency
              this.globalService.distance_unit = data.distance_unit

            } else {
              this.getAppConfigFromServer()

            }

          })
        }
      })
    }


    console.log('eenenenene');



  }
  getAppConfigFromServer() {

    const params = {
      service: btoa('get_config'),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          let apConfigData: APP_CONFIG = res.config

          this.serviceManager.setInLocalStorage('isAppConfigs', apConfigData)

          if (apConfigData) {
            this.globalService.currencySymbol = apConfigData.currency
            this.globalService.distance_unit = apConfigData.distance_unit
          }
          this.configModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.configModal.InsertInAppConfigTable(apConfigData)
            }
          })
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        })
  }
  gpsOptions = { maximumAge: 300000, timeout: 5000, enableHighAccuracy: true };

  confirmSwitchToSetting() {
    let alert = this.alertCtrl.create({
      title: 'Your location is not availble for BeautyApp. Please enable your location',
      message: 'Do you want to enable Location?',
      buttons: [
        {
          text: 'Deny',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown = false;
          }
        },
        {
          text: 'Allow',
          handler: () => {
            console.log('Yes clicked');

            this.diagnostic.switchToLocationSettings()
          }
        }
      ]
    });
    alert.present().then(() => {
      this.alertShown = true;
    });

  }
  switchEnviroment(tapEvent) {

    if (tapEvent.tapCount === 5) {
      // let alertControllerOPtions: NativeTransitionOptions = {
      //   direction: 'top',
      //   duration: 500,
      //   slowdownfactor: 3,
      //   slidePixels: 0,
      //   iosdelay: 100,
      //   androiddelay: 150,
      //   fixedPixelsTop: 0, // not to include this top section in animation 
      //   fixedPixelsBottom: 0 // not to include this Bottom section in animation 
      // };
      // this.nativePageTransitions.slide(alertControllerOPtions)
      this.navCtrl.push(AppInfoPage)

      // let profileModal = this.modalCtrl.create(AppInfoPage, { userId: 8675309 });
      // profileModal.present();
    }

  }
  //static codes
  //On app exit message
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm you want to exit....',
      message: 'Do you want exit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown = false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            // this.customHandleBackButton();
            this.platForm.exitApp();
          }
        }
      ]
    });
    alert.present().then(() => {
      this.alertShown = true;
    });

  }
  padToTwo(number) {
    if (number <= 9) { number = ("0" + number).slice(-2); }
    return number;
  }
  convert24Hrto12Hr(time) {
    if (time === undefined || time === null) {
      this.navCtrl.pop()
      return;
    }
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = + time[0] % 12 || 12; // Adjust hours
    }
    time[0] = this.padToTwo(time[0]);
    return time.join(''); // return adjusted time or original string  
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platForm.registerBackButtonAction(() => {


      if (this.menu.isOpen()) {
        this.menu.close();
      } else {
        if (this.alertShown == false) {
          this.alertShown = true
          this.presentConfirm();
        }
      }

    }, 10);
  }
  public appInfo = ''
  getAppVersionAndcheckForceUpdate() {

    this.appInfo = ''
    try {
      this.appVersion.getAppName().then(appName => {
        this.appName = appName
        this.appInfo += 'app_Name: ' + appName + ',\n'
        this.appVersion.getPackageName().then(packageName => {
          this.appInfo += ('package_Name: ' + packageName + ',\n')
          this.appVersion.getVersionCode().then(buildNo => {
            this.buildNo = buildNo
            this.appInfo += ('version Code: ' + buildNo + ',\n')
            this.appVersion.getVersionNumber().then(versionNumber => {
              this.Version = versionNumber
              this.appInfo += ('versionNumber: ' + versionNumber + ',\n')
              this.checkForceUpdate()

            })
          })
        })
      })
    } catch (error) {

    }


  }
  public appPlatform = ''
  public Version = ''
  public buildNo = ''
  public appName = ''
  public showForceUpdatePushed = false;
  checkForceUpdate() {


    this.buildNo = '5'
    const params = {
      service: btoa('force_update'),
      platform: btoa(this.appPlatform),
      app_version: btoa(this.Version),
      app_build_no: btoa(this.buildNo),
      app_name: btoa(this.appName)
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          if (res.force_update && Number(res.force_update) === 1) {
            this.serviceManager.setInLocalStorage(this.serviceManager.IS_FORCE_UPDATE_AVAILABLE, true)
            this.serviceManager.setInLocalStorage(this.serviceManager.FORCE_UPDATE_MESSAGE, res.update_msg)
            this.serviceManager.setInLocalStorage('lastForceUpdateFetchTime', res.response_datetime)

            this.appCtrl.getRootNav().setRoot(ForceUpdatePage, {
              update_msg: res.update_msg
            })
          } else {

            this.serviceManager.setInLocalStorage('lastForceUpdateFetchTime', this.LAST_UPDATE_DATE_TIME)
            // this.appCtrl.getRootNav().setRoot(MainHomePage)
            this.serviceManager.removeFromStorageForTheKey(this.serviceManager.IS_FORCE_UPDATE_AVAILABLE)
          }
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        }
      )
  }
  //custom splash work:
  //need to show a splash for first time in spalsh there can be anything ad, promotion etc.

  getCustomSplash() {

    const params = {
      service: btoa('get_splash'),
      sp_page: btoa('home'),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {

          this.hideHompePage = true

          console.log('resCustomeSpash', JSON.stringify(res))

          this.customSplashHtml = res.splash.sp_view_main
          this.sp_view_strip = res.splash.sp_view_strip

          this.sp_background = res.splash.sp_background

          this.serviceManager.setInLocalStorage(this.serviceManager.SP_VIEW_STRIP, this.sp_view_strip)

          console.log('responseSplash', this.customSplashHtml)

          if (Number(res.splash.sp_status) === 1) {
            this.serviceManager.setInLocalStorage(this.serviceManager.IS_SPLAH_SHOWN, true)

          }
        },
        (error) => {

        },
        () => {
        }
      )
  }

  //to on hreff click to navigate to salon or product
  htmlOnClick() {

    this.renderer.listen(this.elementRef.nativeElement, 'click', (event) => {
      var rc: boolean = true;
      if (event.target.nodeName == 'A') {

        rc = this.processClickOnATag(event.target.href);
      }
      return rc;
    })

  }

  processClickOnATag(href: string): boolean {
    var rc: boolean = true;
    var pos: number;

    if ((pos = href.indexOf('sign_up')) !== -1) {

      rc = this.processClickOnSignUpLink(href.substring(pos))
    } else if ((pos = href.indexOf('cancel')) !== -1) {
      this.hideHompePage = false
      // rc = this.processClickOnProductPage(href.substring(pos))
    }
    return rc;
  }
 
  private processClickOnSalonPageLink(pageRef: string): boolean {
    var rc: boolean = true;
    this.navCtrl.push(HomePage);
    return rc;
  }
  private processClickOnSignUpLink(pageRef: string): boolean {
    var rc: boolean = true;

    if (this.isPushToNextPage) {
      this.navCtrl.push(LoginSignUpPage);
      this.isPushToNextPage = false
    }

    rc = false;
    return rc;
  }

  //end hreff events


}