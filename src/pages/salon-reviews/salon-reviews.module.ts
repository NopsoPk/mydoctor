import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalonReviewsPage } from './salon-reviews';

@NgModule({
  declarations: [
    SalonReviewsPage,
  ],
  imports: [
    IonicPageModule.forChild(SalonReviewsPage),
  ],
})
export class SalonReviewsPageModule {

 
}
