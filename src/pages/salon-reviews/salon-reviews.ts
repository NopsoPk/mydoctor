import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, ViewController } from 'ionic-angular';
import { DatePipe } from '@angular/common'
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/retrywhen';
import { MainHomePage } from '../main-home/main-home';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { GlobalProvider } from '../../providers/global/global';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

/**
 * Generated class for the SalonReviewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-salon-reviews',
  templateUrl: 'salon-reviews.html',
})
export class SalonReviewsPage {
  allReviews: any[];
  public unregisterBackButtonAction: any;
  currentDate: string = "";
  salId: string;
  rate = 1;
  constructor(
    private menu: MenuController,
    private loadingController: LoadingController,
    public navCtrl: NavController, public datepipe: DatePipe,
    public storage: Storage,
    private viewCtrl: ViewController,
    public navParams: NavParams,
    public platform: Platform,
    public serviceManager: GlobalProvider,
  ) {

  }

  viewAllServices() {

  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // this.navCtrl.pop();
    if(this.menu.isOpen()){
      this.menu.close();
    }else {
       if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage)
    }
    }
  }

  ionViewWillEnter() {

    this.salId = this.navParams.get("salId");
    this.currentDate = this.serviceManager.getCurrentDateTime();
    this.getReviews();
  }

  getReviews() {
    let loading_controller = this.loadingController.create({
      content: "Getting Reviews"
    });
    loading_controller.present();
    var params = {
      app_review_datetime_less: btoa(this.currentDate),
      service: btoa('get_reviews'),
      sal_id: btoa(this.salId),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        console.log('AllResponse', response.reviews);
        this.allReviews = response.reviews;
        loading_controller.dismiss();
      },
        error => {
          loading_controller.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }



  goBack() {
    this.navCtrl.pop();
  }

  getDateFormated(Datetime) {
    //Datetime yyyy-MM-dd hh:mm:ss
    let fullDate = Datetime.split(" ");
    fullDate = fullDate[0];
    let date = new Date();
    let latest_date = this.datepipe.transform(date, 'yyyy-MM-dd');//2017-12-12 04:38:54 full format yyyy-MM-dd hh:mm:ss

    let dt1 = new Date(latest_date);
    let dt2 = new Date(fullDate);

    let diff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));

    let dateFormated = this.datepipe.transform(fullDate, 'MMM dd, yyyy');
    return dateFormated;
  }

  onModelChange(value) {
    console.log("Val", value)
  }
}
