import {
  Component,
  NgZone,
  ElementRef,
  ViewChild,
  Renderer
} from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Events,
  LoadingController
} from "ionic-angular";
import { Platform } from "ionic-angular";
import { timer } from "rxjs/observable/timer";
import { MainHomePage } from "../main-home/main-home";
import { GlobalProvider } from "../../providers/global/global";
import {
  TIPS_DETAIL,
  Salon,
  BEAUTY_TIPS,
  NOPSO_Header_OPTIONS,
  Customer
} from "../../providers/SalonAppUser-Interface";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { TipsDetailModal } from "../../providers/TipsDetailModal/TipsDetailModal";
import { BeautyTipsModal } from "../../providers/BeautyTipsModal/BeautyTipsModal";
import { SqliteDbProvider } from "../../providers/sqlite-db/sqlite-db";
import { SalonServicesModal } from "../../providers/salon-services-modal/salon-services-modal";

import { config } from "../../providers/config/config";
import { GoogleAnalytics } from "@ionic-native/google-analytics";
import { CustomerModal } from "../../providers/CustomerModal/CustomerModal";
import { BeautyTipsViewPage } from "../beauty-tips-view/beauty-tips-view";
import { Content } from "ionic-angular";
import { AppMessages } from "../../providers/AppMessages/AppMessages";
import { SocialSharing } from "@ionic-native/social-sharing";


import { MenuController } from "ionic-angular";
import { SalonDetailsPage } from "../salon-details/salon-details";
import {
  Services,
  Category,
  Tech_appointments,
  SALON_CARD_OPTIONS,
  OFFERS,
  SalonServices,
  SalonHour
} from "../../providers/SalonAppUser-Interface";

@IonicPage()
@Component({
  selector: "page-beauty-tips-detail",
  templateUrl: "beauty-tips-detail.html"
})
export class BeautyTipsDetailPage {
  @ViewChild(Content) content: Content;

  public myCustomer: Customer;

  bt_short_description: string = "";
  bt_description_urdu: string = "";
  bt_title_urdu: string = "";
  bt_description = "";
  bt_title = "";
  twoBeautyTips = [];
  public beautyTips: BEAUTY_TIPS[] = [];

  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };

  public ALLOFFERS: OFFERS[];
  public beautyTipsSalon = [];
  public SalonsServices: SalonServices[];
  public SalonHoursAll: SalonHour[];

  public categoryImageUrl = config.BeautyTipsImageURL;
  public selectedBeautyTipsCategory: TIPS_DETAIL;
  public selectedBeautyTip: TIPS_DETAIL;
  public screenWidth = 0;
  public numberOfChar = 0;
  public beautyTipsDetail: TIPS_DETAIL[] = [];
  public beautyTipsDetailUrdu: TIPS_DETAIL[] = [];
  public beautyTipsDetailEnglish: TIPS_DETAIL[] = [];
  public GenderBasedTrends: string;
  public shouldShowPostsInEnglish = true;
  public selectedTipDetail: TIPS_DETAIL;
  public noStyleFound = false;
  public shouldShowMiniHeader = false;
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS;
  public slider: HTMLElement;
  public unregisterBackButtonAction: any;
  isBtnNewTapped = true;
  disabledBoth = false;
  public bt_sal_id = "1";
  // public shouldDisableEnglishTip = false
  // public shouldDisableUrduTip = false
  constructor(
    public sqlProvider: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    public elementRef: ElementRef,
    public renderer: Renderer,
    private menu: MenuController,
    private socialSharing: SocialSharing,
    public loadingController: LoadingController,
    private zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public ga: GoogleAnalytics,
    public customerModal: CustomerModal,
    public serviceManager: GlobalProvider,
    public tipsDetailModal: TipsDetailModal,
    public beautyTipsModal: BeautyTipsModal,
    public salonServiceModal: SalonServicesModal,
    public database: SqliteDbProvider,
    private nativePageTransitions: NativePageTransitions,
    public events: Events,

  ) {
    this.htmlOnClick();

    this.nopsoHeaderOptions = {
      miniHeaderHeight: "50pt",
      calledFrom: config.BeautyTipsDetailPage
    };
    this.selectedBeautyTipsCategory = navParams.get("beautyTip");
    this.selectedBeautyTip = navParams.get("beautyTip");

    //this.selectedBeautyTip.bt_description="<a (click)='tipsHrefEvent($event)'   >detal about salon</a>"

    console.log("description", this.selectedBeautyTip.bt_description);

    console.log("");
    this.getTwoBeautyTipsFromDB();
    this.getMoreBeautyTips();
    if (this.selectedBeautyTip) {
      this.bt_short_description = this.selectedBeautyTip.bt_short_description;
      this.bt_description_urdu = this.selectedBeautyTip.bt_description_urdu;
      this.bt_title_urdu = this.selectedBeautyTip.bt_title_urdu;
      this.bt_description = this.selectedBeautyTip.bt_description;
      this.bt_title = this.selectedBeautyTip.bt_title;
      console.log("HereSelectedTips", JSON.stringify(this.selectedBeautyTip));
    }
    platform.ready().then(readySource => {
      this.screenWidth = platform.width();
      this.numberOfChar = platform.width() / 7.8; // one character takes
    });

    let isBTFetched = this.serviceManager.getFromLocalStorage(
      this.serviceManager.TIPS_DETAILS_LAST_FETCHED_DATE
    );
    if (isBTFetched) {
      this.tipsDetailModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.tipsDetailModal
            .getBeautyTipsDetail(this.selectedBeautyTipsCategory.btc_id)
            .then(beautyTips => {
              let _beautyTipsDetail: TIPS_DETAIL[] = beautyTips;
              _beautyTipsDetail.forEach(beautyTipPost => {
                if (
                  beautyTipPost.bt_description &&
                  beautyTipPost.bt_description.trim().length > 0
                ) {
                  this.beautyTipsDetailEnglish.push(beautyTipPost);
                }
                if (
                  beautyTipPost.bt_description_urdu &&
                  beautyTipPost.bt_description_urdu.trim().length > 0
                ) {
                  this.beautyTipsDetailUrdu.push(beautyTipPost);
                }
              });

              if (
                this.serviceManager.getFromLocalStorage(
                  this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS
                ) === true
              ) {
                this.shouldShowPostsInEnglish = true;
                this.GenderBasedTrends = "English";
                this.slider = document.getElementById("slider_btd");
                this.slider.style.cssFloat = "left";
                this.beautyTipsDetail = this.beautyTipsDetailEnglish;
                !this.beautyTipsDetail || this.beautyTipsDetail.length === 0
                  ? (this.noStyleFound = true)
                  : (this.noStyleFound = false);
              } else if (
                this.serviceManager.getFromLocalStorage(
                  this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS
                ) === false
              ) {
                this.shouldShowPostsInEnglish = false;
                this.GenderBasedTrends = "Urdu";
                this.beautyTipsDetail = this.beautyTipsDetailUrdu;
                this.slider = document.getElementById("slider_btd");
                this.slider.style.cssFloat = "right";

                !this.beautyTipsDetail || this.beautyTipsDetail.length === 0
                  ? (this.noStyleFound = true)
                  : (this.noStyleFound = false);
                this.serviceManager.setInLocalStorage(
                  this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS,
                  false
                );
              } else {
                this.shouldShowPostsInEnglish = true;
                this.GenderBasedTrends = "English";
                this.slider = document.getElementById("slider_btd");
                this.slider.style.cssFloat = "left";
                this.beautyTipsDetail = this.beautyTipsDetailEnglish;
                !this.beautyTipsDetail || this.beautyTipsDetail.length === 0
                  ? (this.noStyleFound = true)
                  : (this.noStyleFound = false);
                this.serviceManager.setInLocalStorage(
                  this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS,
                  true
                );
              }
            });
        }
      });
    } else {
      this.getTipsDetailFromServer();
    }
  }
  //to on hreff click to navigate to salon or product
  htmlOnClick() {
    this.renderer.listen(this.elementRef.nativeElement, "click", event => {
      var rc: boolean = true;
      if (event.target.nodeName == "A") {
        rc = this.processClickOnATag(event.target.href);
      }
      return rc;
    });
  }

  processClickOnATag(href: string): boolean {
    var rc: boolean = true;
    var pos: number;

    if ((pos = href.indexOf("salon_")) !== -1) {
      rc = this.processClickOnSalonPageLink(href.substring(pos));
    } else if ((pos = href.indexOf("product_")) !== -1) {
      rc = this.processClickOnProductPage(href.substring(pos));
    }
    return rc;
  }
  private processClickOnProductPage(pageRef: string): boolean {
    var rc: boolean = true;
    var refParts = pageRef.split("_");
    let p_id = refParts[1];

    this.selectedProductID = Number(p_id);
    this.getProductProductDetail();
    if (refParts[1] === "myInternalPage") {
      // this.navCtrl.push(MySpecialPage, { pageId:  refParts[2]});
      rc = false;
    }
    return rc;
  }
  private processClickOnSalonPageLink(pageRef: string): boolean {
    var rc: boolean = true;
    var refParts = pageRef.split("_");
    let sal_id = refParts[1];
    this.bt_sal_id = sal_id;
    // Check this is actually an special page.
    if (sal_id) {
      this.getSalonFromLocalDb();
      // this.navCtrl.push(SalonDetailsPage, {
      //   isServerSearch: false,
      //   sal_id: sal_id,
      //   distance: 0,

      // });
    }
    if (refParts[1] === "myInternalPage") {
      // this.navCtrl.push(MySpecialPage, { pageId:  refParts[2]});
      rc = false;
    }
    return rc;
  }
  //end hreff events

  btnShareTapped(beautyTips) {
    let loading = this.loadingController.create({ content: "Please Wait..." });
    loading.present();
    timer(1500).subscribe(() => {
      loading.dismiss();
    });
    let productSshareURL =
      config.shareBeautyTipsURL +
      beautyTips.bt_id +
      "/" +
      beautyTips.bt_title.replace(/ /g, "-");
    this.socialSharing
      .share(
        "This is " +
          this.myCustomer.cust_name +
          ".  I am using BeautyApp.pk on my phone and seen something interesting for you.  Please check the attached.\n" +
          productSshareURL,
        "image",
        beautyTips.bt_imageUrl
      )
      .then(() => {})
      .catch(er => {});
  }
  //custom back button for android
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.customHandleBackButton();
      },
      10
    );
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage);
      }
    }
  }
  //end custom back button for android

  scrollToTop() {
    this.content.scrollToTop();
    let options: NativeTransitionOptions = {
      direction: "top",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 150,
      androiddelay: 0,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    this.nativePageTransitions.fade(options);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad BeautyTipsDetailPage");
  }
  ionViewWillEnter() {
    // this.getBeautyTipsSalon();
    //  this.postsInUrduTapepd();
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(customer => {
          // this.googleAnalytics()
          if (customer) {
            this.myCustomer = customer;
            this.googleAnalytics(
              this.myCustomer.cust_id,
              this.myCustomer.cust_name
            );
          } else {
            let publicUserId = this.serviceManager.getFromLocalStorage(
              this.serviceManager.PUBLIC_USER_ID
            );
            if (publicUserId) {
              this.googleAnalytics(publicUserId, "public user");
            }
          }
        });
      }
    });
  }
  tagClickek() {
    event.stopPropagation();
  }

  beautyTipClicked(event, beautyTip, index) {
    event.stopPropagation();
    this.scrollToTop();
    this.navCtrl.setRoot(BeautyTipsViewPage, {
      beautyTip: beautyTip,
      isFromBeautyTipsDetailPage: true
    });
  }
  categoryClicked(event, beautyTip, index) {
    event.stopPropagation();
    this.scrollToTop();
    this.selectedBeautyTip = beautyTip;
    this.getTwoBeautyTipsFromDB();
  }
  ionViewDidEnter() {
    // this.getProductProductDetail(this.selectedProductID)
    this.initializeBackButtonCustomHandler();
    let abc = false;
    this.events.subscribe("canShowMiniHeader", isHidden => {
      this.zone.run(() => {
        this.shouldShowMiniHeader = isHidden;
      });
    });
  }
  btnViewMoreTapped() {}

  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage);
    }
  }

  getTipsDetailFromServer() {
    const params = {
      service: btoa("get_beauty_tips")
    };

    this.serviceManager.showProgress();
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          this.serviceManager.stopProgress();
          this.beautyTipsDetail = res.TipCategories;
          this.tipsDetailModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.tipsDetailModal
                .InsertInToTipsDetailTable(this.beautyTipsDetail)
                .then(done => {
                  this.serviceManager.setInLocalStorage(
                    this.serviceManager.TIPS_DETAILS_LAST_FETCHED_DATE,
                    true
                  );
                });
            }
          });
        },
        error => {
          this.serviceManager.stopProgress();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );

          console.log("something went wrong", error);
        }
      );
  }

  postsInEnglishTapepd() {
    if (
      !this.selectedBeautyTip ||
      this.selectedBeautyTip.bt_title.trim().length === 0
    ) {
      this.serviceManager.makeToastOnSuccess(
        AppMessages.msgUrduArticleAvailability
      );
      return;
    }
    this.slider = document.getElementById("slider_btd");
    this.slider.style.cssFloat = "left";
    if (this.shouldShowPostsInEnglish) {
      return;
    }
    this.shouldShowPostsInEnglish = true;
    this.beautyTipsDetail = this.beautyTipsDetailEnglish;
    !this.beautyTipsDetail || this.beautyTipsDetail.length === 0
      ? (this.noStyleFound = true)
      : (this.noStyleFound = false);
    this.serviceManager.setInLocalStorage(
      this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS,
      true
    );
  }
  postsInUrduTapepd() {
    if (
      !this.selectedBeautyTip ||
      this.selectedBeautyTip.bt_title_urdu.trim().length === 0
    ) {
      this.serviceManager.makeToastOnSuccess(
        AppMessages.msgEnglishArticleAvailability
      );
      return;
    }
    this.slider = document.getElementById("slider_btd");
    this.slider.style.cssFloat = "right";
    if (!this.shouldShowPostsInEnglish) {
      return;
    }
    this.shouldShowPostsInEnglish = false;
    this.beautyTipsDetail = this.beautyTipsDetailUrdu;
    !this.beautyTipsDetail || this.beautyTipsDetail.length === 0
      ? (this.noStyleFound = true)
      : (this.noStyleFound = false);
    this.serviceManager.setInLocalStorage(
      this.serviceManager.SHOULD_SHOW_ENGLISH_POSTS,
      false
    );
  }
  getTwoBeautyTipsFromDB() {
    this.tipsDetailModal
      .getTwoBeautyTips(
        this.selectedBeautyTip.btc_id,
        this.selectedBeautyTip.bt_id
      )
      .then(beautyTip => {
        this.twoBeautyTips = beautyTip;
        console.log("TwoTips", JSON.stringify(this.twoBeautyTips));
      });
  }
  getMoreBeautyTips() {
    this.tipsDetailModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.beautyTipsModal.getBeautyTips("").then(beautyTips => {
          this.beautyTips = beautyTips;
        });
      }
    });
  }

  googleAnalytics(cust_id, custName) {
    this.ga
      .startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
        console.log("Google analytics is ready now");
        this.ga.trackView(
          custName + "visited Prdocut screen for product_cateogry " + " this."
        );
        this.ga.trackEvent(
          "cat: ionViewWill Enter",
          "act: loads everytime",
          "lab: general label",
          200,
          true
        );
        this.ga.trackTiming(
          "cat: tracking timing",
          600000,
          "variable: not sure what will go here",
          "label: and the same"
        );
        this.ga.debugMode();
        this.ga.setAllowIDFACollection(true);
        this.ga.setUserId(cust_id);
      })
      .catch(e => {
        console.log("Error starting GoogleAnalytics", e);
      });
  }

  //get salon for beautyTips if not exist already

  getBeautyTipsSalon() {
    //this
    let loading = this.loadingController.create({
      content: "Fetching Doctors, Please Wait..."
    });
    loading.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      service: btoa("search_salons"),
      sal_id: btoa(this.bt_sal_id)
    };
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        response => {
          loading.dismissAll();
          if (response.salons) {
            this.beautyTipsSalon = response.salons;
          }
          console.log("beautyTipSalon", JSON.stringify(this.beautyTipsSalon));
          this.handleBeautyTipsSalonResponse(this.beautyTipsSalon);
        },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
  }
  handleBeautyTipsSalonResponse(response) {
    if (this.beautyTipsSalon && this.beautyTipsSalon.length > 0) {
      this.getAndSetSalonOffer(this.beautyTipsSalon);
    }
    let salonIds = this.getSalonIds(this.beautyTipsSalon);
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal
          .deleteMultipleSalonServiceCatogries(salonIds)
          .then(res => {
            this.salonServicesModal.saveSalonServicesCategoriesIntoDB(
              this.beautyTipsSalon
            );
          });
        this.salonServicesModal
          .deleteMultipleSalonServices(salonIds)
          .then(res => {
            this.insertSalonServices(salonIds);
            this.insertSalonDetails(salonIds);
          });
      }
    });
  }
  insertSalonDetails(salonIds) {
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.deleteMultipleSalon(salonIds).then(res => {
          if (
            this.beautyTipsSalon != undefined &&
            this.beautyTipsSalon != null
          ) {
            this.sqlProvider.insert_salon_table(this.beautyTipsSalon);
            console.log("SalHoursCalculationStart......");
            this.calCulateSalHours(salonIds);
            console.log("EndSalHoursCalculationStart......");
            this.pushToSalonDetailPage();
          } else {
          }
        });
      }
    });
  }
  calCulateSalHours(inactive_salons) {
    this.SalonHoursAll = [];
    this.beautyTipsSalon.forEach(salon => {
      let sal_Id = salon.sal_id;
      let sal_hours = salon.sal_hours1;
      // console.log("SalHoursTo",JSON.stringify(sal_hours));
      let salonHours = {};
      let salonHoursKeys = [];
      salonHoursKeys = Object.keys(sal_hours);
      salonHours = sal_hours;
      let temp = {};
      salonHoursKeys.forEach(element => {
        let split = sal_hours[element].split("&");
        if (split[0] == split[1]) {
          temp[element] = "Off";
        } else {
          temp[element] =
            this.convert24Hrto12Hr(split[0]) +
            " - " +
            this.convert24Hrto12Hr(split[1]);
        }
      });
      salonHours = temp;
      salonHoursKeys.forEach(dayKey => {
        // console.log("KeyHere", JSON.stringify(dayKey))
        // console.log("KeyValue", JSON.stringify(salonHours[dayKey]))
        let salonHoursObject = {
          sal_id: Number(sal_Id),
          sal_hours: salonHours[dayKey],
          sal_day: dayKey
        };
        // console.log("ItemHours",JSON.stringify(salonHoursObject))
        this.SalonHoursAll.push(salonHoursObject);
      });
    });
    //insert sal Hours to DB
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (inactive_salons !== undefined && inactive_salons !== null) {
          this.sqlProvider
            .deleteSalHours(inactive_salons)
            .then(isDataInserted => {});
        }
        if (
          this.SalonHoursAll !== undefined &&
          this.SalonHoursAll !== null &&
          this.SalonHoursAll.length > 0
        ) {
          this.sqlProvider
            .saveIntoSalHoursTable(this.SalonHoursAll)
            .then(isDataInserted => {
              console.log("InsertedAllSalHours", isDataInserted);
            });
        }
      }
    });
  }
  getAndSetSalonOffer(salons) {
    let so_ids = "";
    this.ALLOFFERS = [];
    salons.forEach(element => {
      if (element.offers !== null) {
        for (let index = 0; index < element.offers.length; index++) {
          const item = element.offers[index];
          // so_ids += item['so_id'] + ','
          this.ALLOFFERS.push(item);
        }
      }
    });
    // so_ids = so_ids.slice(0, so_ids.length - 1)//after loop
    this.saveSalonOffers(this.ALLOFFERS);
  }
  saveSalonOffers(ALLOFFERS) {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (ALLOFFERS != undefined && ALLOFFERS != null) {
          this.salonServicesModal.insert_salon_offer(ALLOFFERS);
        }
      }
    });
  }
  insertSalonServices(salonIds) {
    if (!this.beautyTipsSalon) {
      return;
    }
    this.SalonsServices = [];
    this.beautyTipsSalon.forEach(salon => {
      this.SalonsServices = this.SalonsServices.concat(salon.services);
    });
    if (this.SalonsServices && this.SalonsServices.length > 0) {
      this.serviceManager.setInLocalStorage(
        this.serviceManager.NEAREST_SALON_SERVICES,
        this.SalonsServices
      );
    }
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.saveSalonServicesIntoDB(this.beautyTipsSalon);
      }
    });
  }
  getSalonIds(response) {
    let salonIds = "";
    let i = 0;
    response.forEach(element => {
      salonIds += element.sal_id + ",";
    });
    salonIds = salonIds.slice(0, salonIds.length - 1);
    return salonIds;
  }
  roundRatingUpToOneDecimal(ratingValue) {
    return Math.round(ratingValue).toFixed(1);
  }
  convert24Hrto12Hr(time) {
    if (time === undefined || time === null) {
      this.navCtrl.pop();
      return;
    }
    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
      // If time format correct
      time = time.slice(1); // Remove full string match value
      time[5] = +time[0] < 12 ? " AM" : " PM"; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    time[0] = this.padToTwo(time[0]);
    return time.join(""); // return adjusted time or original string
  }
  padToTwo(number) {
    if (number <= 9) {
      number = ("0" + number).slice(-2);
    }
    return number;
  }
  //end get salon for beauty tips
  pushToSalonDetailPage() {
    let offerType;
    let promotions = this.getOfferBySalId(this.bt_sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }
    let distance = 0;
    this.navCtrl.push(SalonDetailsPage, {
      isServerSearch: true,
      selectedSalon: this.beautyTipsSalon[0],
      offerType: offerType,
      distance: distance
    });
  }
  getOfferBySalId(Id: string) {
    let found = null;
    if (this.ALLOFFERS) {
      found = this.ALLOFFERS.find(function(element) {
        //console.log('lololol',JSON.stringify(element))
        return element.sal_id == Id;
      });
    } else {
    }

    return found;
  }
  getSalonFromLocalDb() {
    this.sqlProvider.getAllSalonsForTips(this.bt_sal_id).then(myAllSalons => {
      // this.bt_sal_id='1'
      this.beautyTipsSalon = myAllSalons;
      if (this.beautyTipsSalon && this.beautyTipsSalon.length > 0) {
        this.navCtrl.push(SalonDetailsPage, {
          isServerSearch: true,
          selectedSalon: this.beautyTipsSalon[0],
          distance: 0
        });
      } else {
        this.getBeautyTipsSalon();
      }
    });
  }
  /**
   * getting product detail from tips
   *
   *   */
  public selectedProductID = -1;
  getProductProductDetail() {
    // this.productModal.getDatabaseState().subscribe(ready => {
    //   if (ready) {
    //     this.productModal
    //       .getProductDetail(this.selectedProductID)
    //       .then(product => {
    //         if (product) {
    //           this.nativePageTransitions.slide(this.options);
    //           this.navCtrl.push(ProductdetailPage, {
    //             product: product
    //           });
    //         } else {
    //           this.getProdcutFromServer();
    //         }
    //       });
    //   }
    // });
  }
  // getProdcutFromServer() {
  //   const params = {
  //     service: btoa("get_single_product"),
  //     p_id: btoa(this.selectedProductID.toString())
  //   };

  //   this.serviceManager
  //     .getData(params)
  //     .retryWhen(err => {
  //       return err
  //         .scan(retryCount => {
  //           retryCount += 1;
  //           if (retryCount < 3) {
  //             return retryCount;
  //           } else {
  //             throw err;
  //           }
  //         }, 0)
  //         .delay(1000);
  //     })
  //     .subscribe(
  //       res => {
  //         if (res.products) {
  //           this.nativePageTransitions.slide(this.options);
  //           this.navCtrl.push(ProductdetailPage, {
  //             product: res.products
  //           });
  //         }
  //       },
  //       error => {
  //         this.serviceManager.makeToastOnFailure(
  //           AppMessages.msgNoInternetAVailable
  //         );

  //         console.log("something went wrong", error);
  //       }
  //     );
  // }
}
