import { GlobalProvider } from "./../../providers/global/global";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Platform,
  ModalController,
  Events
} from "ionic-angular";
import { MenuController } from "ionic-angular";

import { AddAppointmentPage } from "../add-appointment/add-appointment";

import { SalonReviewsPage } from "../salon-reviews/salon-reviews";
import * as _ from "underscore";

import { Navbar } from "ionic-angular";
import "rxjs/add/operator/retrywhen";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/scan";
import { config } from "../../providers/config/config";
import { GoogleAnalytics } from "../../../node_modules/@ionic-native/google-analytics";
import {
  Customer,
  Services,
  Category,
  Salon,
  Tech_appointments,
  SALON_CARD_OPTIONS,
  OFFERS,
  SalonServices,
  SalonHour
} from "../../providers/SalonAppUser-Interface";
import { CustomerModal } from "../../providers/CustomerModal/CustomerModal";
import { MainHomePage } from "../main-home/main-home";
import { SlotsViewPage } from "../slots-view/slots-view";

import { LoginSignUpPage } from "../login-sign-up/login-sign-up";

import { SalonServicesModal } from "../../providers/salon-services-modal/salon-services-modal";

import { SqliteDbProvider } from "../../providers/sqlite-db/sqlite-db";
import { GlobalServiceProvider } from "../../providers/global-service/global-service";
import { AppMessages } from "../../providers/AppMessages/AppMessages";
import { Network } from "@ionic-native/network";

@IonicPage()
@Component({
  selector: "page-salon-details",
  templateUrl: "salon-details.html"
})
export class SalonDetailsPage {
  @ViewChild(Navbar) navBar: Navbar;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };
  public ALLOFFERS: OFFERS[];
  public beautyTipsSalon = [];
  public SalonsServices: SalonServices[];
  public SalonHoursAll: SalonHour[];
  public allServices: any[];
  public Categories: Category[];
  public selectedServicesObjects: Services[];
  // public Categories = Array();
  servicesCountByCategory = [];

  public SalonHoursFromDb: SalonHour[];
  rate1: any;
  rate2: any;
  rate3: any;
  isenabledConfirmbutton: boolean = false;
  rate4: any;
  servicesSortedByCat: any[];
  public daysArray = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
  public salonHoursKeys = [];
  public salonHours = {};
  public salonImagesURL = config.salonImgUrl;
  public techImagesURL = config.TechImageURL;
  public allTechs = [];
  public totalTechs: number;
  public selectedSalon: Salon;
  public salonForComponent: Salon;
  public pinRelatedServices: Services[];
  public unregisterBackButtonAction: any;
  public salonWorkImages = [];
  public defaultSalonPicName = "default.png";
  public isSalonActive = 0;
  public salonDistance = 0;
  public myCustomer: Customer;
  public rescheduleAppointment: Tech_appointments;
  public offerType: any;
  public salonCardOption: SALON_CARD_OPTIONS;
  public sal_id: number;
  public currencySymbol = "";
  constructor(
    public sqlProvider: SqliteDbProvider,
    public loadingController: LoadingController,
    public salonServicesModal: SalonServicesModal,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private nativePageTransitions: NativePageTransitions,
    public platform: Platform,
    public serviceManager: GlobalProvider,
    private ga: GoogleAnalytics,
    private salonServiceModal: SalonServicesModal,
    private customerModal: CustomerModal,
    public events: Events,
    public gsp: GlobalServiceProvider,
    public network: Network,
    private menu: MenuController
  ) {
    this.selectedServicesObjects = [];
    // this.selectedSalon = this.navParams.get('selectedSalon')
    this.sal_id = this.navParams.get("sal_id");
    let isServerSearch = this.navParams.get("isServerSearch");
    //this.salonDistance = this.navParams.get('distance')
    // this.selectedSalon.distance = this.salonDistance

    this.rescheduleAppointment = this.navParams.get("rescheduleAppointment");
    this.salonForComponent = this.navParams.get("selectedSalon");
    if (this.salonForComponent.pinRelatedServices) {
      this.pinRelatedServices = this.salonForComponent.pinRelatedServices;
    }

    let Salons: Salon[] = [];
    Salons.push(this.salonForComponent);
    let sal_rating = Number(this.salonForComponent.sal_rating);
    var _CardHeight = "315pt";
    if (sal_rating > 0) {
      _CardHeight = "340pt";
    }

    this.salonCardOption = {
      // CardHeight: '57vh',
      CardHeight: _CardHeight,
      calledFrom: config.SalonDetailPage,
      ShouldShowSalonImage: true,
      shouldScroll: false,
      shouldNotLoadSalonAgain: true,
      salon: Salons
    };
    let sal_id = null;
    let tempSalon = this.navParams.get("selectedSalon");
    if (tempSalon) sal_id = tempSalon.sal_id;
    this.getSalonDetailFromDb(sal_id);
    // this.getDataFromServer()
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.customHandleBackButton();
      },
      10
    );
  }
  private customHandleBackButton(): void {
    // if (this.navCtrl.canGoBack()) {
    //   this.navCtrl.pop();
    // } else {
    //   this.navCtrl.setRoot(MainHomePage)
    // }
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage);
      }
    }
  }
  ionViewWillEnter() {
    // alert('here')
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            this.myCustomer = customer;
            this.googleAnalytics();
          },
          error => { }
        );
      }
    });
  }
  ionViewDidLoad() {
    this.currencySymbol = this.gsp.currencySymbol;

    if (!this.currencySymbol || this.currencySymbol.length === 0) {
      let _appConfig = this.serviceManager.getFromLocalStorage("isAppConfigs");
      if (_appConfig) {
        if (_appConfig && _appConfig.currency) {
          this.gsp.currencySymbol = _appConfig.currency;
          this.gsp.distance_unit = _appConfig.distance_unit;
        }
      }
    }
  }

  ionViewCanEnter() {
    let sal_id = "";
    let isFromBookAppointment = this.navParams.get("isFromBookAppointment");
    this.offerType = this.navParams.get("offerType");
    if (isFromBookAppointment) {
      sal_id = this.navParams.get("sal_id");
    } else {
      let tempSalon = this.navParams.get("selectedSalon");
      if (tempSalon) sal_id = tempSalon.sal_id;
    }
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider
          .getSalonTechnicianLatUpdate(sal_id)
          .then(lastupdateTime => {
            this.getSalonTechnician(lastupdateTime, sal_id);
          });
      }
    });
  }
  goToReviewsPage(salId) {
    this.nativePageTransitions.slide(this.options);
    this.navCtrl.push(SalonReviewsPage, {
      salId: salId
    });
  }
  convert24Hrto12Hr(time) {
    if (time === undefined || time === null) {
      this.navCtrl.pop();
      return;
    }
    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
      // If time format correct
      time = time.slice(1); // Remove full string match value
      time[5] = +time[0] < 12 ? " AM" : " PM"; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }

    time[0] = this.padToTwo(time[0]);

    return time.join(""); // return adjusted time or original string
  }

  padToTwo(number) {
    if (number <= 9) {
      number = ("0" + number).slice(-2);
    }
    return number;
  }

  // getAvailability(tech) {

  //   let salOffDays = this.selectedSalon["sal_weekly_offs"];
  //   let techOffDays = tech.tech_weekly_offs;

  //   if (salOffDays == techOffDays && salOffDays == "") {
  //     return "Full Week";
  //   } else if (salOffDays == techOffDays) {

  //     let splitted = salOffDays.split(',');
  //     var tempArray = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  //     for (var i = splitted.length - 1; i >= 0; i--) {
  //       tempArray.splice(parseInt(splitted[i]) - 1, 1);
  //     }

  //     return tempArray;

  //   } else {

  //     var splittedSalDays = salOffDays.split(',');
  //     var splittedTechDays = techOffDays.split(',');
  //     var unionOfSalAndTech = _.union(splittedSalDays, splittedTechDays);

  //     if (splittedTechDays == "") {
  //       unionOfSalAndTech = splittedSalDays;
  //     } else if (splittedSalDays == "") {
  //       unionOfSalAndTech = splittedTechDays;
  //     }

  //     var tempArray2 = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  //     for (var j = unionOfSalAndTech.length - 1; j >= 0; j--) {
  //       tempArray2.splice(parseInt(unionOfSalAndTech[j]) - 1, 1);
  //     }
  //     // console.log('in different seciton....');
  //     return tempArray2;
  //   }
  // }

  gotToAddAppointment() {
    if (!this.myCustomer && this.myCustomer === undefined) {
      //if customer not exist
      this.nativePageTransitions.slide(this.options);
      this.navCtrl.push(LoginSignUpPage, {
        comingFrom: "SalonDetailsPage"
      });
    } else {
      //if customer exit go to next

      if (this.selectedServicesObjects.length === 0) {
        this.serviceManager.makeToastOnFailure(
          AppMessages.msgSelectSalonServices
        );
        return;
      }
      if (!this.totalTechs || this.totalTechs === 0) {
        this.serviceManager.makeToastOnFailure(
          AppMessages.msgUnavailabilityOfTechnician
        );
        return;
      }

      if (this.totalTechs && this.totalTechs === 1) {
        //if (this.selectedSalon.techs && this.selectedSalon.techs.length === 1) {

        this.salonServiceModal.getDatabaseState().subscribe(ready => {
          if (ready) {
            this.salonServiceModal
              .getSalonOffers(this.selectedSalon.sal_id)
              .then(offersObjects => {
                this.nativePageTransitions.slide(this.options);
                this.navCtrl.push(SlotsViewPage, {
                  selectedSalon: this.selectedSalon,
                  salonForComponent: this.salonForComponent,
                  selectedServicesObjects: this.selectedServicesObjects,
                  selectedTech: this.allTechs[0],
                  offersObjects: offersObjects,
                  rescheduleAppointment: this.rescheduleAppointment
                });
              });
          }
        });
      } else if (this.totalTechs && this.totalTechs > 1) {
        //} else if (this.selectedSalon.techs && this.selectedSalon.techs.length > 1) {
        this.nativePageTransitions.slide(this.options);
        if (this.rescheduleAppointment) {
          this.navCtrl.push(AddAppointmentPage, {
            selectedSalon: this.selectedSalon,
            salonForComponent: this.salonForComponent,
            selectedServicesObjects: this.selectedServicesObjects,
            rescheduleAppointment: this.rescheduleAppointment
          });
        } else {
          this.navCtrl.push(AddAppointmentPage, {
            selectedSalon: this.selectedSalon,
            salonForComponent: this.salonForComponent,
            selectedServicesObjects: this.selectedServicesObjects
          });
        }
      }
    }
  }

  goBack() {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options);

    this.navCtrl.pop();
  }

  sortServices() {
    this.servicesSortedByCat = [];
    var BreakException = {};

    // let catFromCategory: string = element['ssc_name'];
    this.allServices.forEach(element => {
      let catFromService: string = element["sser_featured"];
      if (catFromService.trim() === "1") {
        let elementNew = element;
        // console.log('serivce element pushed', elementNew);
        this.servicesSortedByCat.push(elementNew);
      }
    });
    if (this.servicesSortedByCat.length < 4) {
      this.allServices.forEach(element => {
        let catFromService: string = element["sser_featured"];
        if (catFromService.trim() === "0") {
          let elementNew = element;
          // console.log('serivce element pushed', elementNew);
          this.servicesSortedByCat.push(elementNew);
        }
        if (this.servicesSortedByCat.length === 4) {
          return false;
        } else {
          return true;
        }
      });
    }

    let i = 0;
    this.servicesSortedByCat.forEach(element => {
      let catFromService: string = element["sser_featured"];
      let sser_name: string = element["sser_name"];

      if (i == 0) {
        this.rate1 = element;
      }
      if (i == 1) {
        this.rate2 = element;
      }
      if (i == 2) {
        this.rate3 = element;
      }
      if (i == 3) {
        this.rate4 = element;
      }
      i++;
    });
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  getSalonNameFirstCharacte(salonName: string): string {
    if (!salonName) {
      return "";
    } else {
      if (salonName.trim().length === 0) {
        return salonName;
      }
      var shortSalonName = "";
      let charCount = 0;
      let SplitSalonNames = salonName.split(" ");
      SplitSalonNames.forEach(element => {
        let salName: string = element;

        if (charCount < 4 && salName.trim().length > 0) {
          if (
            charCount === 3 &&
            salName.length === 1 &&
            SplitSalonNames.length > 3
          ) {
          } else {
            shortSalonName += salName.slice(0, 1);
            charCount += 1;
          }
        }
      });

      return shortSalonName.toLocaleUpperCase();
    }
  }

  getReviews(review) {
    if (review == "0") {
      return "Not Enough Ratings";
    } else {
      return null;
    }

    //  else{
    //   return  review+" Ratings";
    //  }
  }
  getSalonHoursFromDB(sal_ids) {
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.getSalonHoursBySalId(sal_ids).then(res => {
          this.salonHours = res;
        });
      }
    });
  }

  getSalonDetailFromDb(salId) {
    this.getSalonHoursFromDB(salId);
    this.salonServiceModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServiceModal.getSalonBySalId(salId).then(salons => {
          this.salonServiceModal.getAllSalonServiceBySal(salId).then(allServices => {
            this.salonServiceModal.getAllSalonServiceCategoriesBySalId(salId).then(mainCategories => {
              mainCategories.forEach(category => {
                let ssc_id = category.ssc_id;
                let serviceObj = [];
                allServices.forEach(service => {
                  if (ssc_id == service.ssc_id) {
                    serviceObj.push(service);
                  }
                });
                return (category.services = serviceObj);
              });
              this.getAllDataFromLocalDb(mainCategories, salons[0]);
            });
          });

          //end salon block
        });
      } //if ready block
    });
  }

  getAllDataFromLocalDb(Categories, selectedSalon) {
    console.log("SelectedSalon", JSON.stringify(selectedSalon));
    this.selectedSalon = selectedSalon;
    this.events.publish("gotSalonDetail", this.selectedSalon);

    //distance is not returning from server in lst_salon
    this.isSalonActive = Number(this.selectedSalon["sal_status"]);
    this.Categories = Categories;

    if (this.pinRelatedServices && this.pinRelatedServices.length > 0) {
      this.Categories.forEach((category, index) => {
        let numberOfServicesSelected = 0;
        let catServices: Services[] = category.services;
        this.pinRelatedServices.forEach(pinRelatedSer => {
          let serviceFound = false;
          catServices.forEach(service => {
            if (
              Number(service.sser_id) === Number(pinRelatedSer.sser_id) &&
              !serviceFound
            ) {
              console.log(
                "service.sser_id === pinRelatedSer.sser_id " +
                service.sser_id +
                " === " +
                pinRelatedSer.sser_id
              );
              this.selectedServicesObjects.push(service);
              serviceFound = true;
              numberOfServicesSelected += 1;
              console.log("serviceFound " + serviceFound);
              this.pinRelatedServices.splice(
                this.pinRelatedServices.indexOf(pinRelatedSer),
                1
              );
              return (service.checked = true);
            } else {
              serviceFound = false;
              return (service.checked = false);
            }
          });
        });
        category.numberOfServicesSelected = numberOfServicesSelected;
        console.log(JSON.stringify(category));
        return (category.expanded = false);
      });
    } else if (this.rescheduleAppointment) {
      let appoServices = this.rescheduleAppointment.app_services;
      let appoSersArray = appoServices.split(",");
      let __selectedServicesObjects = [];
      this.Categories.forEach(category => {
        let numberOfServicesSelected = 0;
        let catServices: Services[] = category.services;
        catServices.forEach(service => {
          let serviceFound = false;
          appoSersArray.some(function (appoSer, index) {
            if (service.sser_name.toLowerCase() === appoSer.toLowerCase()) {
              __selectedServicesObjects.push(service);
              serviceFound = true;
              numberOfServicesSelected += 1;
              return true;
            } else {
              serviceFound = false;
              return false;
            }
          });
          // appoSersArray.forEach(appoSer => {
          //   console.log('service.sser_name : appoSer '+ service.sser_name + ' : '+ appoSer);

          //   if (service.sser_name.toLowerCase() === appoSer.toLowerCase()) {
          //     this.selectedServicesObjects.push(service)
          //     serviceFound = true
          //     numberOfServicesSelected += 1
          //   } else {
          //     serviceFound = false
          //   }

          // });
          console.log("serviceFound " + serviceFound);

          if (serviceFound) {
            return (service.checked = true);
          } else {
            return (service.checked = false);
          }
        });
        category.numberOfServicesSelected = numberOfServicesSelected;
        return (category.expanded = false);
      });
      this.selectedServicesObjects = __selectedServicesObjects;
    } else {
      this.Categories.forEach((category, index) => {
        let catServices: Services[] = category.services;
        catServices.forEach((service, serviceIndex) => {
          if (index === 0) {
            this.ServiceTapped(index, serviceIndex)
            return (service.checked = true);
          } else {
            return (service.checked = false);
          }
          // index === 0 ? (service.checked = true) : (service.checked = false);
        });
        category.numberOfServicesSelected = 0;

        return (category.expanded = false);
      });
    }

    // this.salonHoursKeys = Object.keys(this.selectedSalon.sal_hours1[0]);
    // this.salonHours = this.selectedSalon.sal_hours1;
    // let temp = {};
    // this.salonHoursKeys.forEach(element => {
    //   let split = this.selectedSalon.sal_hours1[element].split('&');
    //   if (split[0] == split[1]) {
    //     temp[element] = "Off";
    //   } else {
    //     temp[element] = this.convert24Hrto12Hr(split[0]) + " - " + this.convert24Hrto12Hr(split[1]);
    //   }
    // });
    // this.salonHours = temp;
  }
  getSalonTechnician(last_fetch, sal_id) {
    let loading = this.loadingController.create({
      content: "Fetching Details, Please Wait..."
    });
    // loading.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa("get_salon_techs"),
      sal_id: btoa(sal_id),
      last_fetched: btoa(last_fetch)
    };
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        response => {
          this.isenabledConfirmbutton = true;
          this.handlResponseTech(response, sal_id);
        },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
        }
      );
  }
  handlResponseTech(response, sal_id) {
    this.allTechs = response.sal_techs;
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider
          .deleteSalTechs(response.update_ids)
          .then(lastupdateTime => {
            if (
              this.allTechs !== undefined &&
              this.allTechs !== null &&
              this.allTechs.length > 0
            ) {
              this.totalTechs = this.allTechs.length;
              this.sqlProvider
                .saveIntoSalTechTable(this.allTechs)
                .then(lastupdateTime => {
                  this.sqlProvider.getDatabaseState().subscribe(ready => {
                    if (ready) {
                      this.sqlProvider
                        .getSalonTotalTech(sal_id)
                        .then(totalTechs => {
                          this.totalTechs = totalTechs;
                        });
                    }
                  });
                });
            } else {
              this.sqlProvider.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.sqlProvider
                    .getSalonTotalTech(sal_id)
                    .then(totalTechs => {
                      this.totalTechs = totalTechs;
                    });
                }
              });
            }
          });
      }
    });
  }

  CategoryExpandableToggle(selectedCategory) {
    // $('.serviceCellUnselected').toggleClass('transform-active');
    // $('.transform').toggleClass('transform-active');

    this.Categories.map(cateogry => {
      if (selectedCategory == cateogry) {
        cateogry.expanded = !cateogry.expanded;
      } else {
        cateogry.expanded = false;
      }

      return cateogry;
    });
  }
  ServiceTapped(catIndex, serIndex) {
    // alert(catIndex + ' , ' + serIndex)

    let selectedCategory = this.Categories[catIndex];

    let servicesArray = selectedCategory.services;
    let selectedService = servicesArray[serIndex];

    selectedService.checked = !selectedService.checked;

    servicesArray[serIndex] = selectedService;

    if (this.selectedServicesObjects.length === 0) {
      selectedCategory.numberOfServicesSelected += 1;
      this.selectedServicesObjects.push(selectedService);
    } else if (selectedService.checked) {
      selectedCategory.numberOfServicesSelected += 1;
      this.selectedServicesObjects.push(selectedService);
    } else if (!selectedService.checked) {
      selectedCategory.numberOfServicesSelected -= 1;
      let serIndex = this.selectedServicesObjects.indexOf(selectedService);
      this.selectedServicesObjects.splice(serIndex, 1);
    }

    this.Categories[catIndex] = selectedCategory;
    this.Categories[catIndex].services = servicesArray;
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options);

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage);
    }
  }

  googleAnalytics() {
    if (this.myCustomer) {
      this.ga.trackView(
        this.myCustomer.cust_name + " visited Salon Detail Page"
      );
      this.ga.trackTiming(
        "cat: tracking timing",
        600000,
        "variable: not sure what will go here",
        "label: and the same"
      );
      this.ga.debugMode();
      this.ga.setAllowIDFACollection(true);
      this.ga.setUserId(this.myCustomer.cust_id);
      console.log("Google analytics is ready now");
    }
  }

  checkConnection() {
    let disconnectSubscription = this.network.onDisconnect().subscribe(data => {
      console.log("network was disconnected", data);
      this.serviceManager.makeToastOnFailure("Internet is disconnected.");
    });

    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(data => {
      console.log("network connected!", data);
      this.serviceManager.makeToastOnSuccess("You are connected to Internet");
      this.ionViewCanEnter();
    });
  }
  roundAmount(amount: number | string) {
    return Math.round(Number(amount));
  }
}
