import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Platform, Loading } from 'ionic-angular';
import { MenuController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { retry } from 'rxjs/operator/retry';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { MainHomePage } from '../main-home/main-home';
import { AddAppointmentPage } from '../add-appointment/add-appointment'
import { RateAppointmentPage } from '../rate-appointment/rate-appointment';
import { GlobalServiceProvider } from '../../providers/global-service/global-service'
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { Customer } from '../../providers/SalonAppUser-Interface';
import { SalonDetailsPage } from '../salon-details/salon-details';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db';
import { config } from '../../providers/config/config';
import { NOPSO_Header_OPTIONS } from './../../providers/SalonAppUser-Interface';
import { ViewChild, NgZone } from '@angular/core';
import { Content } from 'ionic-angular';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';
/**
 * Generated class for the AppointmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-appointments',
  templateUrl: 'appointments.html',
})
export class AppointmentsPage {
  ionScrollTop = 0
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };

  public publicUser=false
  public loading: Loading
  public unregisterBackButtonAction: any;
  public slider: HTMLElement
  isSideMenu: string = '0';
  public techPicsURL = config.TechImageURL
  public selectedSegment = 0;
  public apposMainArray = [];
  public dataSource = [];
  public pastApposArray = [];
  public upcomingSelected = true;
  public logging = "These are logs"
  public myCustomer: Customer
  public currencySymbol = ''

  public nopsoHeaderOptions: NOPSO_Header_OPTIONS
  @ViewChild(Content) content: Content;
  constructor(
    private zone: NgZone,
    public alertCtrl: AlertController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public datepipe: DatePipe,
    public storage: Storage,

    public loadingController: LoadingController,

    public navCtrl: NavController,

    public gsp: GlobalServiceProvider,
    public events: Events,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    private custModal: CustomerModal,
    private menu: MenuController,
    public salonModal: SqliteDbProvider,
    public navParams: NavParams) {
    this.isSideMenu = navParams.get('isSideMenu');
    this.needToUpdateData();
    this.nopsoHeaderOptions = {
      miniHeaderHeight: '50pt',
      calledFrom: config.BeautyTipsViewPage,
    }


    //end temp
    platform.ready().then(() => {

      if (platform.is('cordova')) {

        //Subscribe on pause
        this.platform.pause.subscribe(() => {
           });

        //Subscribe on resume
        this.platform.resume.subscribe(() => {
               this.pageRefresh();
        });
      }
    });

  }

  //custom back button

  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    // if(this.menu.isOpen()){
    //   this.menu.close();
    // }else {
    //   this.nativePageTransitions.slide(this.options)
    //   this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
    // }  
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {

      if (this.navCtrl.canGoBack()) {

        this.navCtrl.pop();
      } else {

        this.navCtrl.push(MainHomePage, { isHideTab: "1" }, { animate: false })
      }
    }
  }

  pageRefresh() {
    this.custModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.custModal.getCustomer().then(myCustomer => {
          this.myCustomer = myCustomer
          this.getAppointmentsFromServer(null, true)
        })
      }
    })
  }
  //end custom back button 
  ionViewWillEnter() {
    this.custModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.custModal.getCustomer().then(myCustomer => {
          if(!myCustomer){
          
            this.publicUser=true
          }else{
            this.publicUser=false
          }
          this.myCustomer = myCustomer
          this.getAppointmentsFromServer(null, false)
        })
      }
    })

  }

  goToRegisterScreen(){
    this.navCtrl.push(LoginSignUpPage, { animate: false })
  }
  upcomingClicked() {
    this.slider = document.getElementById('slider_lt');
    this.slider.style.cssFloat = 'left';

    this.upcomingSelected = true;
    // document.getElementById('upcoming').style.color = 'black';
    // document.getElementById('past').style.color = 'rgb(160,158,161)';
    if (this.apposMainArray != undefined) {
      this.filterAppointmentsArray(this.apposMainArray);
    }
  }



  pastClicked() {
    this.slider = document.getElementById('slider_lt');
    this.slider.style.cssFloat = 'right';
    this.upcomingSelected = false;
    // document.getElementById('upcoming').style.color = 'rgb(160,158,161)';
    // document.getElementById('past').style.color = 'black';

    if (this.apposMainArray != undefined) {
      this.filterAppointmentsArray(this.apposMainArray);
    }

  }

  

  

  getAppointmentsFromServer(ionRefresher, isAlreadyLoading) {

    if (!this.myCustomer) {
      setTimeout(() => {
        if (ionRefresher) ionRefresher.complete();
      }, 2000);
      let _customer = this.serviceManager.getFromLocalStorage('zcustomer')
      if (_customer) this.myCustomer = _customer
     
      return
    }
    if (!isAlreadyLoading) {
      this.loading = this.loadingController.create({ content: "Fetching Appointments, Please Wait..." });
      this.loading.present();
    }

    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('lst_cust_appointments'),
      cust_id: btoa(this.myCustomer.cust_id),
      app_status: btoa('accepted,finished,pending,serving'),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        if (ionRefresher) ionRefresher.complete();
        setTimeout(() => {
          // alert('got at 266')
          if (ionRefresher) ionRefresher.complete();
        }, 2000);

        this.apposMainArray = response["appointment"];
      
        this.loading.dismissAll();

        if (this.apposMainArray != undefined) {
          this.filterAppointmentsArray(this.apposMainArray);
        } else {
          this.dataSource = [];
        }
      },
        error => {

          this.loading.dismissAll();
          setTimeout(() => {
            alert('got at 284')
            if (ionRefresher) ionRefresher.complete();
          }, 2000);
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }

  getAppServices(sers) {
    var sersArray = [];

    if (!sers) {
      return sersArray;
    }
    var str = sers;

    if (str.substring(str.length - 2, str.length) == ", ") {
      str = str.substring(0, str.length - 2);
    } else if (str.substring(str.length - 1, str.length) == ",") {
      str = str.substring(0, str.length - 1);
    }
    sersArray = str.split(',');
    return sersArray;
  }


  filterAppointmentsArray(appos) {

    let now = new Date(),
      result = appos.filter(appo => {
        var appStartTime = appo.app_start_time;
        appStartTime = appStartTime.replace(/-/g, '/');
        var time = new Date(appStartTime);
        if (this.upcomingSelected) {
          //Upcoming Appointments filter
          return (now.getTime() < time.getTime() && appo.app_status != this.serviceManager.APP_STATUS_FINISHED);
        } else {
          //Past Appointments filter
          return (now.getTime() > time.getTime() || appo.app_status == this.serviceManager.APP_STATUS_FINISHED);
        }
      });

    if (this.upcomingSelected) {
      //Sorting upcoming appoitnments
      result.sort(function (a, b) {
        var appStartTime1 = a.app_start_time;
        appStartTime1 = appStartTime1.replace(/-/g, '/');
        var appStartTime2 = b.app_start_time;
        appStartTime2 = appStartTime2.replace(/-/g, '/');

        return new Date(appStartTime1).getTime() - new Date(appStartTime2).getTime();
      });
    } else {
      //Sorting past appoitnments
      result.sort(function (a, b) {
        var appStartTime1 = a.app_start_time;
        appStartTime1 = appStartTime1.replace(/-/g, '/');
        var appStartTime2 = b.app_start_time;
        appStartTime2 = appStartTime2.replace(/-/g, '/');

        return new Date(appStartTime2).getTime() - new Date(appStartTime1).getTime();
      });
    }

    this.dataSource = result;
  }

  cancelAppointemnt(appo) {

    if (appo.app_status == "serving") {
      this.serviceManager.makeToastOnFailure(AppMessages.msgCancelServingAppointment)
    }

    let alert = this.alertCtrl.create({
      title: 'Cancel Appointment',
      message: 'Are you sure you want to cancel Appointment?',
      buttons: [
        {
          text: 'NO',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'YES',
          handler: () => {
            this.loading = this.loadingController.create({ content: "Cancelling, Please Wait..." });
            this.loading.present();
            var params = {
              device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
              service: btoa('cancel_app'),
              app_id: btoa(appo.app_id),
            }

            this.serviceManager.getData(params)
              .retryWhen((err) => {
                return err.scan((retryCount) => {
                  retryCount += 1;
                  if (retryCount < 3) {
                    return retryCount;
                  }
                  else {
                    throw (err);
                  }
                }, 0).delay(1000)
              })

              .subscribe((response) => {

                if (!this.myCustomer) {
                  this.custModal.getDatabaseState().subscribe(ready => {
                    if (ready) {
                      this.custModal.getCustomer().then(myCustomer => {
                        this.myCustomer = myCustomer
                        this.getAppointmentsFromServer(null, true)
                      })
                    }
                  })
                } else {
                  this.getAppointmentsFromServer(null, true)
                }



              },
                error => {
                  this.loading.dismissAll();
                  this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
                  console.log('something went wrong', error);
                }
              );


          }
        }
      ]
    });
    alert.present();

  }

  rateAppointemnt(appo) {
    console.log('Rate this appointment....');
    console.log(appo);
    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(RateAppointmentPage, {
      appo: appo,
    })
  }

  rescheduleAppointemnt(appo) {

    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal.getSalonDetailsFrom(appo.sal_id).then(mySalon => {
          if (mySalon) {
            this.nativePageTransitions.slide(this.options)
            this.navCtrl.push(SalonDetailsPage, {
              rescheduleAppointment: appo,
              selectedSalon: mySalon,
              sal_id: appo.sal_id,
            });
          } else {
            this.getSalonDataFromServerAndPush(appo);
          }
        })


      }
    })


  }

  getSalonDataFromServerAndPush(appo) {

    this.loading = this.loadingController.create({ content: "Fetching Details, Please Wait..." });
    this.loading.present();

    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('get_salon_details'),
      cust_id: btoa(this.myCustomer.cust_id),
      sal_id: btoa(appo.sal_id),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe((response) => {
        let selectedSalon = response["salons"][0];
        this.loading.dismissAll();

        this.nativePageTransitions.slide(this.options)
        this.navCtrl.push(SalonDetailsPage, {
          rescheduleAppointment: appo,
          selectedSalon: selectedSalon,
          sal_id: selectedSalon.sal_id
        });
      },
        error => {
          this.loading.dismissAll();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        });
  }
  needToUpdateData() {
   
    this.events.subscribe('NeedToUpdate', (value) => {
      console.log('Listener', 'Received1');
      if (value) {
        let index = this.navCtrl.parent.getSelected().index;
        if (index == 1) {
          if (!this.myCustomer) {
            this.custModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.custModal.getCustomer().then(myCustomer => {
                  this.myCustomer = myCustomer
                  this.getAppointmentsFromServer(null, false)
                })
              }
            })
          } else {
            this.getAppointmentsFromServer(null, false)
          }
        }
      } else {
      }

    });

  }

  getFormattedDate(dateStr) {
    dateStr = dateStr.replace(/-/g, '/');
    return this.datepipe.transform(dateStr, 'EEE, dd MMM (hh:mm a)');
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  //end custom back button for android
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
    let elemUpcoming = document.getElementById('upcoming')
    let elemPast = document.getElementById('past')
    if (elemUpcoming && elemPast) {
      document.getElementById('upcoming').style.color = 'black';
      document.getElementById('past').style.color = 'rgb(160,158,161)';
    }

    //  this.gsp.tabIndex = "1";

    this.initializeBackButtonCustomHandler();

    // this.events.subscribe('canShowMiniHeader', (isHidden) => {
    //   this.zone.run(() => {
    //     this.shouldShowMiniHeader = isHidden
    //   });
    // });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentsPage');

    this.currencySymbol = this.gsp.currencySymbol

    this.content.ionScroll.subscribe(ev =>
      requestAnimationFrame(() => this.ionScrollChange(ev.scrollTop)));


  }


  MainCtrl($scope, $ionicScrollDelegate) {
    console.log('scroll did begin');
    $scope.scrollMainToTop = function () {
      $ionicScrollDelegate.$getByHandle('mainScroll').scrollTop();
    };
    $scope.scrollSmallToTop = function () {
      $ionicScrollDelegate.$getByHandle('small').scrollTop();
    };
  }
  ionScrollChange(scrollTop) {
    this.zone.run(() => {
      this.ionScrollTop = scrollTop
    });
  }
  scrollEvent() {

  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options);

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage);
    }
  }
}