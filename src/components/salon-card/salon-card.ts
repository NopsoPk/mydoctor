import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,

} from "@angular/core";

import { SqliteDbProvider } from "../../providers/sqlite-db/sqlite-db";
import { config } from "../../providers/config/config";
import {
  OFFERS,
  SALON_CARD_OPTIONS,
  PINTEREST_OBJECT,
  Customer,

} from "../../providers/SalonAppUser-Interface";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { AllOffersPage } from "../../pages/all-offers/all-offers";
import { SalonServicesModal } from "../../providers/salon-services-modal/salon-services-modal";
// import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import { CustomerModal } from "../../providers/CustomerModal/CustomerModal";
import { GlobalProvider } from "../../providers/global/global";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";

import { SalonDetailsPage } from "../../pages/salon-details/salon-details";
import { GoogleAnalytics } from "@ionic-native/google-analytics";
import { Events, Platform } from "ionic-angular";
import { MainHomePage } from "../../pages/main-home/main-home";
import { GlobalServiceProvider } from "../../providers/global-service/global-service";
import { AppMessages } from "../../providers/AppMessages/AppMessages";


@Component({
  selector: "salon-card",
  templateUrl: "salon-card.html"
})
export class SalonCardComponent implements OnInit {
  @Input() salonCardOptions: SALON_CARD_OPTIONS;
  @Output() dateSelected = new EventEmitter<Date>();

  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };

  public salonImagesURL = config.salonImgUrl;
  public techImagesURL = config.TechImageURL;
  public subStyleImagesBaseURL = config.StylesImagesURL;
  public allSalons = [];
  public myCustomer: Customer;
  // public SalonCardHeight = '100vh'
  public offersForListing: OFFERS[] = [];
  public allOffers: OFFERS[];
  public currencySymbol = "";
  public distanceUnit = "";

  //favorites variables
  public selectedPostObject: PINTEREST_OBJECT;

  constructor(
    private nativePageTransitions: NativePageTransitions,
    public events: Events,
    public salonModal: SqliteDbProvider,
    public salonServicesModal: SalonServicesModal,
    public navCtrl: NavController,
    // public favPostModal: FavoriteModal,
    public customerModal: CustomerModal,
    public serviceManager: GlobalProvider,
    private ga: GoogleAnalytics,
    public gsp: GlobalServiceProvider,
    // public favModal: FavoriteModal,
    public platform: Platform
  ) {
    this.getCustomer();
  }
  ngOnInit() {
    this.currencySymbol = this.gsp.currencySymbol;
    this.distanceUnit = this.gsp.distance_unit;

    // alert('cardFrom'+this.salonCardOptions.calledFrom)
    this.allSalons = [];
    if (this.salonCardOptions) {
      switch (this.salonCardOptions.calledFrom) {
        case config.FavoritePage:
          this.allSalons = this.salonCardOptions.salon;
          // this.getFavoriteSalonsFromDB()
          break;

        case config.HomePage:
          this.allSalons = this.salonCardOptions.salon;
          break;
        case config.SalonDetailPage:
          this.allSalons = this.salonCardOptions.salon;
          // this.allSalons.push(this.salonCardOptions.salon)

          break;
        case config.PinRelatedSalonPage:
          this.allSalons = this.salonCardOptions.salon;

          break;
        case config.ConfirmBookingPage:
          this.allSalons = this.salonCardOptions.salon;
          break;

        default:
          break;
      }
    }

    this.allOffers = [];
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getAllSalonOffers().then(
          offers => {
            this.allOffers = offers;
          },
          error => {
            console.log("Error In Getting Offerr");
          }
        );
      }
    });
  }

  getAllSalons() {
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal.getAllSalonsFromDB().then(myAllSalons => {
          this.allSalons = myAllSalons;
        });
      }
    });
  }
  btnAllOfferTaped(salId, sal_name, sal_pic) {
    this.offersForListing = [];
    this.allOffers.forEach(element => {
      if (element.sal_id == salId) {
        this.offersForListing.push(element);
      }
    });
    this.navCtrl.push(AllOffersPage, {
      offersForListing: this.offersForListing,
      sal_name: sal_name,
      sal_pic: sal_pic
    });

    // let popover = this.popoverCtrl.create(AllOffersPage, {
    //   salId: "59",
    // }, { cssClass: 'PopOverClass' });
    // popover.present();
  }
  getPromotions(salId) {
    let promotions = this.getOfferBySalId(salId);
    if (promotions == null) {
      return "";
    } else {
      return promotions.so_title;
    }
  }

  getOfferBySalId(Id: string) {
    if (this.allOffers && this.allOffers.length > 0) {
      var found = this.allOffers.find(function(element) {
        return element.sal_id == Id;
      });
      return found;
    } else {
    }
  }

  getFavoriteSalonsFromDB() {
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal.getFavoriteSalon().then(res => {
          this.allSalons = res;
          // !this.favoriteSalons || this.favoriteSalons.length === 0 ? this.noSalonFound = true : this.noSalonFound = false
        });
      }
    });
  }

  // makeSalonFavoriteTapped(salon) {
  //   let mySalon: Salon = salon
  //   let makeSalonFavorite: number
  //   if (mySalon.favsal === 1) {
  //     makeSalonFavorite = 0
  //   } else {
  //     makeSalonFavorite = 1
  //   }
  //   // console.log(makeSalonFavorite);
  //   this.allSalons.forEach(salon => {
  //     if (salon.sal_id === mySalon.sal_id) {
  //       return salon.favsal = makeSalonFavorite
  //     }
  //   });

  //   if (this.salonCardOptions.calledFrom === config.FavoritePage) {
  //     this.allSalons.splice(this.allSalons.indexOf(salon), 1);
  //   }
  //   let params = {
  //     service: btoa('favorite'),
  //     sal_id: btoa(mySalon.sal_id),
  //     cust_id: btoa(this.myCustomer.cust_id),
  //     favorite: btoa(makeSalonFavorite.toString()),

  //   }
  //   this.serviceManager.getData(params).subscribe(res => {
  //     if (res['status'] === '1') {

  //       this.salonModal.getDatabaseState().subscribe(ready => {
  //         if (ready) {
  //           this.salonModal.makeSalonFavorite(mySalon.sal_id, makeSalonFavorite).then(isSalonMarkedFavorite => {
  //             if (isSalonMarkedFavorite) {

  //               // this.salonModal.getSalonDetailsFrom().then()
  //             }
  //           })
  //         }
  //       })
  //     } else {
  //       this.allSalons.forEach(salon => {
  //         if (salon.sal_id === mySalon.sal_id) {
  //           return salon.favsal = !makeSalonFavorite
  //         }
  //       });
  //     }

  //   })
  // }

  //favourite section
  // makeSalonFavoriteTapped(salon, index: number) {
  //   if (!this.myCustomer) {
  //     this.navCtrl.push(LoginSignUpPage, { animate: false })
  //     return
  //   }

  //   let itemBackUp = salon;
  //   this.setFavToDbAndServer(itemBackUp)
  //   let mySalon: Salon = salon
  //   let makeSalonFavorite: number
  //   if (mySalon.fav_id !== 0) {
  //     this.allSalons[index].fav_id = 0
  //   } else {
  //     this.allSalons[index].fav_id = mySalon.sal_id
  //   }
  // }
  // setFavToDbAndServer(salon) {

  //   let item = {
  //     fav_id: salon.sal_id,
  //     cft_id: 1,
  //   }
  //   if (salon.fav_id == 0) {
  //     this.setFavToDb(item)
  //     this.setFavToServer(item, 1)
  //   } else {
  //     this.removeFavFromDb(item)
  //     this.setFavToServer(item, 0)
  //   }

  // }
  // removeFavFromDb(item) {
  //   this.favModal.getDatabaseState().subscribe(ready => {
  //     if (ready) {
  //       this.favModal.deleteFavorite(item.fav_id).then(isSalonMarkedFavorite => {
  //         if (isSalonMarkedFavorite) {
  //           //this.removeFromFavFashion(item);
  //         }
  //       })
  //     }
  //   })
  // }
  // setFavToDb(item) {
  //   this.favModal.getDatabaseState().subscribe(ready => {
  //     if (ready) {
  //       this.favModal.InsertInToFavoriteTable(item).then(isSalonMarkedFavorite => {
  //         if (isSalonMarkedFavorite) {
  //           // this.setFavFashions(item)
  //         }
  //       })
  //     }
  //   })
  // }
  setFavToServer(item, status) {
    const params = {
      service: btoa("favorites"),
      fav_id: btoa(item.fav_id),
      cft_id: btoa("1"),
      cust_id: btoa(this.myCustomer.cust_id),
      favorite: btoa(status)
    };
    console.log("params" + JSON.stringify(params));
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          console.log("Response" + JSON.stringify(res));
        },
        error => {
          this.serviceManager.stopProgress();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
        }
      );
  }
  //end favorite post section

  openDetailsPage(salon) {
    // alert('going to detail-page')
    if (this.salonCardOptions.shouldNotLoadSalonAgain) {
      return;
    }
    let offerType;
    let promotions = this.getOfferBySalId(salon.sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }

    // if (salon.sal_status == "0" && salon.sal_temp_enabled == "0") {
    //   this.nativePageTransitions.slide(this.options)
    //   this.navCtrl.push(InActiveSalonPage);
    // } else {
    if (this.myCustomer) {
      let eventAction =
        this.myCustomer.cust_name +
        " Tapped on Salon " +
        salon.sal_name +
        " of id " +
        salon.sal_id;
      this.ga.trackEvent("Tap", eventAction, "lab: general label", 200, true);
    }

    let distance = 0;
    if (salon.distance) {
      distance = salon.distance;
    }
    this.nativePageTransitions.slide(this.options);
    this.navCtrl.push(SalonDetailsPage, {
      selectedSalon: salon,
      offerType: offerType,
      distance,
      sal_id: salon.sal_id
    });
  }
  btnBookAppointmentTapped(salon) {
    let eventAction =
      this.myCustomer.cust_name +
      " is interested in Booking Appointment with " +
      salon.sal_name;
    this.googleAnalytics(eventAction);
    this.nativePageTransitions.slide(this.options);
    this.navCtrl.push(SalonDetailsPage, {
      selectedSalon: salon,
      sal_id: salon.sal_id
    });
  }
  googleAnalytics(eventAction: string) {
    // this.ga.trackView(this.myCustomer.cust_name + ' visited Salon Detail Page');

    this.ga.trackEvent("Tap", eventAction, "lab: general label", 200, true);
    this.ga.trackTiming(
      "cat: tracking timing",
      600000,
      "variable: not sure what will go here",
      "label: and the same"
    );
    this.ga.debugMode();
    this.ga.setAllowIDFACollection(true);
    this.ga.setUserId(this.myCustomer.cust_id);
    console.log("Google analytics is ready now");
  }

  getCustomer() {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            this.myCustomer = customer;
          },
          error => {
            console.log("error: while getting customer");
          }
        );
      }
    });
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options);

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(MainHomePage);
    }
  }
  roundRatingUpToOneDecimal(ratingValue: number) {
    return Math.round(ratingValue).toFixed(1);
  }
  roundDistance(distanace: number) {
    return distanace ? distanace : 0;
  }
}
