
import { CustomerModal } from "../providers/CustomerModal/CustomerModal";
import { HomeModal } from "../providers/homeModal/homeModal";
import { SqliteDbProvider } from "../providers/sqlite-db/sqlite-db";
import { SalonServicesModal } from "../providers/salon-services-modal/salon-services-modal";

import { SQLitePorter } from "@ionic-native/sqlite-porter";


import { NativePageTransitions } from "@ionic-native/native-page-transitions";
import { FormsModule } from "@angular/forms";
import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler, Nav } from "ionic-angular";
import { MyApp } from "./app.component";
import { SQLite } from "@ionic-native/sqlite";
import { HttpModule } from "@angular/http";
import { HttpClient } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { LazyLoadImageModule } from "ng-lazyload-image";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";

import { IonicStorageModule } from "@ionic/storage";
import { DatePipe } from "@angular/common";

import { Ionic2RatingModule } from "ionic2-rating";

import { File } from "@ionic-native/file";
import {
  FileTransfer,

  FileTransferObject
} from "@ionic-native/file-transfer";
import { FilePath } from "@ionic-native/file-path";
import { Camera } from "@ionic-native/camera";
import { GoogleAnalytics } from "@ionic-native/google-analytics";

import { EmailComposer } from "@ionic-native/email-composer";
import { Base64 } from "@ionic-native/base64";
import { FCM } from "@ionic-native/fcm";
import { CalendarComponent } from "../components/calendar/calendar";
import { TabsPage } from "../pages/tabs/tabs";
import { MainHomePage } from "../pages/main-home/main-home";
import { LoginSignUpPage } from "../pages/login-sign-up/login-sign-up";


import { HomePage } from "../pages/home/home";
import { AppointmentsPage } from "../pages/appointments/appointments";
import { AddAppointmentPage } from "../pages/add-appointment/add-appointment";
import { NotificationsPage } from "../pages/notifications/notifications";
import { ProfilePage } from "../pages/profile/profile";
import { TermsAndConditionsPage } from "../pages/terms-and-conditions/terms-and-conditions";
import { FeedbackPage } from "../pages/feedback/feedback";

import { BeautyTipsViewPage } from "../pages/beauty-tips-view/beauty-tips-view";


import { SalonDetailsPage } from "../pages/salon-details/salon-details";
import { SlotsViewPage } from "../pages/slots-view/slots-view";
import { ConfirmBookingPage } from "../pages/confirm-booking/confirm-booking";
import { ThanksVcPage } from "../pages/thanks-vc/thanks-vc";
import { MenuPage } from "../pages/menu/menu";
import { PrivacyPolicyPage } from "../pages/privacy-policy/privacy-policy";
import { SalonReviewsPage } from "../pages/salon-reviews/salon-reviews";

import { RateAppointmentPage } from "../pages/rate-appointment/rate-appointment";


import { AllOffersPage } from "../pages/all-offers/all-offers";
import { GlobalServiceProvider } from "../providers/global-service/global-service";

import { Geolocation } from "@ionic-native/geolocation";
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { Device } from "@ionic-native/device";
import { AppVersion } from "@ionic-native/app-version";
import { Keyboard } from "@ionic-native/keyboard";

import { DatePickerModule } from "datepicker-ionic2";
import { MultiPickerModule } from "ion-multi-picker";

import { GlobalProvider } from "../providers/global/global";

import { ExpandableHeader } from "../components/expandable-header/expandable-header";
import { ExpandableComponent } from "../components/expandable/expandable";
import { IonicImageViewerModule } from "ionic-img-viewer";

import { AppRate } from "@ionic-native/app-rate";
import { SalonCardComponent } from "../components/salon-card/salon-card";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ConfigModal } from "../providers/ConfigModal/ConfigModal";
import { BeautyTipsDetailPage } from "../pages/beauty-tips-detail/beauty-tips-detail";
import { BeautyTipsModal } from "../providers/BeautyTipsModal/BeautyTipsModal";
import { TipsDetailModal } from "../providers/TipsDetailModal/TipsDetailModal";

import { AppointmentsCardPage } from "../pages/appointments-card/appointments-card";
import { AppointmentCardModel } from "../providers/AppointmentCardModel/AppointmentCardModel";
import { NopsoHideHeaderDirective } from "../directives/nopso-hide-header/nopso-hide-header";

import { Diagnostic } from "@ionic-native/diagnostic";


import { SocialSharing } from "@ionic-native/social-sharing";



import { WheelSelector } from "@ionic-native/wheel-selector";

import { InAppBrowser } from "@ionic-native/in-app-browser";
import { Network } from "@ionic-native/network";


import { ParticlesProvider } from "../providers/particles/particles";
import { AppInfoPage } from "../pages/app-info/app-info";

import { ForceUpdatePage } from "../pages/force-update/force-update";

@NgModule({
  declarations: [
    MyApp,
    MainHomePage,
    LoginSignUpPage,
    BeautyTipsViewPage,
    NopsoHideHeaderDirective,

    SalonDetailsPage,

    AppointmentsPage,
    AddAppointmentPage,
    SlotsViewPage,
    ConfirmBookingPage,
    ThanksVcPage,
    NotificationsPage,
    ProfilePage,
    HomePage,
    TabsPage,

    MenuPage,
    PrivacyPolicyPage,
    TermsAndConditionsPage,
    FeedbackPage,

    // LatestTrendSubCatPage,
    FeedbackPage,
    SalonReviewsPage,

    RateAppointmentPage,

    ExpandableComponent,
    SalonCardComponent,
    CalendarComponent,
    ExpandableHeader,
    AllOffersPage,

    BeautyTipsDetailPage,

    AppointmentsCardPage,
    AppInfoPage,
    ForceUpdatePage
  ],
  imports: [
    IonicImageViewerModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    Ionic2RatingModule,
    DatePickerModule,
    MultiPickerModule,
    LazyLoadImageModule,
    HttpClientModule,

    // HighlightModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      backButtonText: "",
      backButtonIcon: "ios-arrow-back",
      iconMode: "ios"
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainHomePage,
    LoginSignUpPage,
    BeautyTipsViewPage,
    SalonDetailsPage,

    AppointmentsPage,
    AddAppointmentPage,
    SlotsViewPage,
    ConfirmBookingPage,
    ThanksVcPage,
    NotificationsPage,
    ProfilePage,
    HomePage,
    TabsPage,
    MenuPage,
    PrivacyPolicyPage,
    TermsAndConditionsPage,

    FeedbackPage,

    FeedbackPage,
    SalonReviewsPage,

    RateAppointmentPage,

    AllOffersPage,
    BeautyTipsDetailPage,
    AppointmentsCardPage,
    AppInfoPage,
    ForceUpdatePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    Diagnostic,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    // SpinnerDialog,
    GoogleAnalytics,
    NativePageTransitions,

    DatePipe,
    File,
    FileTransfer,
    FilePath,
    Camera,
    FileTransferObject,
    Nav,
    EmailComposer,
    Base64,
    FCM,
    GlobalServiceProvider,
    LocationAccuracy,
    Geolocation,
    Device,
    AppVersion,
    // AdMobFree,
    Keyboard,
    GlobalProvider,
    HttpClient,
    SqliteDbProvider,
    SQLite,
    SQLitePorter,
    SalonServicesModal,
    HomeModal,
    CustomerModal,
    // FavoriteModal,
    // FavoriteModal,
    AppRate,
    ConfigModal,
    BeautyTipsModal,
    TipsDetailModal,
    AppointmentCardModel,
    SocialSharing,

    WheelSelector,

    InAppBrowser,
    AndroidPermissions,
    Network,

    ParticlesProvider
  ]
})
export class AppModule { }
