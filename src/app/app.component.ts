import { config } from "./../providers/config/config";

import { BeautyTipsViewPage } from "./../pages/beauty-tips-view/beauty-tips-view";

import { Homepage, Salon } from "./../providers/SalonAppUser-Interface";
import { HomeModal } from "./../providers/homeModal/homeModal";
import { CustomerModal } from "./../providers/CustomerModal/CustomerModal";
import { GlobalProvider } from "./../providers/global/global";
import { Component, ViewChild, NgZone, ElementRef } from "@angular/core";
import { Platform, Nav, App } from "ionic-angular";
import { EmailComposer } from "@ionic-native/email-composer";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { MainHomePage } from "../pages/main-home/main-home";
import { LoginSignUpPage } from "../pages/login-sign-up/login-sign-up";

import { Storage } from "@ionic/storage";

import { AppointmentsPage } from "../pages/appointments/appointments";
import { TabsPage } from "../pages/tabs/tabs";
import { TermsAndConditionsPage } from "../pages/terms-and-conditions/terms-and-conditions";
import { PrivacyPolicyPage } from "../pages/privacy-policy/privacy-policy";
import { NotificationsPage } from "../pages/notifications/notifications";
import { ProfilePage } from "../pages/profile/profile";
import { FeedbackPage } from "../pages/feedback/feedback";

import { GlobalServiceProvider } from "../providers/global-service/global-service";
import { Events } from "ionic-angular";
import { FCM } from "@ionic-native/fcm";
import { Customer } from "../providers/SalonAppUser-Interface";

import { GoogleAnalytics } from "../../node_modules/@ionic-native/google-analytics";
import {
  NativeTransitionOptions,
  NativePageTransitions
} from "@ionic-native/native-page-transitions";
import { ConfigModal } from "../providers/ConfigModal/ConfigModal";
import { Geolocation } from "@ionic-native/geolocation/";

import { timer } from "rxjs/observable/timer";
import { Diagnostic } from "@ionic-native/diagnostic";
import { AppMessages } from "../providers/AppMessages/AppMessages";
import { Network } from "@ionic-native/network";
import { AlertController } from "ionic-angular";
// import { ShippingInfoPage } from '../pages/shipping-info/shipping-info';
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { SalonServicesModal } from "../providers/salon-services-modal/salon-services-modal";
import { SqliteDbProvider } from "../providers/sqlite-db/sqlite-db";
import { ForceUpdatePage } from "../pages/force-update/force-update";
@Component({
  templateUrl: "app.html"
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public userProfileIcon = "https://nopso.qwpcorp.com/salon/customers/"; //"./assets/imgs/user_profile_pic.png";
  splash = false;
  isLogin: string;
  menuSide: string = "right";
  UserName: string;
  rootPage: any = null;
  message: string = "";
  myCustomer: Customer;
  homepageData: Homepage[];
  public gcmToken;
  // pages: Array<{ title: string, component: any, icon: string }>;
  public staticPages: Array<{ title: string; component: any; icon: string }>;
  public subMenuHeader: Array<{ title: string; component: any; icon: string }>;
  public subMenuPages: Array<{ title: string; component: any; icon: string }>;
  public homeImageUrl = config.salonImgUrl;
  public homeIcon = "menu_home.png";
  locationSource = "assets/imgs/location.png";
  public updatelocationCount = 0;
  public latitude: number;
  public longitude: number;
  public searchElementRef: ElementRef;
  public showSubMenuForAboutUs = false;
  object: any;
  google: any;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public emailComposer: EmailComposer,
    public alertCtrl: AlertController,
    private fcm: FCM,
    public events: Events,
    public homeModal: HomeModal,
    protected appCtrl: App,
    public configModal: ConfigModal,
    public customerModal: CustomerModal,
    public serviceManager: GlobalProvider,
    public ga: GoogleAnalytics,
    private nativePageTransitions: NativePageTransitions,
    public gsp: GlobalServiceProvider,
    public geolocation: Geolocation,
    public diagnostic: Diagnostic,
    private network: Network,
    private zone: NgZone,
    private androidPermissions: AndroidPermissions,
    public salonServicesModal: SalonServicesModal,
    public sqlProvider: SqliteDbProvider
  ) {
    this.initializeApp();
  }
  exitAlertAlreadyShow = false;
  ionViewWillEnter() {
    // this.getCustomer();
    // this.pages = [
    //   { title: 'Home', component: MainHomePage, icon: 'assets/imgs/menu_home.png' }
    // ];
    this.staticPages = [
      {
        title: "Home",
        component: MainHomePage,
        icon: "assets/imgs/menu_home.png"
      },
      // { title: 'View Cart', component: ViewCartPage, icon: 'assets/imgs/menu_cart.png' },
      // { title: 'My Orders', component: MyOrdersPage, icon: 'assets/imgs/menu_my_order.png' },
      {
        title: "My Appointments",
        component: AppointmentsPage,
        icon: "assets/imgs/menu_appointments.png"
      },
      {
        title: "Notifications",
        component: NotificationsPage,
        icon: "assets/imgs/menu_notifications.png"
      },

      {
        title: "Update Location",
        component: PrivacyPolicyPage,
        icon: "assets/imgs/menu_update_location.png"
      }
    ];
    this.events.subscribe("customer:changed", (customer: string) => {
      if (customer !== undefined && customer !== "") {
        this.object = {
          title: "Settings",
          component: ProfilePage,
          icon: "assets/imgs/menu_settings.png"
        };
        // this.staticPages.push(this.object)
        this.ionViewWillEnter();
      }
    });

    if (this.myCustomer) {
      if (!this.object) {
        this.object = {
          title: "Settings",
          component: ProfilePage,
          icon: "assets/imgs/menu_settings.png"
        };
      }
      this.staticPages.push(this.object);
    } else {
      if (!this.object) {
        this.object = {
          title: "Register",
          component: LoginSignUpPage,
          icon: "assets/imgs/menu_settings.png"
        };
      }
      this.staticPages.push(this.object);
    }

    this.subMenuPages = [
      {
        title: "Feedback",
        component: FeedbackPage,
        icon: "assets/imgs/menu_feedback.png"
      },
      {
        title: "Contact Us",
        component: PrivacyPolicyPage,
        icon: "assets/imgs/menu_contact_us.png"
      },
      {
        title: "Terms and Conditions",
        component: TermsAndConditionsPage,
        icon: "assets/imgs/menu_terms_and_conditions.png"
      },
      {
        title: "Privacy Policy",
        component: PrivacyPolicyPage,
        icon: "assets/imgs/menu_privacy_policy.png"
      }
    ];
    this.subMenuHeader = [
      {
        title: "About Us",
        component: PrivacyPolicyPage,
        icon: "assets/imgs/menu_contact_us.png"
      }
    ];
    this.platform.ready().then(() => {
      // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION])
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        console.log("network was disconnected :-(");
      });

      let connectSubscription = this.network.onConnect().subscribe(() => {
        console.log("network connected!");

        setTimeout(() => {
          if (this.network.type === "wifi") {
            console.log("we got a wifi connection, woohoo!");
          }
        }, 3000);
      });

      this.splashScreen.hide();

      timer(1000).subscribe(() => {
        this.splash = false;
      });
    });
    this.gcmToken = this.serviceManager.getFromLocalStorage(
      this.serviceManager.GCM_TOKEN
    );
    this.ga
      .startTrackerWithId(config.GoogleAnalyticsAppID)
      .then(() => {
        console.log("Google analytics is ready now");
      })
      .catch(e => {
        console.log("Error starting GoogleAnalytics", e);
      });
    this.events.subscribe("base64Image:changed", (base64Image: string) => {
      if (base64Image !== undefined && base64Image !== "") {
        this.userProfileIcon = base64Image;
      }
    });
    this.storage.get("base64Image").then(value => {
      console.log("here its value'" + value);
      if (value !== undefined && value !== "" && value !== null) {
        console.log("here its'");
        this.userProfileIcon = value;
      }
    });
  }
  public _isForceUpdateRequired = false;
  setRoot() {
    this._isForceUpdateRequired = this.serviceManager.getFromLocalStorage(
      this.serviceManager.IS_FORCE_UPDATE_AVAILABLE
    );
    if (this._isForceUpdateRequired) {
      this.rootPage = ForceUpdatePage;
    } else {
      this.rootPage = MainHomePage;
    }

    this.ionViewWillEnter();
    // this.storage.get('isLogin').then((isLogin) => {
    //   isLogin ? this.rootPage = MainHomePage : this.rootPage = MainHomePage
    //   // isLogin ? this.rootPage = LoginSignUpPage : this.rootPage = LoginSignUpPage
    // })
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.initFcm();
      this.setRoot();
      this.platform.registerBackButtonAction(() => {});
      this.statusBar.backgroundColorByHexString("#000000"); // set status bar to black
      this.statusBar.styleLightContent();
    });
  }
  initFcm() {
    // FCM Subscription
    this.fcm.subscribeToTopic("marketing");

    this.fcm.onNotification().subscribe(data => {
      this.events.publish("NeedToUpdate", true);
      if (data.wasTapped) {
        this.goToNotificationsTab();

        console.log("Received in background");
      } else {
        console.log("Received in foreground");
      }
    });

    if (!this.gcmToken) {
      this.fcm
        .getToken()
        .then(token => {
          if (token) {
            this.serviceManager.setInLocalStorage(
              this.serviceManager.GCM_TOKEN,
              token
            );
          } else {
            this.serviceManager.sendErrorToServer(
              "token has not been established yet.",
              "getToken() in Component.ts file"
            );
            console.log("token seems null");
          }
        })
        .catch(error => {
          this.serviceManager.sendErrorToServer(
            "token has not been established yet.",
            "getToken() in Component.ts file"
          );
        });

      this.fcm.onTokenRefresh().subscribe(token => {
        if (token) {
          let _token = this.serviceManager.getFromLocalStorage(
            this.serviceManager.GCM_TOKEN
          );
          if (!_token) {
            this.serviceManager.setInLocalStorage(
              this.serviceManager.GCM_TOKEN,
              token
            );
            this.updatePushNotificationToken(token);
          }
        } else {
          this.serviceManager.sendErrorToServer(
            "token has not been established yet.",
            "onTokenRefresh() in Component.ts file"
          );
          console.log("token seems null");
        }
      });
    }
  }
  showSubMenus() {
    this.showSubMenuForAboutUs = !this.showSubMenuForAboutUs;
  }
  openPage(page) {
    if (page.title == "Contact Us") {
      this.sendEmail();
      console.log("time to contact us");
    } else if (page.title == "Update Location") {
      this.openLocationSettingIfNeeded();
    } else {
      this.nav.setRoot(page.component, { isSideMenu: "1" });
    }
  }
  openSubMenuPage(page) {
    if (page.title == "About Us") {
      this.showSubMenuForAboutUs = !this.showSubMenuForAboutUs;
    }
    if (page.title == "Contact Us") {
      this.sendEmail();
      console.log("time to contact us");
    } else if (page.title == "Update Location") {
      this.openLocationSettingIfNeeded();
    } else {
      this.nav.setRoot(page.component, { isSideMenu: "1" });
    }
  }
  sendEmail() {
    let email = {
      to: "contactus@mydr.pk",
      isHtml: true
    };
    this.emailComposer.open(email);
  }
  getCustomer() {
    // this.rootPage = MainHomePage

    // const nav = this.appCtrl.getRootNav();
    // nav.setRoot(MainHomePage, { myCustomer: customer });
    /** 
    console.log('came in get customer');

   
    */
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            if (customer) {
              this.events.publish("customer:changed", customer);
            }
          },
          error => {
            console.log("error: while getting customer");
          }
        );
      }
    });
  }
  getPage(view: string) {
    if (view == "TabsPage") {
      return TabsPage;
    } else if (view == "BeautyTipsViewPage") {
      return BeautyTipsViewPage;
    }
  }
  goToNotificationsTab() {
    let transitionOptions: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };

    this.nav.popToRoot();
    const nav = this.appCtrl.getRootNav();

    this.nativePageTransitions.slide(transitionOptions);
    nav.setRoot(NotificationsPage);
  }
  gotLocationGoigToUpdateCustomeLocation = false;
  gpsOptions = { maximumAge: 300000, timeout: 10000, enableHighAccuracy: true };
  openLocationSettingIfNeeded() {
    this.platform.ready().then(() => {
      if (this.platform.is("android")) {
        this.androidPermissions.requestPermissions([
          this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION
        ]);
        this.geolocation.getCurrentPosition(this.gpsOptions).then(
          location => {
            this.latitude = location.coords.latitude;
            this.longitude = location.coords.longitude;
            this.gotLocationGoigToUpdateCustomeLocation = true;
            this.updateCustomerLocationOnServer();
          },
          onError => {
            this.diagnostic.isGpsLocationAvailable().then(isLocationEnabled => {
              isLocationEnabled
                ? this.getCurrenctLocation()
                : this.confirmSwitchToSetting();
            });
            this.platform.resume.subscribe(() => {
              this.platform.ready().then(() => {
                this.diagnostic
                  .isGpsLocationAvailable()
                  .then(isLocationEnabled => {
                    isLocationEnabled ? this.getCurrenctLocation() : "";
                    // this.serviceManager.makeToastOnFailure('BeautyApp is unable to get location permission. Please allow location permission manually. Please review the message.')
                  });
              });
            });
          }
        );
      } else {
        this.getCurrenctLocation();
      }
    }); //platform ready
  }
  getCurrenctLocation() {
    if (this.gotLocationGoigToUpdateCustomeLocation) {
      return;
    }
    this.geolocation.getCurrentPosition(this.gpsOptions).then(
      location => {
        this.latitude = location.coords.latitude;
        this.longitude = location.coords.longitude;
        this.gotLocationGoigToUpdateCustomeLocation = true;
        this.updateCustomerLocationOnServer();
      },
      onError => {}
    );
  }
  updateCustomerLocationOnServer() {
    if (!this.myCustomer) {
      let _customer = this.serviceManager.getFromLocalStorage("zcustomer"); //try to fetch customer again
      if (_customer) {
        //customer exist
        this.myCustomer = _customer;
      } else {
        //customer not exist

        this.myCustomer = {};
        let publicUserId = this.serviceManager.getFromLocalStorage(
          this.serviceManager.PUBLIC_USER_ID
        );
        if (!publicUserId) return;
        this.myCustomer.cust_id = publicUserId;
      }

      this.gotLocationGoigToUpdateCustomeLocation = false;
      // return
    }

    this.updatelocationCount = 0;
    const params = {
      service: btoa("update_location"),
      cust_id: btoa(this.myCustomer.cust_id),
      cust_lat: btoa(this.latitude.toString()),
      cust_lng: btoa(this.longitude.toString())
    };

    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          console.log(res.msg);
          this.gotLocationGoigToUpdateCustomeLocation = false;
          this.serviceManager.makeToastOnSuccess(res.msg);

          this.customerModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.customerModal
                .updateCustomerTable(this.latitude, this.longitude)
                .then(updated => {
                  this.fetchSalonsInRadius();
                });

              for (let i = 0; i < this.nav.length(); i++) {
                let v = this.nav.getViews()[i];
                console.log(v.component.name + "........");
                if (
                  v.component.name === "MainHomePage" &&
                  this.nav.length() === 1
                ) {
                  this.nav.setRoot(MainHomePage, { isSideMenu: "1" });
                }
              }
            }
          });
        },
        error => {
          this.gotLocationGoigToUpdateCustomeLocation = false;
          console.log(error);

          // this.serviceManager.makeToastOnFailure(error, 'top')
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );

          console.log("something went wrong", error);
        },
        () => {
          this.gotLocationGoigToUpdateCustomeLocation = false;
          // this.serviceManager.stopProgress()
        }
      );

    // }
  }
  salonIds = "";
  fetchSalonsInRadius() {
    console.log("lat2" + this.latitude);
    console.log("lun1" + this.longitude);
    if (!this.myCustomer) {
      let _customer = this.serviceManager.getFromLocalStorage("zcustomer");
      if (_customer) this.myCustomer = _customer;
    }

    this.updatelocationCount = 0;
    const params = {
      service: btoa("salons_in_radius"),
      cust_lat: btoa(this.latitude.toString()),
      cust_lng: btoa(this.longitude.toString())
    };

    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          console.log(res.msg);

          this.allSalon = res.salons;
          if (this.allSalon && this.allSalon.length > 0) {
            this.getAndSetSalonOffer(this.allSalon);
          }

          this.SalonsServices = [];
          this.allSalon.forEach((salon: Salon) => {
            this.salonIds += salon.sal_id + ",";
            this.SalonsServices = this.SalonsServices.concat(salon.services);
          });
          this.salonIds = this.salonIds.slice(0, this.salonIds.length - 1);
          this.sqlProvider.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.sqlProvider.deleteMultipleSalon(this.salonIds).then(res => {
                if (this.allSalon) {
                  this.sqlProvider.insert_salon_table(this.allSalon);
                }
              });
            }
          });
          this.insertSalonServices();
          let get_current_date_time = this.serviceManager.getCurrentDateTime();
          this.serviceManager.setInLocalStorage(
            this.serviceManager.LAST_FETCH_DATE_SALONS,
            get_current_date_time
          );
          this.serviceManager.setInLocalStorage(
            this.serviceManager.SALONS_NEAR_CUSTOMER,
            this.allSalon
          );
        },
        error => {
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
        }
      );
  }

  updatePushNotificationToken(token) {
    if (this.myCustomer) {
      let params = {
        service: btoa("update_cust_device_id"),
        cust_id: btoa(this.myCustomer.cust_id),
        cust_device_id: btoa(token)
      };
      this.serviceManager.getData(params).subscribe(res => {
        if (res.status === "1" || res.status === 1) {
        }
      });
    } else {
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.getCustomer().then(_myCustomer => {
            if (_myCustomer) {
              this.myCustomer = _myCustomer;
              let params = {
                service: btoa("update_cust_token"),
                cust_id: btoa(this.myCustomer.cust_id),
                cust_phone: btoa(this.myCustomer.cust_phone)
              };
              this.serviceManager.getData(params).subscribe(res => {
                if (res.status === "1" || res.status === 1) {
                }
              });
            }
          });
        }
      });
    }
  }
  confirmSwitchToSetting() {
    if (this.platform.is("android")) {
      let alert = this.alertCtrl.create({
        title: "Device location request",
        message:
          "Your device location service is not enabled for BeautyApp.  Please allow BeautyApp to access your location.",
        buttons: [
          {
            text: "Deny",
            role: "cancel",
            handler: () => {
              console.log("Cancel clicked");
            }
          },
          {
            text: "Allow",
            handler: () => {
              console.log("Yes clicked");

              this.diagnostic.switchToLocationSettings();
            }
          }
        ]
      });
      alert.present().then(() => {});
    }
  }

  allSalon;

  ALLOFFERS;
  getAndSetSalonOffer(salons) {
    let so_ids = "";
    this.ALLOFFERS = [];
    salons.forEach(element => {
      if (element.offers !== null) {
        for (let index = 0; index < element.offers.length; index++) {
          const item = element.offers[index];
          this.ALLOFFERS.push(item);
        }
      }
    });
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.saveSalonServicesIntoDB(this.allSalon);
        if (this.ALLOFFERS) {
          this.salonServicesModal.insert_salon_offer(this.ALLOFFERS);
        }
      }
    });
  }

  SALON_SERVICE_SEARCH = [];

  SalonsServices: any[];
  insertSalonServices() {
    if (!this.allSalon) {
      return;
    }

    this.SALON_SERVICE_SEARCH = [];
    this.SalonsServices.forEach(element => {
      let item = {
        keyword: element.sser_name,
        type: "service",
        refer_id: element.ser_id,
        image_url: ""
      };
      this.SALON_SERVICE_SEARCH.push(item);
    });
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider
          .deleteSearchBySalIdFromService(this.salonIds, "service")
          .then(isDeleted => {
            if (
              this.SALON_SERVICE_SEARCH &&
              this.SALON_SERVICE_SEARCH.length > 0
            ) {
              this.sqlProvider
                .InsertInTblSearch(this.SALON_SERVICE_SEARCH)
                .then(isDataInserted => {});
            }
          });
      }
    });
  }
}
